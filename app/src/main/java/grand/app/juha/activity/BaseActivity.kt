package grand.app.juha.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.ParentActivity
import grand.app.juha.customviews.actionbar.BackActionBarView
import grand.app.juha.databinding.ActivityBaseBinding
import grand.app.juha.pages.auth.register.RegisterFragment
import grand.app.juha.pages.intro.IntroFragment
import grand.app.juha.pages.order.ui.OrderDetailsFragment
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.pages.settings.notification.NotificationListFragment
import grand.app.juha.pages.settings.notification.notify.NotificationGCMModel
import grand.app.juha.pages.splash.SplashFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper
import timber.log.Timber

class BaseActivity : ParentActivity() {
    var activityBaseBinding: ActivityBaseBinding? = null
    var backActionBarView: BackActionBarView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeLanguage()
        val component = (applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base)
        getNotification()
        if (!notification_checked) {
            if (intent.hasExtra(Constants.PAGE)) {
                val fragmentName = intent.getStringExtra(Constants.PAGE)
                if (fragmentName != null) {
                    try {
                        val fragment = Class.forName(fragmentName).newInstance() as Fragment
                        MovementHelper.addFragmentTag(this, getBundle(fragment))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                } else Timber.e("Fragment Name is empty")
            } else {
                val fragment = SplashFragment()

                MovementHelper.replaceFragment(
                    this,
                    fragment
                )
            }
        }
    }



    private fun setTitleName() {
        if (intent.hasExtra(Constants.NAME_BAR)) {
            backActionBarView = BackActionBarView(this)
            backActionBarView?.setTitle(intent.getStringExtra(Constants.NAME_BAR))
            activityBaseBinding?.llBaseActionBarContainer?.addView(backActionBarView)
        }
    }

    /*
        notification list   notification_type => 1
        notification order  notification_type => 2
        notification chat   notification_type => 3
     */
    private fun getNotification() {
        Log.d(TAG, "Notification GET")
        if (intent.extras != null && intent.hasExtra(Constants.BUNDLE_NOTIFICATION)) {
            Log.d(TAG, "Notification HAS EXTRA")
            val bundle = intent.getBundleExtra(Constants.BUNDLE_NOTIFICATION)
            val model: NotificationGCMModel? =
                bundle?.getSerializable(Constants.BUNDLE_NOTIFICATION) as NotificationGCMModel?
            if (model != null) {
                notification_checked = true
                when (model.notification_type) {
                    1 -> {//admin notification
                        setTitleName(ResourceManager.getString(R.string.notifications))
                        MovementHelper.replaceFragment(this, NotificationListFragment(), "")
                        return
                    }
                    2 -> {//product notification
                        val bundleNew = Bundle()
                        bundleNew.putInt(Constants.ID,model.productId)
                        val fragment = ProductDetailsFragment()
                        fragment.arguments = bundleNew
                        MovementHelper.replaceFragment(this, fragment, "")
                        return
                    }
                    3 -> {//order notification
                        setTitleName(ResourceManager.getString(R.string.order_details))
                        val bundleNew = Bundle()
                        bundleNew.putInt(Constants.ID,model.orderId)
                        val fragment = OrderDetailsFragment()
                        fragment.arguments = bundleNew
                        MovementHelper.replaceFragment(this, fragment, "")
                        return
                    }
                }
            }
        }
    }

    private fun setTitleName(name: String) {
        backActionBarView = BackActionBarView(this)
        backActionBarView?.setTitle(name)
        activityBaseBinding?.llBaseActionBarContainer?.addView(backActionBarView)
    }

    private fun getBundle(fragment: Fragment?): Fragment? {
        val bundle = intent.getBundleExtra(Constants.BUNDLE)
        fragment?.setArguments(bundle)
        if (intent.hasExtra(Constants.NAME_BAR)) {
            setTitleName()
        }
        return fragment
    }

    override fun onBackPressed() {
        Log.d(TAG,"backpressed")
        val fragment = this.supportFragmentManager.findFragmentById(R.id.fl_home_container)
        if(fragment is ProductDetailsFragment){
            val intent = Intent()
            intent.putExtra(Constants.ID,fragment.viewModel.id)
            intent.putExtra(Constants.FAVOURITE,fragment.viewModel.response.data.isFavourite)
            setResult(Activity.RESULT_OK, intent);
            finish()
        }else
        super.onBackPressed()
    }

    companion object {
        private val TAG: String? = "BaseActivity"
    }
}