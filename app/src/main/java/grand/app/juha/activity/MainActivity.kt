package grand.app.juha.activity

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.simform.custombottomnavigation.SSCustomBottomNavigation
import es.dmoral.toasty.Toasty
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.ParentActivity
import grand.app.juha.customviews.actionbar.HomeActionBarView
import grand.app.juha.customviews.dialog.DialogHelper
import grand.app.juha.customviews.dialog.DialogHelperInterface
import grand.app.juha.databinding.ActivityMainBinding
import grand.app.juha.pages.account.ui.AccountSettingFragment
import grand.app.juha.pages.auth.repository.AuthRepository
import grand.app.juha.pages.cart.ui.CartFragment
import grand.app.juha.pages.departments.ui.DepartmentListFragment
import grand.app.juha.pages.home.ui.HomeFragment
import grand.app.juha.pages.main.MainViewModel
import grand.app.juha.pages.notfound.NotFoundFragment
import grand.app.juha.pages.offer.ui.OfferListFragment
import grand.app.juha.pages.search.SearchFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject

class MainActivity : ParentActivity() {

    lateinit var homeActionBarView: HomeActionBarView
    lateinit var activityMainBinding: ActivityMainBinding

    @Inject
    lateinit var viewModel: MainViewModel

    private val TAG = "MainActivity"

    @Inject
    lateinit var authRepository: AuthRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeLanguage()
        //        initializeToken();
        setContentView(R.layout.activity_main)
        val component = (applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityMainBinding.viewmodel = viewModel
//        navigationDrawerView = NavigationDrawerView(this)
//        activityMainBinding.llBaseContainer.addView(navigationDrawerView)
        setHomeActionBar()
        setTabListeners()



        activityMainBinding.bnvMainBottom.selectedItemId = R.id.action_home
        if (intent.hasExtra(Constants.DIALOG)) {
            intent.getStringExtra(Constants.DIALOG)?.let { AppHelper.showDialog(this, it) }
        } else if (intent.hasExtra(Constants.PAGE)) {
            val name = intent.getStringExtra(Constants.PAGE)
            name?.let {
                when(name){
                    CartFragment::class.java.name -> {
//                        activityMainBinding.bnvMainBottom.selectedItemId = R.id.action_cart
//                        activityMainBinding.bottomNavigation.show(4,true)
                        cartPage()
                    }
                    DepartmentListFragment::class.java.name -> {
//                        activityMainBinding.bnvMainBottom.selectedItemId = R.id.action_departments
                        goToDepartments()
                    }
                }
                MovementHelper.replaceFragment(
                    this,
                    Class.forName(name).newInstance() as Fragment,
                    name
                )
            }
        } else
            homePage()

        viewModel.getLiveData().observe(this, Observer {
            if(it.message == Constants.SEARCH){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, SearchFragment::class.java.name)
                MovementHelper.startActivityBase(this,bundle);
            }
        })

        initializeToken(authRepository)
    }


    private fun setHomeActionBar() {
        homeActionBarView = HomeActionBarView(this)
        homeActionBarView.setTitle(getString(R.string.home))
    }


    private fun setTabListeners() {

        activityMainBinding.bottomNavigation.apply {

            add(SSCustomBottomNavigation.Model(1, R.drawable.ic_sale, getString(R.string.offers)))
            add(SSCustomBottomNavigation.Model(2, R.drawable.ic_department, getString(R.string.departments)))
            add(SSCustomBottomNavigation.Model(3, R.drawable.ic_home, getString(R.string.home)))
            add( SSCustomBottomNavigation.Model(4, R.drawable.ic_cart, getString(R.string.cart)))
            add(SSCustomBottomNavigation.Model(5, R.drawable.ic_man_user, getString(R.string.account)))

            setOnShowListener {
                val name = when (it.id) {
                    1 -> "Home"
                    2 -> "Explore"
                    3 -> "Message"
                    4 -> "Notification"
                    5 -> "Account"
                    else -> ""
                }
                Log.d(TAG,"nameShow:"+name)
            }

            setOnClickMenuListener {
                val name = when (it.id) {
                    1 ->  goToOffers()
                    2 -> goToDepartments()
                    3 -> homePage()
                    4 -> cartPage()
                    5 -> myAccountPage()
                    else -> homePage()
                }
            }
        }

    }

    fun goToDepartments(){
        viewModel.searchBarVisiblity.set(true)
        viewModel.mainHeaderText(getString(R.string.main_departments))
//        activityMainBinding.bnvMainBottom.isSelected = true;
        activityMainBinding.bottomNavigation.show(2,true)
        MovementHelper.replaceFragment(
            this,
            DepartmentListFragment(),
            tag = Constants.DEPARTMENTS
        )
    }

    fun goToOffers(){
        viewModel.mainHeaderText(getString(R.string.offers))
        activityMainBinding.bottomNavigation.show(1,true)
        viewModel.searchBarVisiblity.set(true)
        MovementHelper.replaceFragment(
            this,
            OfferListFragment(),
            tag = Constants.OFFERS
        )
    }

    fun cartPage(){
        viewModel.mainHeaderText(getString(R.string.cart))
        activityMainBinding.bottomNavigation.show(4,true)
        viewModel.searchBarVisiblity.set(false)
        if (UserHelper.isLogin()) {
//            activityMainBinding.bnvMainBottom.isSelected = true;
            MovementHelper.replaceFragment(this, CartFragment(), tag = Constants.ACCOUNT)
        }else{
            MovementHelper.replaceFragment(this, NotFoundFragment(), tag = Constants.NOT_FOUND)
        }
    }

    fun myAccountPage(){
        viewModel.mainHeaderText(getString(R.string.my_account))
        viewModel.searchBarVisiblity.set(false)
//        activityMainBinding.bnvMainBottom.isSelected = true;
        activityMainBinding.bottomNavigation.show(5,true)

        MovementHelper.replaceFragment(
            this,
            AccountSettingFragment(),
            tag = Constants.ACCOUNT
        )
    }


    private fun homePage() {
        viewModel.hideMainHeader()
        viewModel.searchBarVisiblity.set(true)
        MovementHelper.replaceFragment(this, HomeFragment(), tag = Constants.HOME)
        activityMainBinding.bottomNavigation.show(3,true)
    }

    override fun onBackPressed() {
        val mngr = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val taskList = mngr.getRunningTasks(10)
        if (taskList[0].numActivities == 1 && taskList[0].topActivity?.className == this.javaClass.name) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                super.onBackPressed()
                return
            }
            val baseFragment: BaseFragment? =
                supportFragmentManager.findFragmentByTag(Constants.HOME) as BaseFragment?
            if (baseFragment != null) {
                val ids =
                    intArrayOf(R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no)
                DialogHelper.showDialogHelper(
                    this@MainActivity,
                    R.layout.dialog_logout_app,
                    ids,
                    object: DialogHelperInterface{
                        override fun OnClickListenerContinue(dialog: Dialog, view: View) {
                            when (view.id) {
                                R.id.tv_dialog_close_app_yes -> {
                                    dialog.dismiss()
                                    finish()
                                }
                                R.id.tv_dialog_close_app_no -> dialog.dismiss()
                            }
                        }
                    })
            } else {
                MovementHelper.replaceFragment(this, HomeFragment(), tag = Constants.HOME)
//                activityMainBinding.bnvMainBottom.selectedItemId = R.id.action_home
                activityMainBinding.bottomNavigation.show(3,true)
            }
            return
        } else finish()
    }

}