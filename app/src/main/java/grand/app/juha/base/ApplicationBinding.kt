package grand.app.juha.base

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.core.widget.ImageViewCompat.setImageTintList
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import de.hdodenhof.circleimageview.CircleImageView
import grand.app.juha.R
import grand.app.juha.pages.helper.ZoomFragment
import grand.app.juha.pages.home.model.Slider
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.pages.splash.models.Banner
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.slider.SliderAdapterExample
import java.util.*


private val TAG: String? = "ApplicationBinding"

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView?, image: Any?) {

    if (image is String) {
        imageView?.let {
            Glide.with(it.context)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(it)
        }
    } else if (image is Int) {
        imageView?.setImageResource(image)
    }
}

@BindingAdapter("imageBanner")
fun loadZoomImage(imageView: ImageView?, banner: Banner?) {
    //Link_type :0=> category , 1 product
    if(banner == null) imageView?.visibility = View.GONE
    else {
        imageView?.let {
            Glide
                .with(it.context)
                .load(banner?.img)
                .fitCenter() //                .placeholder(R.drawable.progress_animation)
                .into(it)
        }
        imageView?.setOnClickListener {
            banner?.let {
                when (it.linkType) {
                    0 -> {
                        val bundle = Bundle()
                        bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
                        bundle.putInt(Constants.CATEGORY_ID, it.attributeId)
                        bundle.putString(
                            Constants.URL,
                            "${URLS.CATEGORY_PRODUCT}/${it.attributeId}"
                        )
                        bundle.putBoolean(Constants.FILTER, true)
                        MovementHelper.startActivityBase(imageView.context, bundle)
                    }
                    1 -> {
                        val bundle = Bundle()
                        bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                        bundle.putInt(Constants.ID, it.attributeId)
                        bundle.putString(Constants.TITLE, "")
                        MovementHelper.startActivityBase(imageView.context, bundle)
                    }
                }
            }
        }
    }
}

@BindingAdapter("imageZoomUrl")
fun loadZoomImage(imageView: ImageView?, image: String?) {
    imageView?.let {
        Glide
            .with(it.context)
            .load(image)
            .fitCenter() //                .placeholder(R.drawable.progress_animation)
            .into(it)
    }
    imageView?.setOnClickListener {
        val bundle = Bundle()
        bundle.putString(Constants.IMAGE,image)
        bundle.putString(Constants.PAGE, ZoomFragment::class.java.name)
        MovementHelper.startActivityBase(imageView.context,bundle)
    }
}

@BindingAdapter("backgroundStatusLine")
fun setBackgroundLine(view: View, status: Int) {
    Log.d(TAG,"status:"+status);
    if(status >= 3 && status != 4 ){
        view.setBackgroundColor(ResourceManager.getColorBackground(R.color.colorPrimary))
    }else if(status == -1){
        view.setBackgroundColor(ResourceManager.getColorBackground(R.color.colorBlack))
    }else{
        Log.d(TAG,"grey color");
        view.setBackgroundColor(ResourceManager.getColorBackground(R.color.colorGraybright))
    }
}

@BindingAdapter("backgroundStatusCircle")
fun setBackgroundCircle(view: AppCompatImageView, status: Int) {
    Log.d(TAG,"status:"+status);
    if(status >= 3 && status != 4){
        view.setImageResource(R.drawable.circle_primary)
    }else if(status == -1){
        view.setImageResource(R.drawable.circle_black)
    }else{
        Log.d(TAG,"grey color")
        view.setImageResource(R.drawable.circle_grey)
    }
}

@BindingAdapter("textStyle")
fun setText(textView: TextView, isLastItem: Boolean) {
    if(isLastItem){
        textView.setTypeface(textView.typeface, Typeface.BOLD)
    }else
        textView.setTypeface(textView.typeface, Typeface.NORMAL)
}


@BindingAdapter("customWidth")
fun setLayoutHeight(view: CardView, width: Float) {
    view.layoutParams = view.layoutParams.apply { this.width = width.toInt() }
}

@BindingAdapter("color")
fun color(fab: FloatingActionButton, color: String) {
    if (color.isNotEmpty() && color[0] == '#') {
        try{
        fab.backgroundTintList = ColorStateList.valueOf(Color.parseColor(color))
        }catch (ex: Exception){
            Log.d(TAG,"invalid color:"+ex.message)
        }
    }
}



//@BindingAdapter("color")
//fun color(imageView: RelativeLayout, backgroundColor: String) {
//    val circle_primary = GradientDrawable()
//    circle_primary.circle_primary = GradientDrawable.OVAL
//    circle_primary.cornerRadii = floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f)
//    imageView.setBackgroundColor(Color.parseColor(backgroundColor))
//}


@BindingAdapter("app:adapter", "app:span", "app:orientation")
fun getItemsV2Binding(
    recyclerView: RecyclerView,
    itemsAdapter: RecyclerView.Adapter<*>?,
    spanCount: String,
    orientation: String?
) {
    recyclerView.apply {
        adapter = itemsAdapter
        setHasFixedSize(true)
        setItemViewCacheSize(30)
        layoutManager = GridLayoutManager(context,spanCount.toInt(),
            if (orientation == "1") LinearLayoutManager.VERTICAL else LinearLayoutManager.HORIZONTAL,
            false)
    }
}


@BindingAdapter("slider")
fun setSlider(sliderView: SliderView, images: ArrayList<Slider>) {
    Log.e("slider", "start")
    val sliderItems: MutableList<Slider> = ArrayList<Slider>()
    for (i in images.indices) {
        sliderItems.add(images[i])
    }
    val adapter = SliderAdapterExample(sliderView.context, sliderItems)
    sliderView.setSliderAdapter(adapter)
    sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM) //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
    //DEPTHTRANSFORMATION
    sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINDEPTHTRANSFORMATION)
    sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
    sliderView.indicatorSelectedColor = Color.WHITE
    sliderView.indicatorUnselectedColor = Color.GRAY
    sliderView.scrollTimeInSec = 3
    sliderView.isAutoCycle = true
    sliderView.startAutoCycle()
}


@BindingAdapter("ratingBar")
fun setRate(ratingBar: RatingBar?, rate: Float) {
    ratingBar?.let {
        it.rating = rate
    }
}

//@BindingAdapter("rating")
//fun setRating(ratingBar: RatingBar?, rate: Float) {
//    Log.d(TAG,"rating")
//    ratingBar?.let {
//        it.rating = rate
//    }
//}




