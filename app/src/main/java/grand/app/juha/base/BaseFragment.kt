package grand.app.juha.base

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.IOBackCommonPressed
import grand.app.juha.utils.upload.FileOperations

open class BaseFragment : Fragment() {



    lateinit var context2: Context

    private val TAG = "BaseFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        initializeProgress()
    }

    private fun getContextBase(): Context {
        return context ?: context2
    }

    protected fun pickImageDialogSelect() {
        FileOperations.pickImage(getContextBase(), this@BaseFragment, Constants.FILE_TYPE_IMAGE)
    }

    protected fun pickDocument() {
        FileOperations.pickDocument(
            if (context != null) context else context2,
            this@BaseFragment,
            Constants.FILE_TYPE_DOCUMENT
        )
    }

//    lateinit var alertDialog: AlertDialog
//    private fun initializeProgress() {
//        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
//// ...Irrelevant code for customizing the buttons and title
//// ...Irrelevant code for customizing the buttons and title
//        val inflater = this.layoutInflater
//        val dialogView: View = inflater.inflate(R.layout.loader_animation, null)
//        dialogBuilder.setView(dialogView)
//
//        alertDialog = dialogBuilder.create()
//
//    }
//
//    private fun showProgress() {
//        alertDialog.show()
//    }
//
//    private fun hideProgress() {
//        if (alertDialog.isShowing) alertDialog.dismiss()
//    }






    fun handleActions(mutable: Mutable) {
        if (mutable.message == Constants.SHOW_PROGRESS) getActivityBase().showProgress()
        else if (mutable.message == Constants.HIDE_PROGRESS)  getActivityBase().hideProgress()
        else if (mutable.message == Constants.SERVER_ERROR && mutable.obj == null) {
            getActivityBase().hideProgress()
        } else if (mutable.message == Constants.ERROR && mutable.obj is String) {
            getActivityBase().hideProgress()
        } else if (mutable.message == Constants.FAILURE_CONNECTION) {
            getActivityBase().hideProgress()
        }
        getActivityBase().handleActions(mutable)
    }

    fun showError(msg: String) {
        getActivityBase().showError(msg)
    }

    protected fun getActivityBase(): ParentActivity {
        return (if (context != null) context else context2) as ParentActivity
    }

    fun toastMessage(message: String?, icon: Int, color: Int) {
        getActivityBase().toastMessage(message, icon, color)
    }

    fun toastMessage(message: String?) {

        getActivityBase().toastMessage(
            message,
            R.drawable.ic_check_white_24dp,
            R.color.colorPrimary
        )
    }

    protected fun finishActivity() {
        getActivityBase().finish()
    }

    protected fun finishAllActivities() {
        getActivityBase().finishAffinity()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.context2 = context
    }


}