package grand.app.juha.base

import android.graphics.drawable.Drawable
import androidx.databinding.Observable
import androidx.databinding.Observable.OnPropertyChangedCallback
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import grand.app.juha.R
import grand.app.juha.model.base.Mutable
import grand.app.juha.utils.resources.ResourceManager

open class BaseViewModel : ViewModel(), Observable {
    private var mutableLiveData: MutableLiveData<Mutable> = MutableLiveData()
    private val message: ObservableField<String?>? = ObservableField()
    var show = ObservableBoolean(false)
    var noDataTextDisplay = ObservableBoolean(false)
    var noDataText: ObservableField<String> = ObservableField(ResourceManager.getString(R.string.there_are_no_data))

    private val mCallBacks: PropertyChangeRegistry?
    fun getLiveData(): MutableLiveData<Mutable> {
        return mutableLiveData
    }

    fun getMessage(): String? {
        return message?.get()
    }

    fun setMessage(message: Any?) {
        this.message?.set(message?.toString())
    }

    fun setMessageFromRes(stringRes: Int) {
        message?.set(getString(stringRes))
    }

    open fun showPage(isShow : Boolean = true) {
        show.set(isShow)
        notifyChange()
    }

    open fun noData() {
        noDataTextDisplay.set(true)
        notifyChange()
    }

    fun getString(stringRes: Int): String {
        return ResourceManager.getString(stringRes)
    }

    fun getDrawable(drawable: Int): Drawable? {
        return ResourceManager.getDrawable(drawable)
    }

    fun getStringArray(resArray: Int): Array<String?>? {
        return MyApplication.instance.resources?.getStringArray(resArray)
    }

    override fun addOnPropertyChangedCallback(callback: OnPropertyChangedCallback?) {
        mCallBacks?.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: OnPropertyChangedCallback?) {
        mCallBacks?.remove(callback)
    }

    fun notifyChange() {
        mCallBacks?.notifyChange(this, 0)
    }

    fun notifyChange(propertyId: Int) {
        mCallBacks?.notifyChange(this, propertyId)
    }

    init {
        mCallBacks = PropertyChangeRegistry()
    }
}