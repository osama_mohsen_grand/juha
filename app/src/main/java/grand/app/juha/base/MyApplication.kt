package grand.app.juha.base

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import grand.app.juha.base.di.DaggerIApplicationComponent
import grand.app.juha.base.di.IApplicationComponent

class MyApplication : MultiDexApplication() {
    var applicationComponent: IApplicationComponent? = null



    override fun onCreate() {
        super.onCreate()
        instance = this
        applicationComponent = DaggerIApplicationComponent.builder().build()
        initFacebook()
    }

    private fun initFacebook() {
        FacebookSdk.setApplicationId("1017202812023786")
        FacebookSdk.sdkInitialize(applicationContext);
        AppEventsLogger.activateApp(this);
    }


    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener?) {
        ConnectivityReceiver.Companion.connectivityReceiverListener = listener
    }

    companion object {
        lateinit var instance: MyApplication
            private set
    }

    override fun getApplicationContext(): Context {
        return super.getApplicationContext()
    }
}