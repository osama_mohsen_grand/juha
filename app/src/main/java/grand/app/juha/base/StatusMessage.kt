package grand.app.juha.base

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class StatusMessage(
){
    @SerializedName("msg")
    @Expose
    var mMessage: String = ""

//    @SerializedName("code")
//    @Expose
//    var code : Int
//
    @SerializedName("status")
    @Expose
    var status: String = ""

    @SerializedName("code")
    @Expose
    var code: Int = 0
}