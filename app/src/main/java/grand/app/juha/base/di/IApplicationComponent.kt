package grand.app.juha.base.di

import dagger.Component
import grand.app.juha.pages.social.SocialFragment
import grand.app.juha.activity.BaseActivity
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.LiveData
import grand.app.juha.pages.auth.code.VerifyCodeFragment
import grand.app.juha.pages.auth.forgetpassword.ForgetPasswordFragment
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.pages.auth.register.RegisterFragment
import grand.app.juha.pages.splash.SplashFragment
//import grand.app.juha.activity.MainActivity
import grand.app.juha.connection.ConnectionModule
import grand.app.juha.pages.account.ui.AccountSettingFragment
import grand.app.juha.pages.address.add.AddAddressFragment
import grand.app.juha.pages.address.list.ui.AddressListFragment
import grand.app.juha.pages.address.list.ui.NoAddressFragment
import grand.app.juha.pages.auth.changepassword.ChangePasswordFragment
import grand.app.juha.pages.cart.ui.CheckoutFragment
import grand.app.juha.pages.cart.ui.CartFragment
import grand.app.juha.pages.departments.ui.DepartmentListFragment
import grand.app.juha.pages.filter.FilterFragment
import grand.app.juha.pages.home.ui.HomeFragment
import grand.app.juha.pages.intro.IntroFragment
import grand.app.juha.pages.notfound.NotFoundFragment
import grand.app.juha.pages.offer.ui.OfferDetailsFragment
import grand.app.juha.pages.offer.ui.OfferListFragment
import grand.app.juha.pages.order.ui.OrderDetailsFragment
import grand.app.juha.pages.order.ui.OrderListFragment
import grand.app.juha.pages.order.ui.RefundFragment
import grand.app.juha.pages.order.ui.ProductsRefundFragment
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.pages.rates.RatesFragment
import grand.app.juha.pages.search.SearchFragment
import grand.app.juha.pages.settings.complain.ComplainFragment
import grand.app.juha.pages.settings.helper.HelperSettingsFragment
import grand.app.juha.pages.settings.notification.NotificationListFragment
import grand.app.juha.pages.settings.term.ui.SettingsFragment
import grand.app.juha.pages.settings.term.ui.TermsFragment
import grand.app.juha.pages.wallet.WalletFragment
//import grand.app.juha.pages.splash.SplashFragment
import javax.inject.Singleton

//Component refer to an interface or waiter for make an coffee cup to me
@Singleton
@Component(modules = [ConnectionModule::class, LiveData::class])
interface IApplicationComponent {
//    open fun inject(mainActivity: MainActivity?)
    fun inject(tmpActivity: BaseActivity)
    fun inject(tmpActivity: SplashFragment)
    fun inject(mainActivity: MainActivity)
    fun inject(registerFragment: RegisterFragment)
    fun inject(forgetPasswordFragment: ForgetPasswordFragment)
    fun inject(loginFragment: LoginFragment)
    fun inject(verifyCodeFragment: VerifyCodeFragment)
    fun inject(changePasswordFragment: ChangePasswordFragment)
    fun inject(addAddressFragment: AddAddressFragment)
    fun inject(addressListFragment: AddressListFragment)
    fun inject(settingsFragment: SettingsFragment)
    fun inject(complainFragment: ComplainFragment)
    fun inject(notificationListFragment: NotificationListFragment)
    fun inject(socialFragment: SocialFragment)
    fun inject(introFragment: IntroFragment)
    fun inject(termsFragment: TermsFragment)
    fun inject(offerListFragment: OfferListFragment)
    fun inject(homeFragment: HomeFragment)
    fun inject(accountSettingFragment: AccountSettingFragment)
    fun inject(offerDetailsFragment: OfferDetailsFragment)
    fun inject(productListFragment: ProductListFragment)
    fun inject(helperSettingsFragment: HelperSettingsFragment)
    fun inject(productDetailsFragment: ProductDetailsFragment)
    fun inject(ratesFragment: RatesFragment)
    fun inject(departmentListFragment: DepartmentListFragment)
    fun inject(cartFragment: CartFragment)
    fun inject(checkoutFragment: CheckoutFragment)
    fun inject(noAddressFragment: NoAddressFragment)
    fun inject(orderListFragment: OrderListFragment)
    fun inject(orderDetailsFragment: OrderDetailsFragment)
    fun inject(productsRefundFragment: ProductsRefundFragment)
    fun inject(refundFragment: RefundFragment)
    fun inject(searchFragment: SearchFragment)
    fun inject(filterFragment: FilterFragment)
    fun inject(walletFragment: WalletFragment)
    fun inject(notFoundFragment: NotFoundFragment)

    @Component.Builder
    interface Builder {
        open fun build(): IApplicationComponent?
    }
}