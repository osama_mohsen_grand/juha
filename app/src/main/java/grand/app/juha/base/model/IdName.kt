package grand.app.juha.base.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class IdName(
    var id: Int,
    var name: String
) : Serializable