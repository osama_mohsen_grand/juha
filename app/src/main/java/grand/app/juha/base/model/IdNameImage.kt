package grand.app.juha.base.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class IdNameImage(
    @SerializedName("id")
    var id: Int,
    @SerializedName("img")
    var img: String
) : Serializable