package grand.app.juha.connection

interface ConnectionListenerInterFace {
    open fun onRequestResponse(response: Any?)
}