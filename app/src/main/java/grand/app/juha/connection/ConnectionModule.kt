package grand.app.juha.connection

import android.util.Log
import dagger.Module
import dagger.Provides
import grand.app.juha.utils.URLS
import grand.app.juha.utils.session.LanguagesHelper
import grand.app.juha.utils.session.UserHelper
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Elmohandes on 20/09/2018.
 */
@Module
class ConnectionModule {
    private var retrofit: Retrofit? = null

    @Singleton
    @Provides
    fun webService(): Api? {
        if (retrofit == null) {
            Log.d(TAG, "retrofit")
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            val okHttpClientBuilder = OkHttpClient.Builder() //.addInterceptor(interceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor { chain ->
//                    val newRequest = chain.request().newBuilder()
//                        .addHeader("jwt",UserHelper.getUserDetails().jwt)
//                        .addHeader("Lang",LanguagesHelper.getCurrentLanguage())
//                        .build()
//                    chain.proceed(newRequest)
                    val newRequest = chain.request().newBuilder()
                    if(UserHelper.getUserId() != -1)
                        newRequest.addHeader("jwt",UserHelper.getUserDetails().jwt)
                    newRequest.addHeader("Lang",LanguagesHelper.getCurrentLanguage())
                    chain.proceed(newRequest.build())

                }.addInterceptor(interceptor)

            retrofit = Retrofit.Builder()
                .baseUrl(URLS.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build()
        }
        return retrofit?.create(Api::class.java)
    }

    companion object {
        var bufferSize = 256 * 1024
        private val TAG: String? = "ConnectionModule"
    }

    /*

     */
}