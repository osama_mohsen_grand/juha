package grand.app.juha.customviews.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import grand.app.juha.R

class DialogHelper {

    companion object {

        lateinit var context: Activity
        lateinit var showDialog: Dialog

        fun DialogHelper(context: Activity) {
            this.context = context
        }


        fun SetViewLayout(dialog: Int): View {
            val displayRectangle = Rect()
            val window = context.window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)

            // inflate and adjust layout
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(dialog, null)
            view.minimumWidth = (displayRectangle.width() * 0.9f).toInt()
            return view

        }

        fun setView(context: Context?, view: View?) {
            try {
                val builder =
                    AlertDialog.Builder(context!!, R.style.customDialog)
                builder.setView(view)
                showDialog = builder.create()
                showDialog!!.show()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        // Best Array Dialog
        fun showDialogHelper(
            context: Context?,
            layout: Int,
            ids: IntArray,
            dialogHelperInterface: DialogHelperInterface
        ) {
            try {
                val dialogHelper = DialogHelper(context as Activity)

                val view: View = SetViewLayout(layout)
                for (id in ids) {
                    val view_id = view.findViewById<View>(id)
                    view_id.setOnClickListener {
                        dialogHelperInterface.OnClickListenerContinue(
                            showDialog,
                            view_id
                        )
                    }
                }
                setView(context, view)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


        // Best Array Dialog
        fun showDialogHelper(
            context: Context?,
            layout: Int,
            ids: IntArray,
            text: String,
            dialogHelperInterface: DialogHelperInterface
        ) {
            try {
                val dialogHelper = DialogHelper(context as Activity)

                val view: View = SetViewLayout(layout)
                for (id in ids) {
                    val view_id = view.findViewById<View>(id)
                    view_id.setOnClickListener {
                        dialogHelperInterface.OnClickListenerContinue(
                            showDialog,
                            view_id
                        )
                    }
                }

                val textView:TextView = view.findViewById(R.id.tv_checked_out_message)
                textView.text = text

                setView(context, view)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


    }

}