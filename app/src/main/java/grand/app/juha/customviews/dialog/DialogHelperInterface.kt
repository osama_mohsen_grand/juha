package grand.app.juha.customviews.dialog

import android.app.Dialog
import android.view.View

interface DialogHelperInterface {
    fun OnClickListenerContinue(
        dialog: Dialog,
        view: View
    )
}