package grand.app.juha.customviews.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.content.res.ResourcesCompat
import grand.app.juha.R

class CustomCheckBox (context: Context, attributeSet: AttributeSet) :
    AppCompatCheckBox(context, attributeSet) {

    init {

        val font: Typeface? = ResourcesCompat.getFont(context, R.font.bold);

        typeface = font

//        movementMethod = ScrollingMovementMethod()
    }

}