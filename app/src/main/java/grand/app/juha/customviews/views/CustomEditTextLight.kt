package grand.app.juha.customviews.views

import android.content.Context
import android.graphics.Typeface
import android.text.InputType
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.textfield.TextInputEditText
import grand.app.juha.R
import grand.app.juha.utils.resources.ResourceManager

class CustomEditTextLight  (context: Context, attributeSet: AttributeSet) : TextInputEditText(context,attributeSet)  {
    init {
        val font: Typeface? = ResourcesCompat.getFont(context, R.font.regular);
//        font = Typeface.createFromAsset(context.assets, "regular.otf")
        typeface = font
        background = ResourceManager.getDrawable(R.drawable.shadow_rectangle_edittext)
//        textAlignment = View.TEXT_ALIGNMENT_VIEW_START
//        textDirection = View.TEXT_DIRECTION_RTL
    }
}
