package grand.app.juha.customviews.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import grand.app.juha.R

class CustomTextViewRegular(context: Context, attributeSet: AttributeSet) :
    AppCompatTextView(context, attributeSet) {

    init {
        val font: Typeface? = ResourcesCompat.getFont(context, R.font.regular);
        typeface = font
//        movementMethod = ScrollingMovementMethod()
    }

}