package grand.app.juha.pages.account

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.databinding.ItemSettingBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.account.model.AccountModel
import grand.app.juha.pages.account.viewmodel.ItemAccountViewModel
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.pages.settings.term.ui.SettingsFragment
import grand.app.juha.pages.settings.term.ui.TermsFragment
import grand.app.juha.pages.social.SocialFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper

class AccountSettingAdapter : RecyclerView.Adapter<AccountSettingAdapter.ViewHolder>() {
    // fetch list data
    private lateinit var modelList: ArrayList<AccountModel>
    private lateinit var activity: FragmentActivity
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSettingBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_setting,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    fun getActivity(): FragmentActivity {
        return activity
    }

    fun setActivity(activity: FragmentActivity) {
        this.activity = activity
    }

    private val TAG = "AccountSettingAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            val model: AccountModel = modelList[mutable.position]
            val bundle = Bundle()
            if (model.isAuthorize) {
                Log.d("mutable ", "${mutable.message}")
                Log.d(TAG,"${model.title}")
                when (model.title) {
                    ResourceManager.getString(R.string.rate_app) -> {
                        AppHelper.openBrowser(
                            holder.itemView.context,
                            AppHelper.getPlayStoreLink(holder.itemView.context)
                        )
                    }
                    ResourceManager.getString(R.string.share_app) -> {
                        //share app
                        AppHelper.shareUrl(
                            AppHelper.getPlayStoreLink(holder.itemView.context),
                            holder.itemView.context
                        )
                    }
                    ResourceManager.getString(R.string.logout) -> {
                        UserHelper.clearUserDetails()
                        MovementHelper.startActivity(
                            holder.itemView.context,MainActivity::class.java
                        )
                    }
                    else -> {
                        bundle.putString(Constants.PAGE, model.fragment)
                        Log.d(TAG,model.fragment)
                        if (model.fragment == ProductListFragment::class.java.name) {
                            bundle.putString(Constants.URL, URLS.FAVOURITE)
                        }

                        val titleBar = when(model.fragment){
                            SocialFragment::class.java.name,
                            TermsFragment::class.java.name,
                            SettingsFragment::class.java.name -> ""
                            else -> model.title
                        }

                        Log.d(TAG,titleBar)
                        MovementHelper.startActivityBase(
                            holder.itemView.context,
                            bundle,
                            titleBar
                        )
                    }
                }
            } else {
                //not authorized
                bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                MovementHelper.startActivityBase(
                    holder.itemView.context,
                    bundle,
                    ResourceManager.getString(R.string.login)
                )
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return if (::modelList.isInitialized) modelList.size else 0
    }

    fun update(modelList: ArrayList<AccountModel>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(itemCount, modelList.size - 1);
    }

    class ViewHolder(private val binding: ItemSettingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemAccountViewModel
        fun bind(model: AccountModel) {
            viewModel = ItemAccountViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}