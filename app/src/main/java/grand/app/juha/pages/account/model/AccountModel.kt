package grand.app.juha.pages.account.model

import androidx.fragment.app.Fragment

data class AccountModel(var title: String, var drawable : Int, var fragment: String = "", val type : Int = 0,val isAuthorize:Boolean = true) {
}