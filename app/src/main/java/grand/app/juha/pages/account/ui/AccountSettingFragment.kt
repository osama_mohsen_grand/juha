package grand.app.juha.pages.account.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.facebook.login.LoginFragment
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentAccountSettingsBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.account.viewmodel.AccountSettingViewModel
import grand.app.juha.pages.auth.register.RegisterFragment
import grand.app.juha.pages.social.model.SocialResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject

class AccountSettingFragment : BaseFragment() {

    private lateinit var binding: FragmentAccountSettingsBinding

    @Inject
    lateinit var viewModel: AccountSettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account_settings, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        viewModel.adapter.setActivity(requireActivity())
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            when (mutable.message) {
                URLS.SOCIALS -> {
                    viewModel.setData(mutable.obj as SocialResponse)
                }
                Constants.UPDATE_PROFILE -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle, getString(R.string.personal_page))
                }
                Constants.TWITTER -> {
                    AppHelper.openBrowser(context, viewModel.twitter)
                }
                Constants.FACEBOOK -> {
                    AppHelper.openBrowser(context, viewModel.facebook)
                }
                Constants.WHATSAPP -> {
                    AppHelper.openWhats(requireActivity(), viewModel.whatsapp)
                }
                Constants.LOGIN -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle,getString(R.string.login))
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }

}


