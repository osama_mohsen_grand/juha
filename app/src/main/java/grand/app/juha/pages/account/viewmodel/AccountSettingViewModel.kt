package grand.app.juha.pages.account.viewmodel

import android.util.Log
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.account.AccountSettingAdapter
import grand.app.juha.pages.account.model.AccountModel
import grand.app.juha.pages.address.list.ui.AddressListFragment
import grand.app.juha.pages.auth.login.model.User
import grand.app.juha.pages.order.ui.OrderListFragment
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.pages.settings.helper.HelperSettingsFragment
import grand.app.juha.pages.settings.notification.NotificationListFragment
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.pages.social.model.SocialResponse
import grand.app.juha.pages.wallet.WalletFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AccountSettingViewModel : BaseViewModel {
    //    @Inject
    lateinit var repository: SettingsRepository;
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = AccountSettingAdapter()
    var bottomList = arrayListOf<AccountModel>()
    var user: User = UserHelper.getUserDetails()
    var twitter = ""
    var facebook = ""
    var whatsapp = ""

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        repository.setLiveData(getLiveData())
        repository.getSocials()
    }

    init {
        bottomList.add(
            AccountModel(
                getString(R.string.addresses),
                R.drawable.ic_addresses,
                AddressListFragment::class.java.name,
                isAuthorize = UserHelper.isLogin()
            )
        )
        bottomList.add(AccountModel(getString(R.string.my_requests), R.drawable.ic_my_requests, OrderListFragment::class.java.name,isAuthorize = UserHelper.isLogin()))
        bottomList.add(AccountModel(getString(R.string.wallet), R.drawable.ic_wallet, WalletFragment::class.java.name,isAuthorize = UserHelper.isLogin()))
        bottomList.add(AccountModel(getString(R.string.favourite), R.drawable.ic_favourite,ProductListFragment::class.java.name,isAuthorize = UserHelper.isLogin()))
        bottomList.add(
            AccountModel(
                getString(R.string.notifications), R.drawable.ic_notification,
                NotificationListFragment::class.java.name,
                isAuthorize = true
            )
        )
        bottomList.add(
            AccountModel(
                getString(R.string.help),
                R.drawable.ic_help,
                HelperSettingsFragment::class.java.name,
                isAuthorize = true
            )
        )
        if (UserHelper.getUserId() == -1)
            bottomList.add(
                AccountModel(
                    getString(R.string.login), R.drawable.ic_logout,
                    grand.app.juha.pages.auth.login.LoginFragment::class.java.name
                )
            )
        else
            bottomList.add(
                AccountModel(
                    getString(R.string.logout), R.drawable.ic_logout,
                    ""
                )
            )

        adapter.update(bottomList)
    }

    private val TAG = "AccountSettingViewModel"
    fun updateProfile() {
        if(UserHelper.isLogin())
            getLiveData().value = Mutable(Constants.UPDATE_PROFILE)
        else
            getLiveData().value = Mutable(Constants.LOGIN)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun facebook() {
        getLiveData().value = Mutable(Constants.FACEBOOK)
    }

    fun twitter() {
        getLiveData().value = Mutable(Constants.TWITTER)

    }

    fun whatsapp() {
        getLiveData().value = Mutable(Constants.WHATSAPP)
    }


    public fun setData(response: SocialResponse) {
        showPage(true)
        for (i in 0 until response.social.size) {
            when (response.social[i].name) {
                "facebook" -> facebook = response.social[i].text
                "twitter" -> twitter = response.social[i].text
                "whatsapp" -> whatsapp = response.social[i].text
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}

