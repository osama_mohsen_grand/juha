package grand.app.juha.pages.account.viewmodel

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.account.model.AccountModel
import grand.app.juha.utils.Constants

data class ItemAccountViewModel(val model : AccountModel, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}