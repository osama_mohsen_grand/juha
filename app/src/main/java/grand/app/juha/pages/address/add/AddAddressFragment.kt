package grand.app.juha.pages.address.add

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.protobuf.LazyStringArrayList
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.base.model.IdName
import grand.app.juha.databinding.FragmentAddressAddBinding
import grand.app.juha.pages.address.list.model.AddAddressResponse
import grand.app.juha.pages.address.list.model.AreaListResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.popup.PopUpInterface
import grand.app.juha.utils.popup.PopUpMenuHelper
import javax.inject.Inject

class AddAddressFragment : BaseFragment() {
    private lateinit var binding: FragmentAddressAddBinding
    @Inject
    lateinit var viewModel: AddAddressViewModel

    val popUpMenuHelper = PopUpMenuHelper()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_address_add, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message+",")
            when(mutable.message){
                URLS.ADD_ADDRESS ->{
                    toastMessage((mutable.obj as AddAddressResponse).mMessage)
                    val intent = Intent()
                    intent.putExtra(Constants.ADDRESS, (mutable.obj as AddAddressResponse).data)
                    requireActivity().setResult(Activity.RESULT_OK, intent);
                    requireActivity().finish()
                }
                URLS.ALL_AREA_DATA -> {
                    viewModel.setData(mutable.obj as AreaListResponse)
                    setViewListeners()
                }
            }
        })

    }

    private fun setViewListeners() {
        binding.edtAddAddressGovernment.setOnClickListener {
            val arr = LazyStringArrayList()
            for(item in viewModel.areaListResponse.data.governments) arr.add(item.name)
            popUpMenuHelper.openPopUp(requireActivity(),binding.edtAddAddressGovernment,arr,
                object : PopUpInterface {
                    override fun submitPopUp(position: Int) {
                        binding.edtAddAddressGovernment.setText(viewModel.areaListResponse.data.governments[position].name)
                        viewModel.request.governorateId = viewModel.areaListResponse.data.governments[position].id
                        viewModel.request.areaId = -1
                        viewModel.request.cityId = -1
                        binding.edtAddAddressCity.setText("")
                        binding.edtAddAddressArea.setText("")
                    }
                })
        }

        binding.edtAddAddressCity.setOnClickListener {
            val arr = ArrayList<IdName>()
            for(item in viewModel.areaListResponse.data.cities)
                if (item.governmentId == viewModel.request.governorateId)
                    arr.add(IdName(item.id, item.name))
            popUpMenuHelper.openPopUpIdName(requireActivity(),binding.edtAddAddressCity,arr,
                object : PopUpInterface {
                    override fun submitPopUp(position: Int) {
                        binding.edtAddAddressCity.setText(arr[position].name)
                        viewModel.request.cityId = arr[position].id
                        viewModel.request.areaId = -1
                        binding.edtAddAddressArea.setText("")
                    }
                })
        }

        binding.edtAddAddressArea.setOnClickListener {
            val arr = ArrayList<IdName>()
            for(item in viewModel.areaListResponse.data.areas)
                if (item.cityId == viewModel.request.cityId)
                    arr.add(IdName(item.id, item.name))
            popUpMenuHelper.openPopUpIdName(requireActivity(),binding.edtAddAddressArea,arr,
                object : PopUpInterface {
                    override fun submitPopUp(position: Int) {
                        binding.edtAddAddressArea.setText(arr[position].name)
                        viewModel.request.areaId = arr[position].id
                    }
                })
        }
    }


    private val TAG = "AddAddressFragment"

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
        viewModel.settingsRepository.setLiveData(viewModel.liveDataViewModel)
    }
}