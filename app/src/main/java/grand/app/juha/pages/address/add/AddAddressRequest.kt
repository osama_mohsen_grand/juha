package grand.app.juha.pages.address.add

import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.validation.Validate
import java.io.Serializable

class AddAddressRequest() : Serializable {



    @SerializedName("governorate_id")
    @Expose
    var governorateId = -1

    @SerializedName("city_id")
    @Expose
    var cityId = -1

    @SerializedName("area_id")
    @Expose
    var areaId = -1

    @SerializedName("first_name")
    @Expose
    var firstName = ""
        get() = field
        set(value) {
            field = value
            this.firstNameError.set("")
        }


    @SerializedName("last_name")
    @Expose
    var lastName = ""
        get() = field
        set(value) {
            field = value
            lastNameError.set("")
        }

    @SerializedName("phone")
    @Expose
    var phone1 = ""
        get() = field
        set(value) {
            field = value
            phone1Error.set("")
        }

    @SerializedName("another_phone")
    @Expose
    var phone2 = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName("gov")
    @Expose
    var gov = ""
        get() = field
        set(value) {
            field = value
            govError.set("")
        }

    @SerializedName("city")
    @Expose
    var city = ""
        get() = field
        set(value) {
            field = value
            cityError.set("")
        }


    @SerializedName("area")
    @Expose
    var area = ""
        get() = field
        set(value) {
            field = value
            areaError.set("")
        }

    @SerializedName("st")
    @Expose
    var st = ""
        get() = field
        set(value) {
            field = value
            streetNoError.set("")
        }

    @SerializedName("building_no")
    @Expose
    var build_no = ""
        get() = field
        set(value) {
            field = value
            buildNoError.set("")
        }


    @SerializedName("floor")
    @Expose
    var floor = ""
        get() = field
        set(value) {
            field = value
            floorError.set("")
        }

    @SerializedName("mark")
    @Expose
    var specialArea = ""
        get() = field
        set(value) {
            field = value
            specialAreaError.set("")
        }


    @Transient
    lateinit var lastNameError: ObservableField<String>

    @Transient
    lateinit var firstNameError: ObservableField<String>


    @Transient
    lateinit var phone1Error: ObservableField<String>

    @Transient
    lateinit var govError: ObservableField<String>

    @Transient
    lateinit var cityError: ObservableField<String>

    @Transient
    lateinit var areaError: ObservableField<String>

    @Transient
    lateinit var streetNoError: ObservableField<String>


    @Transient
    lateinit var buildNoError: ObservableField<String>

    @Transient
    lateinit var floorError: ObservableField<String>

    @Transient
    lateinit var specialAreaError: ObservableField<String>


    init {
        firstNameError = ObservableField()
        lastNameError = ObservableField()
        phone1Error = ObservableField()
        govError = ObservableField()
        cityError = ObservableField()
        areaError = ObservableField()
        streetNoError = ObservableField()
        buildNoError = ObservableField()
        specialAreaError = ObservableField()
        floorError = ObservableField()

    }

    private val TAG = "AddAddressRequest"

    fun isValid(): Boolean {
        var valid = true
        if (!Validate.isValid(firstName)) {
            Log.d(TAG,"error:firstName")
            this.firstNameError.set(Validate.error)
            valid = false
        } else this.firstNameError.set("")

        if (!Validate.isValid(lastName)) {
            Log.d(TAG,"error:lastName")
            lastNameError.set(Validate.error)
            valid = false
        } else lastNameError.set("")

        if (!Validate.isValid(city)) {
            Log.d(TAG,"error:city")
            cityError.set(Validate.error)
            valid = false
        } else cityError.set("")

        if (!Validate.isValid(gov)) {
            Log.d(TAG,"error:street_no")
            this.govError.set(Validate.error)
            valid = false
        } else this.govError.set("")

        if (!Validate.isValid(st)) {
            Log.d(TAG,"error:street_no")
            streetNoError.set(Validate.error)
            valid = false
        } else streetNoError.set("")


        if (!Validate.isValid(area)) {
            Log.d(TAG,"error:area")
            areaError.set(Validate.error)
            valid = false
        } else areaError.set("")

        if (!Validate.isValid(build_no)) {
            Log.d(TAG,"error:build_no")
            buildNoError.set(Validate.error)
            valid = false
        } else buildNoError.set("")

        if (!Validate.isValid(specialArea)) {
            Log.d(TAG,"error:specialArea")
            specialAreaError.set(Validate.error)
            valid = false
        } else specialAreaError.set("")

        if (!Validate.isValid(floor)) {
            Log.d(TAG,"error:doorNo")
            floorError.set(Validate.error)
            valid = false
        } else floorError.set("")


        if (!Validate.isValid(phone1, Constants.PHONE)) {
            Log.d(TAG,"error:phone1")
            phone1Error.set(Validate.error)
            valid = false
        } else phone1Error.set("")

        return valid
    }


}