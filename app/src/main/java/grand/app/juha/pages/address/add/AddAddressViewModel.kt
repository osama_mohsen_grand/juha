package grand.app.juha.pages.address.add

import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.connection.FileObject
import grand.app.juha.pages.address.list.model.AreaListResponse
import grand.app.juha.pages.repository.UserRepository
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddAddressViewModel : BaseViewModel {
    @Inject
    lateinit var repository: UserRepository;

    @Inject
    lateinit var settingsRepository: SettingsRepository;

    var editable: ObservableBoolean = ObservableBoolean(true)
    var cities = arrayListOf<String>()

    var compositeDisposable: CompositeDisposable
    var request: AddAddressRequest

    val user = UserHelper.getUserDetails()

    lateinit var areaListResponse: AreaListResponse
    
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
//    var settingsLiveDataViewModel: MutableLiveData<Mutable>
    var x: Int = 6
    var file : FileObject? = null

    @Inject
    constructor(repository: UserRepository,settingsRepository : SettingsRepository) {
        this.repository = repository
        this.settingsRepository = settingsRepository
        this.liveDataViewModel = MutableLiveData()
//        this.settingsLiveDataViewModel = MutableLiveData()
        compositeDisposable = CompositeDisposable()
        request = AddAddressRequest()
        repository.setLiveData(liveDataViewModel)
//        settingsRepository.setLiveData(settingsLiveDataViewModel)
        settingsRepository.getAreaData()
        
    }

    fun submit() {
        if (request.isValid()) {
            repository.addAddress(request)
        }
    }

    private val TAG = "AddAddressViewModel"

    fun setData(response: AreaListResponse){
        this.areaListResponse = response
        Log.d(TAG,areaListResponse.mMessage)
        Log.d(TAG,areaListResponse.status)

        showPage(true)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}