package grand.app.juha.pages.address.list.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import java.io.Serializable

data class AddAddressResponse (
    @SerializedName("data")
    @Expose
    var data: Address
): StatusMessage() , Serializable