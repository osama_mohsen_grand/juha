package grand.app.juha.pages.address.list.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Address(
    @SerializedName("another_phone")
    var anotherPhone: String = "",
    @SerializedName("area")
    var area: Area = Area(),
    @SerializedName("building_no")
    var buildingNo: String = "",
    @SerializedName("city")
    var city: City = City(),
    @SerializedName("first_name")
    var firstName: String = "",
    @SerializedName("floor")
    var floor: String = "",
    @SerializedName("governorate")
    var governorate: Governorate = Governorate(),
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("last_name")
    var lastName: String = "",
    @SerializedName("mark")
    var mark: String = "",
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("st")
    var st: String = ""
) : Serializable