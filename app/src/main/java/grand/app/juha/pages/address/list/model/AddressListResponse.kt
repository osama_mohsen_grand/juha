package grand.app.juha.pages.address.list.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class AddressListResponse (
    @SerializedName("data")
    @Expose
    var data: ArrayList<Address> = arrayListOf(),
    @SerializedName("wallet")
    @Expose
    var wallet: Double = 0.0,
    @SerializedName("address_id")
    @Expose
    var addressId: Int = -1
): StatusMessage()