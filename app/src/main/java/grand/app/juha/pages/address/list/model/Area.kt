package grand.app.juha.pages.address.list.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Area(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("city_id")
    @Expose
    var cityId: Int = 0

): Serializable