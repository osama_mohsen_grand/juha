package grand.app.juha.pages.address.list.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import java.io.Serializable

class AreaData(
    @SerializedName("governorates")
    var governments: ArrayList<Governorate>,
    @SerializedName("cities")
    var cities: ArrayList<City>,
    @SerializedName("areas")
    var areas: ArrayList<Area>
):StatusMessage(), Serializable {
}