package grand.app.juha.pages.address.list.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import java.io.Serializable

class AreaListResponse(
    @SerializedName("data")
    var data: AreaData
):StatusMessage() , Serializable {
}