package grand.app.juha.pages.address.list.model


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Governorate(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = ""
): Serializable