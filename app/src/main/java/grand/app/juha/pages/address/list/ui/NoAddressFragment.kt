package grand.app.juha.pages.address.list.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentAddressListBinding
import grand.app.juha.databinding.FragmentNoAddressBinding
import grand.app.juha.pages.address.add.AddAddressFragment
import grand.app.juha.pages.address.list.viewmodel.AddressListViewModel
import grand.app.juha.pages.address.list.model.Address
import grand.app.juha.pages.address.list.model.AddressListResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject

class NoAddressFragment : BaseFragment() {
    private lateinit var binding: FragmentNoAddressBinding

    private val TAG = this::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_no_address, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
//        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        binding.tvNoAddressAdd.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(Constants.PAGE, AddAddressFragment::class.java.name)
            MovementHelper.startActivityBaseForResult(this, bundle,Constants.ADDRESS_RESULT, getString(R.string.add_new_address))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG,"onActivityResult")
    }

    override fun onResume() {
        super.onResume()
    }
}