package grand.app.juha.pages.address.list.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.address.list.AddressAdapter
import grand.app.juha.pages.address.list.model.AddressListResponse
import grand.app.juha.pages.repository.UserRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddressListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: UserRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = this::class.java.name
    var response: AddressListResponse = AddressListResponse()
    var adapter: AddressAdapter = AddressAdapter()
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: UserRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        getAddressList()
    }


    private fun getAddressList() {
        Log.d(TAG,"getAddressList")
        repository.getAddresses()
    }

    fun addAddress(){
        Log.d(TAG,"addAddress")
        liveDataViewModel.value = Mutable(Constants.ADD_ADDRESS)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }


    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapter.update(response.data)
        show.set(true)
        Log.d(TAG,"done setData")
    }

}