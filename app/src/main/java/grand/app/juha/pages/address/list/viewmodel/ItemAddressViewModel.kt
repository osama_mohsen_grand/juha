package grand.app.juha.pages.address.list.viewmodel

import android.util.Log
import android.widget.RadioGroup
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.pages.address.list.model.Address
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager


data class ItemAddressViewModel(
    val model: Address,
    var position: Int,
    var selected: Boolean
) {
    var mutableLiveData = MutableLiveData<Any>()
    var name: String = "${ResourceManager.getString(R.string.name)} : ${model.firstName} ${model.lastName}"
    var address: String = "${ResourceManager.getString(R.string.address)} : ${model.governorate.name} - ${model.city.name} - ${model.area.name} - ${model.st}"
    var phone: String = "${ResourceManager.getString(R.string.phone)} : ${model.phone}"

    private  val TAG = "ItemAddressViewModel"
    fun submit(){
        Log.d(TAG,"submit")
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }

    fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)

    }

}