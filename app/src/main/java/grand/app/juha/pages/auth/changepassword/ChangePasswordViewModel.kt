package grand.app.juha.pages.auth.changepassword

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.auth.repository.AuthRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ChangePasswordViewModel : BaseViewModel {

    @Inject
    lateinit var repository: AuthRepository;
    var compositeDisposable: CompositeDisposable
    var request: ChangePasswordRequest

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>

    @Inject
    constructor(repository: AuthRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        request = ChangePasswordRequest()
        repository.setLiveData(liveDataViewModel)
    }


    fun submit() {
        if (request.isValid()) {
            repository.changePassword(request)
        }
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    private fun unSubscribeFromObservable() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setArgument(bundle: Bundle) {
        if (bundle.containsKey(Constants.PHONE))
            request.phone = bundle.getString(Constants.PHONE).toString()
    }


//    lateinit var liveData: MutableLiveData<Mutable> = null
//    var isLogin = ObservableBoolean(false)
//
//    @Inject
//    var repository: AuthRepository? = null
//    private val compositeDisposable: CompositeDisposable? = CompositeDisposable()
//    var request: ChangePasswordRequest? = null
//
//    @Inject
//    fun ChangePasswordViewModel(repository: AuthRepository) {
//        this.repository = repository
//        liveData = MutableLiveData<Mutable>()
//        repository.setLiveData(liveData)
//        request = ChangePasswordRequest()
//        if (UserHelper.getUserId() !== -1) isLogin.set(true)
//    }
//
//    fun submit() {
//        if (request.isValid()) {
//            repository.changePassword(request)
//        }
//    }
//
//    private fun unSubscribeFromObservable() {
//        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
//            compositeDisposable.dispose()
//        }
//    }
//
//    override fun onCleared() {
//        super.onCleared()
//        unSubscribeFromObservable()
//    }
}