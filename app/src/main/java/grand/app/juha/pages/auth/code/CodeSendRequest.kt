package grand.app.juha.pages.auth.code

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CodeSendRequest(@SerializedName("phone") var phone : String,@SerializedName("code") val code : String = "20"): Serializable {
}