package grand.app.juha.pages.auth.code

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentVerifyCodeBinding
import grand.app.juha.base.StatusMessage
import grand.app.juha.pages.auth.login.model.LoginResponse
import grand.app.juha.pages.auth.changepassword.ChangePasswordFragment
import grand.app.juha.pages.auth.login.model.User
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject

class VerifyCodeFragment : BaseFragment() {
    private lateinit var binding: FragmentVerifyCodeBinding

    @Inject
    lateinit var viewModel: VerifyCodeViewModel


    private val TAG = "VerifyCodeFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_code, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == Constants.SUCCESS) {
                Log.d(TAG,viewModel.type)
                if (viewModel.type == Constants.FORGET_PASSWORD) {
                    Log.d(TAG,"here is forget password")
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, ChangePasswordFragment::class.java.name)
                    bundle.putString(Constants.PHONE, viewModel.request.phone)
                    MovementHelper.startActivityBase(
                        context,
                        bundle)
                }
                else {
                    Log.d(TAG,"here else")
                    val user : User = ((mutable.obj) as LoginResponse).user
                    UserHelper.saveUserDetails(user)
                    finishAllActivities()
                    startActivity(Intent(context,MainActivity::class.java))
                }
            }else if(mutable.message == URLS.SEND_CODE){
                toastMessage((mutable.obj as StatusMessage).mMessage)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}