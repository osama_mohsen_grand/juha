package grand.app.juha.pages.auth.code

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VerifyCodeRequest(@SerializedName("phone") var phone : String = "",@SerializedName("code") var code : String = "" ): Serializable {
}