package grand.app.juha.pages.auth.code

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.connection.FileObject
import grand.app.juha.pages.auth.register.RegisterRequest
import grand.app.juha.pages.auth.repository.AuthRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class VerifyCodeViewModel : BaseViewModel {

    @Inject
    lateinit var repository: AuthRepository;


    private var verify: String = ""
    var type: String = ""
    private  val TAG = "VerificationViewModel"
    var timeLeftText : ObservableField<String> = ObservableField("01:00")
    private val TIME_DURATION:Long = 60000
    lateinit var countDownTimer : CountDownTimer
    var timeLeftInMilliSecond : Long = TIME_DURATION
    var timerRunning : Boolean = false
    lateinit var file: FileObject


    var resend : ObservableBoolean = ObservableBoolean(false)

    var request : VerifyCodeRequest = VerifyCodeRequest()

    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var x: Int = 6

    @Inject
    constructor(repository: AuthRepository){
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
    }


    fun setArgument( bundle : Bundle){
        this.type = bundle.getString(Constants.TYPE).toString()
        this.request.phone = bundle.getString(Constants.PHONE).toString()
        startTimer()
    }


    private fun startTimer() {
        resend.set(false)
        timeLeftInMilliSecond = TIME_DURATION
        countDownTimer = object: CountDownTimer(timeLeftInMilliSecond, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMilliSecond = millisUntilFinished
                updateTimer()
            }

            override fun onFinish() {
                countDownTimer.cancel()
                timerRunning = false
                resend.set(true)
            }
        }
        countDownTimer.start()
        timerRunning = true
    }

    private fun updateTimer() {
        val minute = (timeLeftInMilliSecond / 60000).toInt()
        val second = (timeLeftInMilliSecond % 60000 / 1000).toInt()
        timeLeftText.set("$minute:")
        if(second < 10) timeLeftText .set(timeLeftText.get()+"0")
        timeLeftText.set(timeLeftText.get()+"$second")
    }

    fun back(){
        getLiveData().value = Mutable(Constants.BACK)
    }

    fun stopTimer(){
        countDownTimer.cancel()
        timerRunning = false
        resend.set(false)
    }


    fun resend(){
        Log.d(TAG,"resend")
        startTimer()
        repository.codeSend(CodeSendRequest(request.phone))
//        repositoryFireBase.sendVerificationCode(registerRequest.phone)
    }

    fun submit(){
        if(request.code.length == 4){
            repository.verifyCode(request)
        }
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }


}