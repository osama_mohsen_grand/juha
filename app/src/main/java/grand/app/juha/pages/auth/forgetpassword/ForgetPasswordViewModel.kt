package grand.app.juha.pages.auth.forgetpassword

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.auth.code.CodeSendRequest
import grand.app.juha.pages.auth.forgetpassword.model.ForgetPasswordRequest
import grand.app.juha.pages.auth.repository.AuthRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ForgetPasswordViewModel : BaseViewModel {
    @Inject
    lateinit var repository: AuthRepository;

    lateinit var compositeDisposable: CompositeDisposable
    var request: ForgetPasswordRequest
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>

    @Inject
    constructor(repository: AuthRepository){
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        request =
            ForgetPasswordRequest()
        repository.setLiveData(liveDataViewModel)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun submit(){
        if(request.isValid()){
            val req = CodeSendRequest(request.phone,request.code)
            repository.codeSend(req)
        }
    }

    fun sendFirebase(){
        liveDataViewModel.value = Mutable(Constants.WRITE_CODE)
//        repositoryFireBase.sendVerificationCode(request.phone)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}