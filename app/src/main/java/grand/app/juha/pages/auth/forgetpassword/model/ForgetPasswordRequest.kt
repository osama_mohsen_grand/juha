package grand.app.juha.pages.auth.forgetpassword.model

import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.UserHelper
import grand.app.juha.utils.validation.Validate
import timber.log.Timber

class ForgetPasswordRequest() {

    @SerializedName("code")
    @Expose
    var code = "20"

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set(null)
        }


    @Transient
    lateinit var phoneError: ObservableField<String>

    constructor(phone: String) : this() {
        this.phone = phone
    }

    init {
        phoneError = ObservableField()

    }

    fun isValid(): Boolean {
        var valid = true

        if (!Validate.isValid(phone, Constants.PHONE)) {
            Log.d("valid", "phone")
            phoneError.set(Validate.error)
            valid = false
        } else phoneError.set(null)

        return valid
    }


}