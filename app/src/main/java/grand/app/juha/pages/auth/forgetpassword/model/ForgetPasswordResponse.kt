package grand.app.juha.pages.auth.forgetpassword.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class ForgetPasswordResponse(@SerializedName("exist") @Expose val exist: Boolean) :
    StatusMessage()