package grand.app.juha.pages.auth.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentLoginBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.auth.code.VerifyCodeFragment
import grand.app.juha.pages.auth.forgetpassword.ForgetPasswordFragment
import grand.app.juha.pages.auth.login.model.LoginResponse
import grand.app.juha.pages.auth.login.model.LoginSocialRequest
import grand.app.juha.pages.auth.register.RegisterFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.Constants.RC_SIGN_IN
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject

class LoginFragment : BaseFragment() {
    private lateinit var binding: FragmentLoginBinding

    @Inject
    lateinit var viewModel: LoginViewModel
    lateinit var mGoogleSignInClient: GoogleSignInClient
    var auth = FirebaseAuth.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        viewModel.callbackManager = CallbackManager.Factory.create();
        binding.viewmodel = viewModel
        setEvent()
        setupGoogleLogin()
        setupFacebookLogin()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setupFacebookLogin() {
        LoginManager.getInstance().logOut()
        binding.btnFacebook.setReadPermissions(listOf("public_profile", "email", "user_friends"));
        // If using in a fragment
        binding.btnFacebook.fragment = this;
        viewModel.btnFacebook = binding.btnFacebook
        viewModel.setupFacebookSignIn()
    }


    private fun setupGoogleLogin() {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(getActivityBase(), signInOptions)
    }

    private val TAG = "LoginFragment"


    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
            when (mutable.message) {
                Constants.SUCCESS -> {
                    finishAllActivities()
                    UserHelper.saveUserDetails((mutable.obj as LoginResponse).user)
                    MovementHelper.startActivity(context, MainActivity::class.java)
                }
                Constants.REGISTER -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle, getString(R.string.register))
                }
                Constants.FORGET_PASSWORD -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, ForgetPasswordFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle, "")
                }
                Constants.BACK -> {
                    requireActivity().finish()
                }
                Constants.SKIP -> {
                    finishAllActivities()
                    MovementHelper.startActivity(context, MainActivity::class.java)
                }
                Constants.FACEBOOK -> {
                    binding.btnFacebook.performClick();
                }
                Constants.GOOGLE -> {
                    val signInIntent = mGoogleSignInClient.signInIntent
                    startActivityForResult(signInIntent, RC_SIGN_IN)
                }
                "${Constants.RESPONSE_NOT_ACTIVE}" -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PHONE, viewModel.request.loginkey)
                    bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
                    bundle.putString(
                        Constants.TYPE,
                        Constants.VERIFY
                    )
                    MovementHelper.startActivityBase(context, bundle, "")
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
    }


    lateinit var photo: String
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        viewModel.callbackManager.onActivityResult(requestCode, resultCode, data)//facebook
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {//google
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)

                if (account != null) {
                    photo =
                        if (account.photoUrl != null) account.photoUrl.toString() else "https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png"
                    viewModel.loginSocial(
                        LoginSocialRequest(
                            account.id.toString(),
                            account.displayName.toString(),
                            account.email.toString(),
                            photo
                        )
                    )
                }
            } catch (e: ApiException) {
                e.stackTrace
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }
}