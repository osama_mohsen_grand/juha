package grand.app.juha.pages.auth.login

import android.os.Bundle
import android.util.Log
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.internal.OnConnectionFailedListener
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.auth.login.model.LoginRequest
import grand.app.juha.pages.auth.login.model.LoginSocialRequest
import grand.app.juha.pages.auth.repository.AuthRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONException
import org.json.JSONObject
import javax.inject.Inject


class LoginViewModel : BaseViewModel , OnConnectionFailedListener {

    @Inject
    lateinit var repository: AuthRepository;
    var compositeDisposable: CompositeDisposable
    var request: LoginRequest
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    lateinit var btnFacebook: LoginButton
    lateinit var callbackManager: CallbackManager

    @Inject
    constructor(repository: AuthRepository){
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        request = LoginRequest()
        repository.setLiveData(getLiveData())
    }

    private  val TAG = "LoginOrRegisterViewMode"


    fun forget(){
        getLiveData().value = Mutable(Constants.FORGET_PASSWORD)
    }
    fun register() {
        getLiveData().value = Mutable(Constants.REGISTER)
    }
    fun back() {
        getLiveData().value = Mutable(Constants.BACK)
    }

    fun submit(){
        if(request.isValid()){
            repository.login(request)
        }
    }

    fun submitGoogle(){
        getLiveData().value = Mutable(Constants.GOOGLE)
    }

    fun submitFacebook(){
        // Callback registration
        Log.d(TAG,"submitFacebook")
        getLiveData().value = Mutable(Constants.FACEBOOK)
    }


    fun loginSocial( request: LoginSocialRequest) {
        repository.loginSocial(request)
    }

    fun skip(){
        getLiveData().value = Mutable(Constants.SKIP)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    fun setupFacebookSignIn() {
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    Log.d(TAG,"onSuccess")
                    Log.e("start", "start here success login")
                    val request = GraphRequest.newMeRequest(
                        loginResult!!.accessToken
                    ) { `object`: JSONObject, response: GraphResponse? ->
                        Log.e("facebook", "completed")
                        try {
                            val social_id = `object`.getString("id")
                            val email = `object`.getString("email")
                            val name  = `object`.getString("name")
                            val image =
                                "https://graph.facebook.com/" + `object`.getString("id") + "/picture?width=250&height=250"
                            Log.e("social_id", social_id)
                            Log.e("email", email)
                            Log.e("image", image)
                            LoginManager.getInstance().logOut()
                            loginSocial(LoginSocialRequest(
                                social_id,
                                name,
                                email,
                                image
                            ))
                        } catch (e: JSONException) {
                            Log.e("start", "error")
                            e.printStackTrace()
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    Log.d(TAG,"onCancel")
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    Log.d(TAG,"onError")
                    // App code
                }
            })

    }
}