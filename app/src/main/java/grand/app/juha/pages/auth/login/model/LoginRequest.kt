package grand.app.juha.pages.auth.login.model


import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.validation.Validate

class LoginRequest() {

    private val TAG = "LoginRequest"


    @SerializedName("password")
    @Expose
    var password: String = ""
        get() = field
        set(value) {
            field = value
            passwordError.set(null)
//            if (field != "") passwordError.set(null)
        }

    @SerializedName("firebase_token")
    @Expose
    var firebase_token = "test"

    @SerializedName("loginkey")
    @Expose
    var loginkey = ""
        get() = field
        set(value) {
            field = value
            loginKeyError.set(null)
//            if (Validate.isValid(field,Constants.PHONE)) phoneError.set("")
//            else phoneError.set(Validate.error)
        }
    @Transient
    var loginKeyError: ObservableField<String> = ObservableField("")

    @Transient
    var phoneTextError : String = ""

    @Transient
    var passwordError: ObservableField<String> = ObservableField("")

    fun isValid(): Boolean {
        Log.d(TAG, "password is:$password")
        var valid = true
        if (!Validate.isValid(loginkey, Constants.PHONE) && !Validate.isValid(loginkey, Constants.EMAIL)) {
            Log.d("valid", "phone not valid")
            loginKeyError.set(Validate.error)
            phoneTextError = Validate.error
            valid = false

        } else loginKeyError.set("")

        if (!Validate.isValid(password)) {
            Log.d("password", "password not valid")
            passwordError.set(Validate.error)
            valid = false
        } else passwordError.set("")

        return valid
    }


}