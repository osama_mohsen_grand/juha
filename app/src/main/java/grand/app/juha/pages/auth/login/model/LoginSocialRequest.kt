package grand.app.juha.pages.auth.login.model


import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.validation.Validate

class LoginSocialRequest(
    @SerializedName("social_id") var socialId: String,
    @SerializedName("name") var name: String,
    @SerializedName("email") var email: String,
    @SerializedName("img") var image: String,
    @SerializedName("firebase_token") var token: String = "token"
) {
}