package grand.app.juha.pages.auth.login.model

import com.google.gson.annotations.SerializedName

class TokenRequest(@SerializedName("token") var token: String)