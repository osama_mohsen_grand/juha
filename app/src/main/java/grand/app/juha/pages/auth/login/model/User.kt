package grand.app.juha.pages.auth.login.model


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("email")
    var email: String = "",
    @SerializedName("firebase_token")
    var firebaseToken: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("img")
    var img: String = "",
    @SerializedName("jwt")
    var jwt: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("type")
    var type: Int = 0,
    @SerializedName("social_id")
    var social_id: String = ""
)