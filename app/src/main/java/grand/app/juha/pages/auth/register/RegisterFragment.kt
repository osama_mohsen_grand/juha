package grand.app.juha.pages.auth.register

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.StatusMessage
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.connection.FileObject
import grand.app.juha.databinding.FragmentRegisterBinding
import grand.app.juha.pages.auth.changepassword.ChangePasswordFragment
import grand.app.juha.pages.auth.code.VerifyCodeFragment
import grand.app.juha.pages.auth.forgetpassword.model.ForgetPasswordResponse
import grand.app.juha.pages.auth.login.model.LoginResponse
import grand.app.juha.pages.settings.term.ui.TermsFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.popup.PopUpInterface
import grand.app.juha.utils.popup.PopUpMenuHelper
import grand.app.juha.utils.session.UserHelper
import grand.app.juha.utils.upload.FileOperations
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class RegisterFragment : BaseFragment() {
    private lateinit var binding: FragmentRegisterBinding
    val popupMenu = PopUpMenuHelper()

    @Inject
    lateinit var viewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        if (UserHelper.isLogin()) Glide.with(requireActivity())
            .load(UserHelper.getUserDetails().img)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
            .into(binding.imgRegister)
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private val TAG = "RegisterFragment"

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG, mutable.message)
            if (mutable.message == Constants.FORGET_PASSWORD) {
                if ((mutable.obj as ForgetPasswordResponse).exist)
                    showError((mutable.obj as ForgetPasswordResponse).mMessage)
                else {
                    viewModel.sendVerificationCode()
                }
            } else if (mutable.message == Constants.REGISTER) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.register))
            } else if (mutable.message == Constants.LOGIN) {
                finishActivity()
            } else if (mutable.message == Constants.SELECT_IMAGE) {
                pickImageDialogSelect()
            } else if (mutable.message == Constants.CHANGE_PASSWORD) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ChangePasswordFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle)
            } else if (mutable.message == Constants.UPDATE_PROFILE) {
                toastMessage((mutable.obj as LoginResponse).mMessage)
                UserHelper.saveUserDetails((mutable.obj as LoginResponse).user)
                finishAllActivities()
                MovementHelper.startActivity(context, MainActivity::class.java)
            } else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            } else if (mutable.message == Constants.WRITE_CODE) {
                val bundle = Bundle()
                bundle.putSerializable(Constants.REGISTER, viewModel.request)
                bundle.putString(Constants.PHONE, viewModel.request.phone)
                bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
                bundle.putString(
                    Constants.TYPE,
                    if (UserHelper.isLogin()) Constants.UPDATE_PROFILE else Constants.REGISTER
                )
                MovementHelper.startActivityBase(context, bundle, "")
            }else if (mutable.message == URLS.SEND_CODE) {
                val bundle = Bundle()
                bundle.putSerializable(Constants.UPDATE_PROFILE, viewModel.request)
                bundle.putString(Constants.PHONE, viewModel.request.phone)
                bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
                bundle.putString(
                    Constants.TYPE,
                    if (UserHelper.isLogin()) Constants.UPDATE_PROFILE else Constants.REGISTER
                )
                MovementHelper.startActivityBase(context, bundle, "")
            } else if (mutable.message == Constants.TERMS_AND_CONDITIONS) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, TermsFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, "")
            }
        })

//        viewModel.liveDataViewModelFirebase.observe(requireActivity(), Observer {
//            val mutable = it as Mutable
//            handleActions(mutable)
//            Log.d(TAG, "setEvent: "+mutable.message)
//            if (mutable.message == Constants.WRITE_CODE) {
//                val bundle = Bundle()
//                bundle.putSerializable(Constants.REGISTER, viewModel.request)
//                bundle.putSerializable(Constants.IMAGE, viewModel.file)
//                bundle.putString(Constants.VERIFY_ID, viewModel.repositoryFireBase.getVerificationId())
//                bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
//                bundle.putString(Constants.TYPE, if(UserHelper.isLogin()) Constants.UPDATE_PROFILE else Constants.REGISTER )
//                MovementHelper.startActivityBase(context, bundle, getString(R.string.verify))
//            }
//        })

//        binding.edtRegisterEmployee.setOnClickListener{
//            popupMenu.openPopUp(
//                requireActivity(),
//                binding.edtRegisterEmployee,
//                viewModel.employers,object : PopUpInterface{
//                    override fun submitPopUp(position: Int) {
//                        binding.edtRegisterEmployee.setText(viewModel.employers[position])
//                        viewModel.request.employer_id = viewModel.mainSettingsResponse.employers[position].id
//                    }
//                })
//        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super method removed
//        Timber.e("onActivityResult:$requestCode")
        Log.d(TAG, "onActivityResult: $resultCode")
        Log.d(TAG, "onActivityResult: $requestCode")
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            if (data != null)
                Log.d(TAG, "data not null")
            else
                Log.d(TAG, "data null")
            viewModel.file = FileOperations.getFileObject(
                requireActivity(),
                data,
                Constants.IMAGE,
                Constants.FILE_TYPE_IMAGE
            )!!
            binding.imgRegister.setImageURI(
                Uri.parse(
                    File(viewModel.file!!.getFilePath().toString()).toString()
                )
            )
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}