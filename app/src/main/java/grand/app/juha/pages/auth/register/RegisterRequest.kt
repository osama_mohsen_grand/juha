package grand.app.juha.pages.auth.register

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.SharedPreferenceHelper
import grand.app.juha.utils.session.UserHelper
import grand.app.juha.utils.validation.Validate
import timber.log.Timber
import java.io.Serializable

class RegisterRequest() : Serializable{

    @SerializedName("code")
    @Expose
    var code: String = "20"

    @SerializedName("email")
    @Expose
     var email = ""
        get() = field
        set(value) {
            field = value
            emailError.set("")
        }

    @SerializedName("password")
    @Expose
    var password: String = ""
        get() = field
        set(value) {
            field = value
            passwordError.set("")
        }

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set("")
        }



    @SerializedName("name")
    @Expose
    var name = ""
        get() = field
        set(value) {
            field = value
            nameError.set("")
        }

    @SerializedName("firebase_token")
    @Expose
    var firebaseToken = SharedPreferenceHelper.getKey(Constants.TOKEN)
        get() = field




    @Transient
    lateinit var phoneError: ObservableField<String>

    @Transient
    lateinit var emailError: ObservableField<String>

    @Transient
    lateinit var passwordError: ObservableField<String>
    @Transient
    lateinit var nameError: ObservableField<String>


    @Transient
    lateinit var isLogin: ObservableBoolean



    init {
        emailError = ObservableField()
        passwordError = ObservableField()
        phoneError = ObservableField()
        nameError = ObservableField()
        isLogin = ObservableBoolean(UserHelper.isLogin())
    }

    fun isValid(): Boolean {
        var valid = true
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error)
            valid = false
        } else nameError.set("")

        if (!Validate.isValid(phone, Constants.PHONE)) {
            phoneError.set(Validate.error)
            valid = false
        } else phoneError.set("")
        if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error)
            valid = false
        } else emailError.set("")

        if (!UserHelper.isLogin()) {
            if (!Validate.isValid(password)) {
                passwordError.set(Validate.error)
                valid = false
            } else passwordError.set("")
        }
        return valid
    }


}