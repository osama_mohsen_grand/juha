package grand.app.juha.pages.auth.register

import android.util.Log
import android.widget.CompoundButton
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.connection.FileObject
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.auth.code.CodeSendRequest
import grand.app.juha.pages.auth.forgetpassword.model.ForgetPasswordRequest
import grand.app.juha.pages.auth.repository.AuthRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class RegisterViewModel : BaseViewModel {
    @Inject
    lateinit var repository: AuthRepository;

    var compositeDisposable: CompositeDisposable
    var request: RegisterRequest

    val user = UserHelper.getUserDetails()
    val isLogin = ObservableBoolean(UserHelper.isLogin())
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var file : FileObject = FileObject()

    private val TAG = "RegisterViewModel"
    var checked: Boolean = UserHelper.getUserId() != -1

    @Inject
    constructor(repository: AuthRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        request = RegisterRequest()
        if(UserHelper.isLogin()){
            request.name = user.name
            request.phone = user.phone
            request.email = user.email
        }
        repository.setLiveData(getLiveData())
    }
    fun login() {
        getLiveData().value = Mutable(Constants.LOGIN)
    }

    fun register() {
        getLiveData().value = Mutable(Constants.REGISTER)
    }

    fun changePassword() {
        getLiveData().value = Mutable(Constants.CHANGE_PASSWORD)
    }

    fun image() {
        getLiveData().value = Mutable(Constants.SELECT_IMAGE)
    }

    fun termSubmit(){
        getLiveData().value = Mutable(Constants.TERMS_AND_CONDITIONS)
    }


    fun onCheckedChange(button: CompoundButton?, check: Boolean) {
        checked = check
    }

    fun submit() {
        if (request.isValid()) {
            if(checked) {
                if (!UserHelper.isLogin()) {
                    repository.register(request, file);
                } else {
                    if (!UserHelper.isLogin() || (UserHelper.isLogin() && UserHelper.getUserDetails().phone != request.phone)) {
                        if (checked) {
                            if(!UserHelper.isLogin())
                                repository.register(
                                    request, file
                                )
                            else{
                                sendVerificationCode()
                            }
                        }
                    } else {
                        repository.updateProfile(
                            request, file, file.getParamName() != ""
                        )
                    }
                }
            }else {
                Log.d(TAG,"error here")
                getLiveData().value = Mutable(
                    Constants.ERROR,
                    getString(R.string.please_confirm_on_terms_and_conditions)
                )
            }
        }
    }

    fun sendVerificationCode() {
        repository.codeSend(CodeSendRequest(request.phone))
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}