package grand.app.juha.pages.auth.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.StatusMessage
import grand.app.juha.pages.auth.forgetpassword.model.ForgetPasswordRequest
import grand.app.juha.pages.auth.login.model.LoginRequest
import grand.app.juha.pages.auth.login.model.LoginResponse
import grand.app.juha.pages.auth.register.RegisterRequest
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.connection.FileObject

import grand.app.juha.pages.auth.changepassword.ChangePasswordRequest
import grand.app.juha.pages.auth.code.CodeSendRequest
import grand.app.juha.pages.auth.code.VerifyCodeRequest
import grand.app.juha.pages.auth.forgetpassword.model.ForgetPasswordResponse
import grand.app.juha.pages.auth.login.model.LoginSocialRequest
import grand.app.juha.pages.auth.login.model.TokenRequest
import grand.app.juha.pages.repository.BaseRepository

import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }

    fun login(request: LoginRequest): Disposable? {
        if(request.loginkey[0] == '0')
            request.loginkey = request.loginkey.substring(1)
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.LOGIN, request, LoginResponse::class.java,
            Constants.SUCCESS, true
        )
    }


    fun loginSocial(request: Any?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.LOGIN_SOCIAL, request, LoginResponse::class.java,
            Constants.SUCCESS, true
        )
    }


    fun register(
        request: RegisterRequest, file: FileObject): Disposable? {
        if(request.phone[0] == '0')
            request.phone = request.phone.substring(1)
        return if(file.getFilePath() != "") {
            val fileObjects: ArrayList<FileObject> = ArrayList()
            fileObjects.add(file)
            connectionHelper.requestApi(
                URLS.REGISTER, request,fileObjects, StatusMessage::class.java,
                Constants.WRITE_CODE, true
            )
        }else{
            connectionHelper.requestApi(
                Constants.POST_REQUEST,
                URLS.REGISTER, request, StatusMessage::class.java,
                Constants.WRITE_CODE, true
            )
        }
    }


    fun codeSend(request: CodeSendRequest): Disposable? {
//        if(request.phone.trim().isNotEmpty() && request.phone.trim()[0] == '0'){
//            request.phone = request.phone.trim().substring(1)
//        }

        if(request.phone[0] == '0')
            request.phone = request.phone.substring(1)

        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.SEND_CODE, request,
            StatusMessage::class.java,
            URLS.SEND_CODE, true
        )
    }


    private val TAG = "AuthRepository"
    fun updateProfile(
        request: RegisterRequest?,
        file: FileObject,
        haveFile: Boolean
    ): Disposable? {
        return if (!haveFile) {
            Log.d(TAG, "not fileObject")
            connectionHelper.requestApi(
                Constants.POST_REQUEST, URLS.UPDATE_PROFILE, request,
                LoginResponse::class.java,
                Constants.UPDATE_PROFILE, true
            )
        } else {
            Log.d(TAG, "fileObject")
            val fileObjects: ArrayList<FileObject> = ArrayList<FileObject>()
            fileObjects.add(file)
            connectionHelper.requestApi(
                URLS.UPDATE_PROFILE, request, fileObjects, LoginResponse::class.java,
                Constants.UPDATE_PROFILE, true
            )
        }
    }

    fun updateToken(token: String): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            URLS.UPDATE_TOKEN, TokenRequest(token), StatusMessage::class.java,
            URLS.UPDATE_TOKEN, false
        )
    }

    fun forgetPassword(request: ForgetPasswordRequest): Disposable? {
        if(request.phone[0] == '0')
            request.phone = request.phone.substring(1)

        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.FORGET_PASSWORD, request,
            ForgetPasswordResponse::class.java,
            Constants.FORGET_PASSWORD, true
        )
    }


    fun changePassword(request: ChangePasswordRequest?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.CHANGE_PASSWORD, request,
            StatusMessage::class.java,
            Constants.CHANGE_PASSWORD, true
        )
    }



    fun verifyCode(request: VerifyCodeRequest): Disposable? {
        if(request.phone[0] == '0')
            request.phone = request.phone.substring(1)
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.VERIFY, request,
            LoginResponse::class.java,
            Constants.SUCCESS, true
        )
    }


}