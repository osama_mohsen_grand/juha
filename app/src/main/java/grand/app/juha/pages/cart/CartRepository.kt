package grand.app.juha.pages.cart

import androidx.lifecycle.MutableLiveData
import grand.app.juha.base.StatusMessage
import grand.app.juha.model.base.Mutable
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.model.CheckoutRequest
import grand.app.juha.pages.cart.model.UpdateCartRequest
import grand.app.juha.pages.repository.BaseRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class CartRepository  : BaseRepository {
    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()

    fun getCart(): Disposable? {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.USER_CART, Any(),
            CartResponse::class.java, URLS.USER_CART, true
        )
    }

    fun addToCart(updateCartRequest: UpdateCartRequest): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.ADD_TO_CART, updateCartRequest,
            CartResponse::class.java, URLS.ADD_TO_CART, true
        )
    }

    fun deleteItemCart(id: Int): Disposable? {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.DELETE_ITEM_CART + id, Any(),
            CartResponse::class.java, Constants.DELETE, true
        )
    }

    fun checkout(request: CheckoutRequest) : Disposable {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHECKOUT, request, StatusMessage::class.java,
            URLS.CHECKOUT, true)
    }
}