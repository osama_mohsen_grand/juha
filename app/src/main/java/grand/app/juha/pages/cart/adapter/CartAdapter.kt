package grand.app.juha.pages.cart.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemCartBinding
import grand.app.juha.pages.cart.model.Cart
import grand.app.juha.pages.cart.model.UpdateCartRequest
import grand.app.juha.pages.cart.viewmodel.ItemCartViewModel
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.MovementHelper
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_product.view.*

class CartAdapter(var modelList:ArrayList<Cart> = ArrayList(), private var repository: ProductRepository? = null) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    var newValue : Int = -1
    private  val TAG = "CartAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCartBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_cart,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            if(mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                bundle.putInt(Constants.ID, model.productId)
                bundle.putInt(Constants.COLOR_ID, model.colorId)
                bundle.putInt(Constants.SIZE_ID, model.sizeId)
                bundle.putString(Constants.TITLE, model.name)
                MovementHelper.startActivityBase(holder.itemView.context, bundle);

            }else if(mutable.message == Constants.ADD){
//                Log.d(TAG, "qty_tmp:"+model.qty_tmp.toString())
                newValue = model.quantity+1
                val request = UpdateCartRequest(model.productId,1)
                request.colorId = model.colorId.toString()
                request.sizeId = model.sizeId.toString()
                repository!!.addToCart(request)
            }else if( mutable.message == Constants.MINUS){
//                Log.d(TAG, "qty_tmp:"+model.qty_tmp.toString())
                newValue = model.quantity-1
                val request = UpdateCartRequest(model.productId,-1)
                request.colorId = model.colorId.toString()
                request.sizeId = model.sizeId.toString()
                repository!!.addToCart(request)
            }else if(mutable.message == Constants.FAVOURITE){
                Log.d(TAG,"message favourite")
                holder.itemView.img_item_cart_fav.startAnimation(
                    AnimationUtils.loadAnimation(
                        holder.itemView.rootView.context,
                        R.anim.clockwise
                    )
                )
                favPosition = position
                repository!!.addFavourite(model.productId)
                modelList[selectedItem].isFavorite = if(modelList[selectedItem].isFavorite == 0) 1 else 0
                notifyItemChanged(selectedItem)
            }else if(mutable.message == Constants.DELETE){
                repository!!.deleteItemCart(model.id)
            }
            notifyItemChanged(selectedItem)
        }
    }

    fun updateCart(){
        if(selectedItem != -1){
            modelList[selectedItem].quantity = newValue
            notifyItemChanged(selectedItem)
        }
    }

    fun deleteItem() {
        if(selectedItem != -1){
            modelList.removeAt(selectedItem)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun updateFavourite(favorite: Int) {
        modelList[favPosition].isFavorite = favorite
        notifyItemChanged(favPosition)
    }


    fun update(modelList: ArrayList<Cart>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }



    class ViewHolder(private val binding: ItemCartBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemCartViewModel

        //bint
        fun bind(model: Cart) {
            viewModel = ItemCartViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}