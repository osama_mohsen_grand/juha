package grand.app.juha.pages.cart.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.databinding.ItemCartDetailsBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.model.Cart
import grand.app.juha.pages.cart.model.UpdateCartRequest
import grand.app.juha.pages.cart.viewmodel.ItemCartViewModel
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager

class CartDetailsAdapter(var modelList:ArrayList<Cart> = ArrayList(), private var repository: ProductRepository? = null) : RecyclerView.Adapter<CartDetailsAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    private lateinit var fragment : BaseFragment
    var isOrderDetails = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCartDetailsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_cart_details,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    fun setFragment(fragment: BaseFragment){
        this.fragment = fragment
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[selectedItem]

            if(mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()

            }
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun updateFromScratch(modelList: ArrayList<Cart>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun update(modelList: ArrayList<Cart>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    class ViewHolder(private val binding: ItemCartDetailsBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemCartViewModel

        //bint
        fun bind(model: Cart) {
            viewModel = ItemCartViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}