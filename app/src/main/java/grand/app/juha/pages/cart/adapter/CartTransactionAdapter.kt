package grand.app.juha.pages.cart.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.R
import grand.app.juha.databinding.ItemCartTransactionBinding
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.model.CartTransaction
import grand.app.juha.pages.cart.viewmodel.ItemCartTransactionViewModel
import grand.app.juha.utils.resources.ResourceManager

class CartTransactionAdapter(var modelList: ArrayList<CartTransaction> = ArrayList()) :
    RecyclerView.Adapter<CartTransactionAdapter.ViewHolder>() {
    // fetch list data
    lateinit var response: CartResponse
    fun setList(response: CartResponse) {
        this.response = response
        modelList.clear()
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.cost),
                response.cartData.productsPrice.toString()+ " "+ResourceManager.getString(R.string.currency)
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.discount),
               if( response.cartData.discountedPrice == 0.0) ResourceManager.getString(R.string.no_thing) else
                   response.cartData.discountedPrice.toString()+ " "+ResourceManager.getString(R.string.currency)
            )
        )
//        modelList.add(
//            CartTransaction(
//                "${ResourceManager.getString(R.string.code_discount)} ${response.data.promoPercent}",
//                response.data.promoAmount
//            )
//        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.vat),
                response.cartData.vat.toString()+" %"
            )
        )
        if(response.cartData.productsPrice > response.cartData.freeShippingPrice || response.cartData.freeShippingPrice.equals("0")) {
            modelList.add(
                CartTransaction(
                    ResourceManager.getString(R.string.shipping),
                    ResourceManager.getString(R.string.free)
                )
            )
        }else{
            modelList.add(
                CartTransaction(
                    ResourceManager.getString(R.string.shipping),
                    response.cartData.shipping+ " "+ResourceManager.getString(R.string.currency)
                )
            )
        }
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.total_summation),
                response.cartData.total.toString()+ " "+ResourceManager.getString(R.string.currency)
            )
        )
        notifyDataSetChanged()
    }

    private val TAG = "CartTransactionAdapter"


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCartTransactionBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_cart_transaction,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

//    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun update(modelList: ArrayList<CartTransaction>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
//        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
        notifyDataSetChanged()
    }

//    fun update(total: Int) {
//        modelList[0].value = total
////        modelList[3].value = total + modelList[1].value + modelList[2].value + response.data.taxesAmount
//        notifyDataSetChanged()
//    }


    inner class ViewHolder(private val binding: ItemCartTransactionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemCartTransactionViewModel

        //bint
        fun bind(model: CartTransaction) {
            viewModel = ItemCartTransactionViewModel(
                model,
                adapterPosition,
                adapterPosition == modelList.size - 1
            )
            binding.viewmodel = viewModel
        }
    }
}