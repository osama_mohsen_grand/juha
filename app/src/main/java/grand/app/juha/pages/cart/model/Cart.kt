package grand.app.juha.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.model.IdNameImage
import grand.app.juha.pages.product.model.details.Size
import java.io.Serializable

data class Cart(
    @SerializedName("cart_id")
    var cartId: Int = 0,
    @SerializedName("color")
    var color: Size = Size(),
    @SerializedName("color_id")
    var colorId: Int = 0,
    @SerializedName("color_name")
    var colorName: String = "",
    @SerializedName("discounted_price")
    var discountedPrice: Int = 0,
    @SerializedName("has_discount")
    var hasDiscount: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("img")
    var img: String = "",
    @SerializedName("is_favorite")
    var isFavorite: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("price")
    var price: Double = 0.0,
    @SerializedName("product_id")
    var productId: Int = 0,

    @SerializedName("product_quantity")
    var productQuantity: Int = 0,
    @SerializedName("quantity")
    var quantity: Int = 0,
    @SerializedName("rate")
    var rate: Float = 0f,
    @SerializedName("size")
    var size: Size = Size(),
    @SerializedName("size_id")
    var sizeId: Int = 0,
    @SerializedName("size_name")
    var sizeName: String = "",
    @SerializedName("default_img")
    @Expose
    var defaultImg: IdNameImage = IdNameImage(-1, "")

): Serializable{
}