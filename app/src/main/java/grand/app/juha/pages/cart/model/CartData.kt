package grand.app.juha.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CartData(
    @SerializedName("code")
    @Expose
    var code: Code = Code(),
    @SerializedName("discounted_price")
    @Expose
    var discountedPrice: Double = 0.0,
    @SerializedName("free_shipping_price")
    @Expose
    var freeShippingPrice: Double = 0.0,
    @SerializedName("products")
    @Expose
    var products: ArrayList<Cart> = arrayListOf(),
    @SerializedName("products_price")
    @Expose
    var productsPrice: Double = 0.0,
    @SerializedName("shipping")
    @Expose
    var shipping: String = "",
    @SerializedName("total")
    @Expose
    var total: Double = 0.0,
    @SerializedName("address_id")
    @Expose
    var addressId: Int = -1,
    @SerializedName("vat")
    @Expose
    var vat: Double = 0.0
) : Serializable