package grand.app.juha.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import java.io.Serializable

data class CartResponse(
    @SerializedName("data")
    @Expose
    var cartData: CartData = CartData()
) : StatusMessage() , Serializable