package grand.app.juha.pages.cart.model

import androidx.databinding.ObservableBoolean
import com.google.gson.annotations.SerializedName
import java.io.Serializable

public class CheckoutRequest(
    @SerializedName("products_id")
    var productsId: ArrayList<Int> = arrayListOf(),
    @SerializedName("quantity")
    var quantity: ArrayList<Int> = arrayListOf(),
    @SerializedName("size_id")
    var sizeId: ArrayList<Int> = arrayListOf(),
    @SerializedName("color_id")
    var colorId: ArrayList<Int> = arrayListOf(),
    @SerializedName("products_price")
    var productsPrice: Double = 0.0,
    @SerializedName("total")
    var total: Double = 0.0,
    @SerializedName("shipping")
    var shipping: String = "",
    @SerializedName("address_id")
    var addressId: Int = 0,
    @SerializedName("addition")
    var addition: String = "",
    @SerializedName("payment_method")
    var paymentMethod: Int = 0,
    @SerializedName("wallet")
    var wallet: Double = 0.0,
    @SerializedName("current_balance")
    var currentBalance: ObservableBoolean = ObservableBoolean(false)

    ) : Serializable{
    fun reset() {
        productsId.clear()
        quantity.clear()
        sizeId.clear()
        colorId.clear()
    }

}