package grand.app.juha.pages.cart.model


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Code(
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("times")
    var times: Int = 0,
    @SerializedName("type")
    var type: Int = 0,
    @SerializedName("user_id")
    var userId: String = "",
    @SerializedName("value")
    var value: Int = 0
): Serializable