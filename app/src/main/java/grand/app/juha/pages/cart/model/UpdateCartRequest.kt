package grand.app.juha.pages.cart.model

import com.google.gson.annotations.SerializedName

public class UpdateCartRequest(@SerializedName("product_id") val product_id:Int,@SerializedName("quantity") val quantity:Int) {

    @SerializedName("size_id") lateinit var sizeId:String
    @SerializedName("color_id") lateinit var colorId:String

    constructor(product_id:Int,quantity:Int, sizeId:String, colorId:String) :this(product_id,quantity){
        this.sizeId = sizeId
        this.colorId = colorId
    }
}