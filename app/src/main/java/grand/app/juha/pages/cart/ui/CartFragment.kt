package grand.app.juha.pages.cart.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.StatusMessage
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentCartBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.viewmodel.CartViewModel
import grand.app.juha.pages.product.model.FavouriteResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.SharedPreferenceHelper
import javax.inject.Inject

class CartFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentCartBinding
    @Inject
    lateinit var viewModel: CartViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        binding.rvCart.itemAnimator = null
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.USER_CART) {
                viewModel.response = (mutable.obj as CartResponse)
                viewModel.setData()
            }
//            else if (mutable.message == URLS.ADD_FAVOURITE) {
//                viewModel.adapter.updateFavourite( (mutable.obj as FavouriteResponse).isFavorite)
//            }
            else if(mutable.message == URLS.ADD_TO_CART){
                val response = mutable.obj as CartResponse
                viewModel.updateCart(response)
                viewModel.adapterTransaction.setList(response)
            }else if(mutable.message == Constants.SUBMIT){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, CheckoutFragment::class.java.name)
                viewModel.response.cartData.products = viewModel.adapter.modelList
                Log.d(TAG,"${viewModel.response.cartData.products[0].quantity}")
                bundle.putSerializable(Constants.CART, viewModel.response)
                MovementHelper.startActivityBaseForResult(this, bundle,Constants.RELOAD_REQUEST, getString(R.string.confirm_order))
            }else if(mutable.message == Constants.DELETE){
                val response = mutable.obj as CartResponse
                viewModel.adapterTransaction.setList(response)
                viewModel.adapter.deleteItem()
                if(viewModel.adapter.modelList.size == 0){
                    viewModel.show.set(false)
                    viewModel.noData()
                }
                toastMessage(response.mMessage)
//                SharedPreferenceHelper.saveKey(Constants.CART,response.data.cartCount.toString())
//                Log.d(TAG,response.data.cartCount.toString())
//                (requireActivity() as MainActivity).updateCartCount()
//                Log.d("cart","cart_count_page:"+SharedPreferenceHelper.getKey(Constants.CART))
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
        viewModel.getCart()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constants.RELOAD_REQUEST && resultCode == RESULT_OK){
            data?.hasExtra(Constants.RELOAD).let {
                viewModel.repository.setLiveData(viewModel.liveDataViewModel)
//                viewModel.repository.setLiveData(viewModel.liveDataViewModel)
                viewModel.getCart()
            }
        }
    }
}