package grand.app.juha.pages.cart.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.StatusMessage
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.customviews.dialog.DialogHelper
import grand.app.juha.customviews.dialog.DialogHelperInterface
import grand.app.juha.databinding.FragmentCheckoutBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.address.add.AddAddressFragment
import grand.app.juha.pages.address.list.model.Address
import grand.app.juha.pages.address.list.model.AddressListResponse
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.viewmodel.CheckoutViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.IOnBackPressed
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject

class CheckoutFragment : BaseFragment(), IOnBackPressed {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentCheckoutBinding

    @Inject
    lateinit var viewModel: CheckoutViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkout, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it, this) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.ADDRESS_LIST) {
                viewModel.responseAddress = mutable.obj as AddressListResponse
                viewModel.setData()
            }else if (mutable.message == URLS.CHECKOUT) {

                val ids =
                    intArrayOf(R.id.tv_dialog_close_app_yes )
                DialogHelper.showDialogHelper(
                    context,
                    R.layout.dialog_checked_out,
                    ids,
                    object: DialogHelperInterface {
                        override fun OnClickListenerContinue(dialog: Dialog, view: View) {
                            when (view.id) {
                                R.id.tv_dialog_close_app_yes -> {
                                    dialog.dismiss()
                                    MovementHelper.startActivity(requireActivity(),MainActivity::class.java)
                                }

                            }
                        }
                    })


            } else if (mutable.message == URLS.PROMO) {
                AppHelper.showDialog(requireContext(), (mutable.obj as CartResponse).mMessage)
                viewModel.promoSend = viewModel.promo
                Log.d(TAG, viewModel.promoSend)
                viewModel.promo = ""
                viewModel.adapterTransaction.setList((mutable.obj as CartResponse))
                viewModel.notifyChange()
            } else if (mutable.message == Constants.ADD_ADDRESS) {
                val bundle = Bundle()
//                when(viewModel.adapterAddress.modelList.size){
//                    0 -> {
//                        bundle.putString(
//                            Constants.PAGE,
//                            NoAddressFragment::class.java.name
//                        )
//                        MovementHelper.startActivityBaseForResult(
//                            this,
//                            bundle,
//                            Constants.ADDRESS_RESULT,
//                            getString(R.string.addresses)
//                        )
//                    }
//                    else -> {
//                        bundle.putString(
//                            Constants.PAGE,
//                            AddAddressFragment::class.java.name
//                        )
//
//                        MovementHelper.startActivityBaseForResult(
//                            this,
//                            bundle,
//                            Constants.ADDRESS_RESULT,
//                            getString(R.string.add_address)
//                        )
//                    }
//                }

                bundle.putString(
                    Constants.PAGE,
                    AddAddressFragment::class.java.name
                )

                MovementHelper.startActivityBaseForResult(
                    this,
                    bundle,
                    Constants.ADDRESS_RESULT,
                    getString(R.string.add_address)
                )

            } else if (mutable.message == Constants.SUBMIT) {
                if (viewModel.adapterAddress.id == -1 && viewModel.responseCart.cartData.addressId == -1) {
                    showError(getString(R.string.please_add_your_address_first))
                } else {
                    viewModel.checkout()
                }
            } else if (mutable.message == URLS.PROMO) {
                toastMessage(((mutable.obj) as StatusMessage).mMessage)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "request:$requestCode")
        Log.d(TAG, "result:$resultCode")
        if (requestCode == Constants.ADDRESS_RESULT && resultCode == Activity.RESULT_OK) {
            viewModel.repositoryUser.setLiveData(viewModel.liveDataViewModel)
            data?.let {
                if (data.hasExtra(Constants.ADDRESS)) {
                    val address: Address = data.getSerializableExtra(Constants.ADDRESS) as Address
                    viewModel.adapterAddress.add(address, 0)
                }
            }

        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repositoryUser.setLiveData(viewModel.liveDataViewModel)
    }

    override fun onBackPressed(): Boolean {
        Log.d(TAG, "onBackPressed")
        val intent = Intent()
        intent.putExtra(Constants.RELOAD, true)
        requireActivity().setResult(Activity.RESULT_OK, intent);
        requireActivity().finish()
        return false
    }
}