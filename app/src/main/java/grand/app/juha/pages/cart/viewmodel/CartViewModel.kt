package grand.app.juha.pages.cart.viewmodel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.adapter.CartAdapter
import grand.app.juha.pages.cart.adapter.CartTransactionAdapter
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CartViewModel : BaseViewModel {
    var response: CartResponse = CartResponse()

    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable
    var adapter: CartAdapter
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var adapterTransaction = CartTransactionAdapter()

    var type: Int = 0
    private val TAG = "CartViewModel"
    var title: ObservableField<String> =
        ObservableField("")

    var shippingFreeCost: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = CartAdapter(repository = repository)
//        adapter.request = ClothesModelRequest()
//        AppSpecificHelper.initClothesModels(adapter.request.models)
    }


    fun getCart() {
        show.set(false)
        repository.getCart()
    }

    fun submit(){
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {

        adapter.update(response.cartData.products)
        adapterTransaction.setList(response)
        shippingFreeCost.set("${getString(R.string.if_the_shipping_cost_exceeds)} ${response.cartData.freeShippingPrice} ${getString(R.string.currency)} ${getString(R.string.the_shipping_cost_is_free)}")
        show.set(response.cartData.products.size > 0)
        if(!show.get()) noData()
    }
//
//    fun updateFavourite(favorite: Int) {
//        adapter.updateFavourite(favorite)
//    }
//
    fun updateCart(response: CartResponse) {
        adapter.updateCart()
        adapterTransaction.setList(response)
//        this.response.cartData.taxesAmount = response.cartData.taxesAmount
//        this.response.cartData.shipping = response.cartData.shipping
//        this.response.cartData.totalPrice = response.cartData.totalPrice
//        adapterTransaction.update(response)
        notifyChange()
    }
}