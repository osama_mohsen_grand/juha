package grand.app.juha.pages.cart.viewmodel

import android.os.Bundle
import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.address.list.AddressAdapter
import grand.app.juha.pages.address.list.model.AddressListResponse
import grand.app.juha.pages.cart.adapter.CartDetailsAdapter
import grand.app.juha.pages.cart.adapter.CartTransactionAdapter
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.model.CheckoutRequest
import grand.app.juha.pages.cart.model.PromoRequest
import grand.app.juha.pages.cart.ui.CheckoutFragment
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.repository.UserRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CheckoutViewModel : BaseViewModel {
    var promoSend: String = ""

    @Inject
    lateinit var repositoryUser: UserRepository;

    @Inject
    lateinit var productRepository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "CartDetailsViewModel"
    var responseAddress: AddressListResponse = AddressListResponse()
    lateinit var responseCart: CartResponse
    var adapterAddress: AddressAdapter
    var adapterTransaction = CartTransactionAdapter()
    var adapterCart: CartDetailsAdapter
    var promo = ""
    val request = CheckoutRequest()

    @Inject
    constructor(productRepository: ProductRepository, repositoryUser: UserRepository) {
        this.repositoryUser = repositoryUser
        this.productRepository = productRepository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repositoryUser.setLiveData(liveDataViewModel)
        adapterAddress = AddressAdapter()
        adapterCart = CartDetailsAdapter()
    }

    fun setArgument(
        bundle: Bundle,
        fragment: CheckoutFragment
    ) {
        adapterCart.setFragment(fragment)
        responseCart = bundle.getSerializable(Constants.CART) as CartResponse
        adapterCart.update(responseCart.cartData.products)
        request.shipping = responseCart.cartData.shipping
        request.total = responseCart.cartData.total
        request.productsPrice = responseCart.cartData.productsPrice
        request.addressId = adapterAddress.id
        adapterTransaction.setList(responseCart)
        Log.d(TAG, adapterCart.modelList.size.toString())
        getAddressList()
    }

    public fun onSplitTypeChanged(radioGroup: RadioGroup, id: Int) {
        when (id) {
            R.id.rb_cash -> request.paymentMethod = 0
            else -> request.paymentMethod = 1
        }
    }

    private fun getAddressList() {
        repositoryUser.getAddresses()
    }

    fun addAddress() {
        liveDataViewModel.value = Mutable(Constants.ADD_ADDRESS)
    }

    fun submit() {
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }

    fun submitPromo() {
        if (promo.trim() != "") {
            repositoryUser.addPromo(PromoRequest(promo))
        }
    }


    fun onCheckedChanged(checked: Boolean) {
        request.wallet = if(checked) responseAddress.wallet else 0.0
    }


    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
//        Log.d(TAG,responseAddress.wallet)
//        responseAddress.data.forEachIndexed { index, address ->
//            if (address.id == responseAddress.addressId) {
//                adapterAddress.selected = index
//                adapterAddress.id = responseAddress.addressId
//            }
//        }
        adapterAddress.update(responseAddress.data)
        adapterAddress.setAddressSelect(responseAddress.addressId)
        showPage(true)
    }

    fun checkout() {
        Log.d(TAG, "checked:" + request.currentBalance.get())
        request.reset()
        request.addressId = adapterAddress.id
        responseCart.cartData.products.forEach {
            request.productsId.add(it.productId)
            request.colorId.add(it.colorId)
            request.sizeId.add(it.sizeId)
            request.quantity.add(it.quantity)
        }
        productRepository.checkout(request)
    }

}