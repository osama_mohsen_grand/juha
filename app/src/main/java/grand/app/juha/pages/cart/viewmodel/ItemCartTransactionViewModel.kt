package grand.app.juha.pages.cart.viewmodel

import grand.app.juha.pages.cart.model.CartTransaction

data class ItemCartTransactionViewModel(
    val model: CartTransaction,
    var position: Int,
    val isLastItem: Boolean = false
) {


}