package grand.app.juha.pages.cart.viewmodel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.model.Cart
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager

data class ItemCartViewModel(val model : Cart, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()

    private  val TAG = "ItemCartViewModel"

    var qty = ObservableField<String>(model.quantity.toString())

    init {

        Log.d(TAG,"qunatity:"+qty.get())
    }

    var price: ObservableField<String> =
        ObservableField("${if(model.discountedPrice == 0) model.price * model.quantity else model.discountedPrice * model.quantity} ${ResourceManager.getString(R.string.currency)}")

    fun fav(){
        mutableLiveData.value = Mutable(Constants.FAVOURITE , position)
    }
    fun delete(){
        mutableLiveData.value = Mutable(Constants.DELETE , position)
    }

    fun add(){
        val qtyTmp: Int = model.quantity + 1
        if(qtyTmp <= model.productQuantity)
            mutableLiveData.value = Mutable(Constants.ADD, position,qtyTmp)
        else
            mutableLiveData.value = Mutable(Constants.ERROR,ResourceManager.getString(R.string.sorry_you_reached_to_maximum_limit))
    }
    fun minus(){
        val qtyTmp: Int = model.quantity - 1
        if(qtyTmp > 0) {
            mutableLiveData.value = Mutable(Constants.MINUS, position,qtyTmp)
        }
    }
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}