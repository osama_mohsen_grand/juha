package grand.app.juha.pages.departments.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemDepartmentBinding
import grand.app.juha.pages.departments.viewmodel.ItemDepartmentViewModel
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.splash.models.Department
import grand.app.juha.utils.Constants

class DepartmentAdapter(private var modelList:List<Department> = ArrayList()) : RecyclerView.Adapter<DepartmentAdapter.ViewHolder>() {
    // fetch list data
    var selectedItem : Int = 0
    var mutableLiveData: MutableLiveData<Mutable> = MutableLiveData()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemDepartmentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_department,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    private  val TAG = "DepartmentAdapter"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            Log.d(TAG,""+selectedItem)
            selectedItem = it.position
            mutableLiveData.value = Mutable(Constants.SUBMIT,selectedItem)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun getCurrentId() : Int{
        return if(selectedItem == -1) selectedItem else modelList[selectedItem].id

    }


    fun update(modelList: List<Department>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemDepartmentBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemDepartmentViewModel

        //bint
        fun bind(model: Department) {
            viewModel = ItemDepartmentViewModel(model, adapterPosition,selectedItem == adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}