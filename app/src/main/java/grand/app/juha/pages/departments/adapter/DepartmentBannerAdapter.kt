package grand.app.juha.pages.departments.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemDepartmentSubImageBinding
import grand.app.juha.pages.departments.viewmodel.ItemDepartmentViewModel
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.offer.model.Sub
import grand.app.juha.pages.offer.viewmodel.ItemOfferViewModel
import grand.app.juha.pages.splash.models.Department
import grand.app.juha.utils.Constants

class DepartmentBannerAdapter(private var modelList:List<Sub> = ArrayList()) : RecyclerView.Adapter<DepartmentBannerAdapter.ViewHolder>() {
    // fetch list data
    var selectedItem : Int = 0
    var mutableLiveData: MutableLiveData<Mutable> = MutableLiveData()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemDepartmentSubImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_department_sub_image,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    private  val TAG = "DepartmentAdapter"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            Log.d(TAG,""+selectedItem)
            selectedItem = it.position
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun getCurrentId() : Int{
        return if(selectedItem == -1) selectedItem else modelList[selectedItem].id

    }


    fun update(modelList: List<Sub>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemDepartmentSubImageBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOfferViewModel

        //bint
        fun bind(model: Sub) {
            viewModel = ItemOfferViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}