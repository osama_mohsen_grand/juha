package grand.app.juha.pages.departments.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemDepartmentSubBinding
import grand.app.juha.databinding.ItemDepartmentSubImageBinding
import grand.app.juha.databinding.ItemOfferBinding
import grand.app.juha.pages.departments.viewmodel.ItemDepartmentSubViewModel
import grand.app.juha.pages.offer.model.Sub
import grand.app.juha.pages.offer.viewmodel.ItemOfferViewModel
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.pages.splash.models.Section
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper

class DepartmentSubAdapter(private var modelList:List<Sub> = ArrayList()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    // fetch list data
    var selectedItem : Int = 0
    var category_id = -1
    var mutableLiveData: MutableLiveData<Mutable> = MutableLiveData()
    companion object {
        const val VIEW_TYPE_LIST = 1
        const val VIEW_TYPE_IMAGE = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_LIST) {
            val binding: ItemOfferBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_offer,
                parent,
                false
            )
            return ViewHolderDepartments(
                binding
            )

        }else {
            //VIEW_TYPE_IMAGE
            val binding: ItemDepartmentSubImageBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_department_sub_image,
                parent,
                false
            )
            return ViewHolderImage(
                binding
            )
        }

    }

    private  val TAG = "DepartmentSubAdapter"
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(modelList[position].id != 0){//list
            (holder as ViewHolderDepartments).bind(modelList[position])
            holder.viewModel.mutableLiveData.observeForever {
                Log.d(TAG,"CategoryId:"+category_id)
//                mutableLiveData.value = it as Mutable
                val value = it as Mutable
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
                bundle.putInt(Constants.SUB_CATEGORY_ID,modelList[value.position].id)
                bundle.putString(Constants.TITLE, modelList[value.position].name)
                bundle.putString(Constants.URL, "${URLS.SUB_CATEGORY_PRODUCT}/${modelList[value.position].id}")
                bundle.putBoolean(Constants.FILTER, true)
                MovementHelper.startActivityBase(
                    holder.itemView.context,
                    bundle,
                    ""
                )
            }
        }else{//image
            (holder as ViewHolderImage).bind(modelList[position])
            holder.viewModel.mutableLiveData.observeForever {
                mutableLiveData.value = it as Mutable
            }
        }

    }

//    private  val TAG = "DepartmentSubAdapter"
//    override fun onBindViewHolder(holder: ViewHolderDepartments, position: Int) {
//        holder.bind(modelList[position])
//        holder.viewModel.adapter.mutableLiveData.observeForever {
//            selectedItem = it.position
//            mutableLiveData.value = Mutable(Constants.SUBMIT,selectedItem)
//            notifyDataSetChanged()
//        }
//    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(modelList[position].id != 0) VIEW_TYPE_LIST else VIEW_TYPE_IMAGE
    }

    fun update(modelList: List<Sub>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    inner class ViewHolderDepartments(private val binding: ItemOfferBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOfferViewModel

        //bint
        fun bind(model: Sub) {
            viewModel = ItemOfferViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }

    inner class ViewHolderImage(private val binding: ItemDepartmentSubImageBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOfferViewModel

        //bint
        fun bind(model: Sub) {
            viewModel = ItemOfferViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}