package grand.app.juha.pages.departments.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentDepartmentListBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.departments.viewmodel.DepartmentListViewModel
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject


class DepartmentListFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentDepartmentListBinding
    @Inject
    lateinit var viewModel: DepartmentListViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_department_list, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        bindView()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun bindView() {

        val layoutManager = GridLayoutManager(requireContext(), 3)

        layoutManager.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (position % 10) {
                    0 -> 3
                    else -> 1
                }
                throw IllegalStateException("internal error")
            }
        }
        binding.rvDepartmentsSub.layoutManager = layoutManager
        binding.rvDepartmentsSub.adapter = viewModel.adapterSections
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
            when(mutable.message){
                Constants.PRODUCT_LIST -> {
//                    val bundle = Bundle()
//                    bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
//                    bundle.putInt(Constants.CATEGORY_ID,viewModel.adapter.getCurrentId())
//                    bundle.putString(Constants.URL, "${URLS.SUB_CATEGORY_PRODUCT}/${viewModel.adapter.getCurrentId()}")
//                    bundle.putBoolean(Constants.FILTER, true)
//                    MovementHelper.startActivityBase(
//                        context,
//                        bundle,
//                        ""
//                    )
                }
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}