package grand.app.juha.pages.departments.viewmodel

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.departments.adapter.DepartmentAdapter
import grand.app.juha.pages.departments.adapter.DepartmentBannerAdapter
import grand.app.juha.pages.departments.adapter.DepartmentSubAdapter
import grand.app.juha.pages.offer.model.Sub
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.pages.splash.models.Banner
import grand.app.juha.pages.splash.models.DepartmentSections
import grand.app.juha.pages.splash.models.Section
import grand.app.juha.pages.splash.models.MainSettingsResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject

class DepartmentListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "DepartmentListViewModel"
    var response: MainSettingsResponse =
        UserHelper.retrieveJsonResponse(
            Constants.SETTINGS,
            MainSettingsResponse::class.java
        ) as MainSettingsResponse
    var adapter: DepartmentAdapter
    var adapterSections: DepartmentSubAdapter = DepartmentSubAdapter()
    val adapterSectionShow: ObservableBoolean = ObservableBoolean(false)
    var adapterSubs: DepartmentSubAdapter = DepartmentSubAdapter()
    val adapterSubsShow: ObservableBoolean = ObservableBoolean(false)
    var adapterBanner: DepartmentBannerAdapter = DepartmentBannerAdapter()
    val adapterBannerShow: ObservableBoolean = ObservableBoolean(false)
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor() {
        this.liveDataViewModel = MutableLiveData();
        adapter = DepartmentAdapter(response.data)
        showSection(0)
        adapterEvent()
    }

    fun loadList(index: Int): ArrayList<Sub> {
        adapterSectionShow.set(true)
        val newSubs = ArrayList<Sub>()
        var start = 0
        var counter = 0
        val subHeader = addBanner(index, counter)
        subHeader?.let {
            newSubs.add(it)
        }
        counter++
        while ((start + 9) < response.data[index].subs.size) {
            newSubs.addAll(response.data[index].subs.subList(start, start + 9))
            if (counter < response.data[index].banners.size) {
                val sub = Sub()
                sub.banner = response.data[index].banners[counter]
                newSubs.add(sub)
                counter++
            }
            start += 9
        }
        Log.d(TAG,response.data[index].subs.toString())
        Log.d(TAG,start.toString())
        Log.d(TAG,start.toString())
        if(start < response.data[index].subs.size)
            newSubs.addAll(response.data[index].subs.subList(if( start != 0 ) start+1 else 0, response.data[index].subs.size))
        Log.d(TAG,newSubs.size.toString())
        loadListBanner(counter,index)
        return newSubs
    }




    fun addBanner(index: Int, counter: Int): Sub? {
        if (index < response.data.size && counter < response.data[index].banners.size) {
            val sub = Sub()
            sub.banner = response.data[index].banners[counter]
            return sub
        }
        return null
    }

    fun loadListEmptyBanner(index: Int) : ArrayList<Sub>{
        adapterSubsShow.set(true)
        val newSubs = ArrayList<Sub>()
        newSubs.addAll(response.data[index].subs)
        return newSubs
    }

    fun resetVisibility(){
        adapterSectionShow.set(false)
        adapterBannerShow.set(false)
        adapterSubsShow.set(false)
    }

    fun loadListBanner(counter:Int , index: Int){
        Log.d(TAG,"loadListBanner")
        val newSubsBanner = ArrayList<Sub>()
        for (i in counter until response.data[index].banners.size) {
            val sub = Sub()
            sub.banner = response.data[index].banners[i]
            newSubsBanner.add(sub)
        }
        Log.d(TAG,"newSubsBanner: ${newSubsBanner.size}")
        if(newSubsBanner.isNotEmpty()) {
            adapterBannerShow.set(true)
            adapterBanner.update(newSubsBanner)
        }
    }

    private fun adapterEvent() {
        adapter.mutableLiveData.observeForever {
            val mutable = it as Mutable
            showSection(mutable.position)
        }

        adapterSections.mutableLiveData.observeForever {
            val mutable = it as Mutable
            Log.d(TAG, mutable.position.toString())
            liveDataViewModel.value = Mutable(Constants.PRODUCT_LIST)
        }

        adapterSubs.mutableLiveData.observeForever {
            val mutable = it as Mutable
            Log.d(TAG, mutable.position.toString())
            liveDataViewModel.value = Mutable(Constants.PRODUCT_LIST)
        }

        adapterBanner.mutableLiveData.observeForever {
            val mutable = it as Mutable
            Log.d(TAG, mutable.position.toString())
            liveDataViewModel.value = Mutable(Constants.PRODUCT_LIST)
        }

    }

    fun showSection(position : Int){
        if(position < response.data.size) {
            resetVisibility()
            if (response.data[position].banners.isEmpty()) {
                val list = loadListEmptyBanner(position)
                adapterSubs.update(list)
            } else {
                val list = loadList(position)
                adapterSections.update(list)
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
    }


}
