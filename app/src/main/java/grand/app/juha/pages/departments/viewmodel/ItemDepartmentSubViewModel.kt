package grand.app.juha.pages.departments.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.offer.OffersAdapter
import grand.app.juha.pages.offer.model.Sub
import grand.app.juha.pages.splash.models.Section
import grand.app.juha.utils.Constants

data class ItemDepartmentSubViewModel(val model: Sub, var position: Int) {

//    var adapter: OffersAdapter = OffersAdapter(Sub)
}