package grand.app.juha.pages.favourite

import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.model.paginate.PaginationModel

class ProductListPaginateData(@SerializedName("data") var products : ArrayList<Product>): PaginationModel() {
}