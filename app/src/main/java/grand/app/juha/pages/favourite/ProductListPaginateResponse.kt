package grand.app.juha.pages.favourite

import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class ProductListPaginateResponse(@SerializedName("data") var  data : ProductListPaginateData) : StatusMessage() {
}