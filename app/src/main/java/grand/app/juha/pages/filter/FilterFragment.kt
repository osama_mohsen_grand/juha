package grand.app.juha.pages.filter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentFilterBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.filter.model.FilterResponse
import grand.app.juha.pages.filter.viewmodel.FilterViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.resources.ResourceManager
import javax.inject.Inject


class FilterFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentFilterBinding
    @Inject
    lateinit var viewModel: FilterViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        binding.viewmodel = viewModel
        initViews()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    var rotation = 0

    private fun initViews() {
//        binding.rlItemDepartment.setOnClickListener {
////            rotation += 180
////            binding.imgArrow.animate().rotation(rotation.toFloat()).setDuration(200).start()
//            viewModel.showDepartments.set(!viewModel.showDepartments.get())
////            binding.scroll.smoothScrollTo(0,0)
//
//            binding.scroll.viewTreeObserver
//                .addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
//                    override fun onGlobalLayout() {
//                        binding.scroll.viewTreeObserver.removeOnGlobalLayoutListener(this)
//                        binding.scroll.fullScroll(View.FOCUS_UP)
//                    }
//                })
//        }
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
            when(mutable.message){
                URLS.FILTER -> {
                    viewModel.response = (mutable.obj as FilterResponse)
                    viewModel.setData()
                    setData()
                }
                Constants.BACK -> {
                    requireActivity().onBackPressed()
                }
                Constants.SUBMIT -> {
                    val intent = Intent()
                    intent.putExtra(Constants.FILTER_MODEL,viewModel.request)
                    requireActivity().setResult(Activity.RESULT_OK, intent);
                    requireActivity().finish()
                }
            }
        })
    }

    private fun setData() {
        binding.tvMinPrice.text = viewModel.response.data.priceRange[0].toString()
        binding.tvMaxPrice.text = viewModel.response.data.priceRange[1].toString()

        binding.rangeSeekbar1.setMinValue(viewModel.response.data.priceRange[0].toFloat())
        binding.rangeSeekbar1.setMaxValue(viewModel.response.data.priceRange[1].toFloat())
        binding.rangeSeekbar1.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            viewModel.request.minPrice = minValue.toString()
            viewModel.request.maxPrice = maxValue.toString()
            binding.tvMinPrice.text = minValue.toString()
            binding.tvMaxPrice.text = maxValue.toString()
        }
    }



    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}