package grand.app.juha.pages.filter.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemFilterCategoryBinding
import grand.app.juha.pages.filter.model.Category
import grand.app.juha.pages.filter.viewmodel.category.ItemFilterCategoryViewModel
import grand.app.juha.utils.Constants

class FilterCategoryAdapter(private var modelList:List<Category> = ArrayList()) : RecyclerView.Adapter<FilterCategoryAdapter.ViewHolder>() {
    // fetch list data
    var selected = arrayListOf<Int>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemFilterCategoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_filter_category,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    private val TAG = "FilterCategoryAdapter"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            if(mutable.message == Constants.SUBMIT){

            }
            val index= find(modelList[mutable.position].id)
            Log.d(TAG,index.toString())
            if(index == -1)
                selected.add(modelList[mutable.position].id)
            else
                selected.removeAt(index)
            notifyItemChanged(mutable.position)
        }
    }

    private fun find(categoryId: Int): Int {
        return selected.indexOfFirst {
            it == categoryId
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(
        modelList: ArrayList<Category>,
        categoryIds: ArrayList<Int>?
    ) {
        categoryIds?.let {
            selected.addAll(categoryIds)
        }
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun reset() {
        selected.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemFilterCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemFilterCategoryViewModel

        fun bind(model: Category) {
            viewModel = ItemFilterCategoryViewModel(model, adapterPosition,selected.any { categoryId -> categoryId == model.id  })
            binding.viewmodel = viewModel
        }
    }
}