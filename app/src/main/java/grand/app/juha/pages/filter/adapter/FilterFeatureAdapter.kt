package grand.app.juha.pages.filter.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemFilterFeatureBinding
import grand.app.juha.pages.filter.model.Feature
import grand.app.juha.pages.filter.model.FilterRequest
import grand.app.juha.pages.filter.viewmodel.ItemFilterFeatureViewModel
import grand.app.juha.utils.Constants

class FilterFeatureAdapter(private var modelList:ArrayList<Feature> = ArrayList()) : RecyclerView.Adapter<FilterFeatureAdapter.ViewHolder>() {
    // fetch list data
    var selectedFeatures = arrayListOf<Int>()
    var selectedColors = arrayListOf<Int>()
    private val TAG = "FilterFeatureAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemFilterFeatureBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_filter_feature,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.adapter.mutableLiveData.observeForever {
            Log.d(TAG,"observe")
            val mutable: Mutable = it as Mutable
            if(mutable.message == Constants.SELECT){
                if(mutable.position == -1){
                    selectedColors = mutable.obj as ArrayList<Int>
                }else
                    selectedFeatures = mutable.obj as ArrayList<Int>
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(
        modelList: ArrayList<Feature>,
        request: FilterRequest
    ) {
        request.colorIds?.let {
            this.selectedColors.addAll(request.colorIds!!)
        }
        request.featuresValuesId?.let {
            this.selectedFeatures.addAll(request.featuresValuesId!!)
        }
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun reset() {
        selectedColors.clear()
        selectedFeatures.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemFilterFeatureBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemFilterFeatureViewModel

        fun bind(model: Feature) {
            viewModel = ItemFilterFeatureViewModel(model, adapterPosition,selectedFeatures)
            binding.viewmodel = viewModel
        }
    }
}