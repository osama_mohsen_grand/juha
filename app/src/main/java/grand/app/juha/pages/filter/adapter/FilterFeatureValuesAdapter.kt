package grand.app.juha.pages.filter.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemFilterCategoryBinding
import grand.app.juha.databinding.ItemFilterFeatureValueBinding
import grand.app.juha.pages.filter.model.Category
import grand.app.juha.pages.filter.model.Feature
import grand.app.juha.pages.filter.model.FeaturesValue
import grand.app.juha.pages.filter.viewmodel.ItemFilterFeatureValueViewModel
import grand.app.juha.pages.filter.viewmodel.ItemFilterFeatureViewModel
import grand.app.juha.pages.filter.viewmodel.category.ItemFilterCategoryViewModel
import grand.app.juha.utils.Constants

class FilterFeatureValuesAdapter(private var modelList:List<FeaturesValue> = ArrayList()) : RecyclerView.Adapter<FilterFeatureValuesAdapter.ViewHolder>() {

    // fetch list data
    var selected = arrayListOf<Int>()
    var mutableLiveData = MutableLiveData<Any>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemFilterFeatureValueBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_filter_feature_value,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    private  val TAG = "FilterFeatureValuesAdap"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            Log.d(TAG,"FilterFeatureValuesAdapter")
            val mutable: Mutable = it as Mutable
            val index= find(modelList[mutable.position].id)
            if(index == -1)
                selected.add(modelList[mutable.position].id)
            else
                selected.removeAt(index)
            notifyItemChanged(mutable.position)
            mutableLiveData.value = Mutable(Constants.SELECT,selected,modelList[mutable.position].featureId)

        }
    }

    private fun find(categoryId: Int): Int {
        return selected.indexOfFirst {
            it == categoryId
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: ArrayList<FeaturesValue>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemFilterFeatureValueBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemFilterFeatureValueViewModel

        fun bind(model: FeaturesValue) {
            viewModel = ItemFilterFeatureValueViewModel(model, adapterPosition,selected.any { categoryId -> categoryId == model.id  })
            binding.viewmodel = viewModel
        }
    }
}