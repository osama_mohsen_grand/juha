package grand.app.juha.pages.filter.model


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("count")
    var count: Int = 0,
    @SerializedName("name")
    var name: String = ""
)