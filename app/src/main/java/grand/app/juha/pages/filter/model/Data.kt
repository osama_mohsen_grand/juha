package grand.app.juha.pages.filter.model


import android.util.Size
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.product.model.details.Color

data class Data(
    @SerializedName("categories")
    var categories: ArrayList<Category> = arrayListOf(),
    @SerializedName("colors")
    var colors: ArrayList<Color> = arrayListOf(),
    @SerializedName("price_range")
    var priceRange: ArrayList<Int> = arrayListOf(),
    @SerializedName("features")
    var features: ArrayList<Feature> = arrayListOf()
)