package grand.app.juha.pages.filter.model


import com.google.gson.annotations.SerializedName

data class Feature(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("type")
    var type: String = "",
    @SerializedName("features_values")
    var featuresValues: ArrayList<FeaturesValue> = arrayListOf(),
    @SerializedName("text")
    var text: String = ""
)