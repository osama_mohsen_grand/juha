package grand.app.juha.pages.filter.model


import com.google.gson.annotations.SerializedName

data class FeaturesValue(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("feature_id")
    var featureId: Int = 0,
    @SerializedName("value")
    var value: String = "",
    @SerializedName("count")
    var count: Int = 0
)