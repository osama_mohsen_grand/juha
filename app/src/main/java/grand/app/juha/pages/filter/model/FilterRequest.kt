package grand.app.juha.pages.filter.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FilterRequest(  @Transient var TAG: String = "FilterRequest") : java.io.Serializable  {



    @SerializedName("category_id")
    @Expose
    var categoryIds: ArrayList<Int> = arrayListOf()

    @SerializedName("sub_category_id")
    @Expose
    var subCategoriesId: ArrayList<Int>? = null

    @SerializedName("color_id")
    @Expose
    var colorIds: ArrayList<Int>? = null

    @SerializedName("features_values_id")
    @Expose
    var featuresValuesId: ArrayList<Int>? = null

    @SerializedName("min_price")
    @Expose
    var minPrice: String? = null

    @SerializedName("max_price")
    @Expose
    var maxPrice: String? = null

    @SerializedName("sortby")
    @Expose
    var sortBy: Int? = null

    @SerializedName("shop_id")
    @Expose
    var shopId: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    fun isClear(): Boolean {
        if ((subCategoriesId == null || subCategoriesId!!.isEmpty()) && ( colorIds == null || colorIds!!.isEmpty()) && (featuresValuesId == null  || featuresValuesId!!.isEmpty()) && (minPrice == null) && (maxPrice == null) && (sortBy == null || sortBy ==0) && (shopId == null || shopId == 0) && (name == "" || name == null))
            return true
        return false
    }

    fun reset(){
        subCategoriesId = null
        colorIds = null
        featuresValuesId = null
        minPrice = null
        maxPrice = null
        sortBy = null
        shopId = null
        name = null
    }

}