package grand.app.juha.pages.filter.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class FilterResponse(
    @SerializedName("data")
    var data: Data = Data()
) : StatusMessage()