package grand.app.juha.pages.filter.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetFilterRequest(
    @SerializedName("category_id")
    @Expose
    var categoryId: String? = null,
    @SerializedName("sub_category_id")
    @Expose
    var subCategoryId: String? = null) {
}