package grand.app.juha.pages.filter.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableBoolean
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.filter.adapter.FilterCategoryAdapter
import grand.app.juha.pages.filter.adapter.FilterFeatureAdapter
import grand.app.juha.pages.filter.model.*
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FilterViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    private val TAG = "RatesViewModel"
    lateinit var response: FilterResponse
    var adapterCategory: FilterCategoryAdapter = FilterCategoryAdapter()
    var adapterFeatures: FilterFeatureAdapter = FilterFeatureAdapter()

    var showDepartments: ObservableBoolean = ObservableBoolean(true)
    var haveCategory: ObservableBoolean = ObservableBoolean(true)


    lateinit var request: FilterRequest

//    var adapterFeatures:

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
    }

    fun setArgument(bundle: Bundle) {
        val getFilterRequest = GetFilterRequest()
        if(bundle.containsKey(Constants.CATEGORY_ID))
            getFilterRequest.categoryId = bundle.getInt(Constants.CATEGORY_ID).toString()
        else if(bundle.containsKey(Constants.SUB_CATEGORY_ID))
            getFilterRequest.subCategoryId = bundle.getInt(Constants.SUB_CATEGORY_ID).toString()

        repository.getFilter(getFilterRequest)
        request = bundle.getSerializable(Constants.FILTER) as FilterRequest
    }

    fun reset(){
        request.reset()
        adapterFeatures.reset()
        adapterCategory.reset()
    }

    fun back(){
        getLiveData().value = Mutable(Constants.BACK)
    }

    fun categorySubmit(){
        showDepartments.set(!showDepartments.get())
    }

    fun submit(){
        request.subCategoriesId = if(adapterCategory.selected.isNotEmpty())  adapterCategory.selected else null
        request.colorIds = if(adapterFeatures.selectedColors.isNotEmpty())  adapterFeatures.selectedColors else null
        request.featuresValuesId = if(adapterFeatures.selectedFeatures.isNotEmpty())  adapterFeatures.selectedFeatures else null
        getLiveData().value = Mutable(Constants.SUBMIT)
    }


    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        if(response.data.categories.size > 0) {
            adapterCategory.update(response.data.categories, request.subCategoriesId)
        }else{
            haveCategory.set(false)
        }

        val feature = Feature()
        feature.id = -1
        feature.name = getString(R.string.color)

        response.data.colors.forEach {
            val featureValue = FeaturesValue()
            featureValue.count = it.count
            featureValue.id = it.id
            featureValue.value = it.color
            featureValue.featureId = -1
            feature.featuresValues.add(featureValue)
        }
        response.data.features.add(0,feature)
        adapterFeatures.update(response.data.features,request)
        show.set(true)
    }

}