package grand.app.juha.pages.filter.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.pages.filter.model.Feature
import grand.app.juha.pages.filter.model.FeaturesValue
import grand.app.juha.pages.rates.model.Rate
import grand.app.juha.pages.settings.notification.model.Notification
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager

data class ItemFilterFeatureListViewModel(val model: FeaturesValue, var position: Int, var selected : Boolean) {

    var mutableLiveData = MutableLiveData<Any>()
    
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}