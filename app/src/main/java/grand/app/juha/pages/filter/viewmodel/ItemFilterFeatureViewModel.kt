package grand.app.juha.pages.filter.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.pages.filter.adapter.FilterFeatureValuesAdapter
import grand.app.juha.pages.filter.model.Feature

data class ItemFilterFeatureViewModel(
    val model: Feature,
    var position: Int,
    var selectedFeatures: ArrayList<Int>
) {

    var mutableLiveData = MutableLiveData<Any>()
    var adapter: FilterFeatureValuesAdapter = FilterFeatureValuesAdapter(model.featuresValues)
    init {
        adapter.selected.addAll(selectedFeatures)
    }

    val isShow: ObservableBoolean = ObservableBoolean(false)


    fun toggle(){
        isShow.set(!isShow.get())
//        mutableLiveData.value = Mutable(Constants.TOGGLE , position)
    }

}