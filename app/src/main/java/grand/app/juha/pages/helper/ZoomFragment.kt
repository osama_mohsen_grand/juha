package grand.app.juha.pages.helper

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.databinding.FragmentZoomBinding
import grand.app.juha.utils.Constants


class ZoomFragment : BaseFragment() {

    lateinit var binding: FragmentZoomBinding
    val viewModel = ZoomViewModel()



    private val TAG = "ZoomFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_zoom, container, false)


        viewModel.image.get()?.let { Log.d(TAG, it) }
        binding.viewmodel = viewModel
        arguments?.let {
            viewModel.image.set(it.getString(Constants.IMAGE,""))
        }


//        binding.ivZoomable.setImageUrl(viewModel.image.get())


        return binding.root
    }


}