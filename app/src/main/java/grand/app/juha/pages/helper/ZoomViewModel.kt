package grand.app.juha.pages.helper

import androidx.databinding.ObservableField
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.offer.OffersAdapter
import grand.app.juha.pages.offer.model.OffersResponse
import grand.app.juha.pages.product.repository.ProductRepository
import javax.inject.Inject

class ZoomViewModel : BaseViewModel() {

    var image: ObservableField<String> = ObservableField()

    override fun onCleared() {
        super.onCleared()
    }

}