package grand.app.juha.pages.home.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemHomeBinding
import grand.app.juha.pages.home.model.FeaturedCategory
import grand.app.juha.pages.home.viewmodel.ItemHomeViewModel
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper

class HomeAdapter(
    var modelList: List<FeaturedCategory> = ArrayList(),
    val repository: ProductRepository
) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    // fetch list data
    var favPosition: Int = 0
    var position: Int = -1
    var number: Int = -1
    lateinit var fragment: Fragment
    var checkedFragment = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemHomeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_home,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            this.position = it.position
            number = it.number
            if (mutable.message == Constants.MORE) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
                bundle.putInt(Constants.CATEGORY_ID, modelList[position].id)
                bundle.putString(
                    Constants.URL,
                    "${URLS.CATEGORY_PRODUCT}/${modelList[position].id}"
                )
                bundle.putString(Constants.TITLE, "${modelList[position].name}")
                bundle.putBoolean(Constants.FILTER, true)
                MovementHelper.startActivityBase(
                    holder.itemView.context,
                    bundle,
                    ""
                )
            }
        }
    }


    fun setFragmentPage(fragment: Fragment) {
        this.fragment = fragment
        checkedFragment = true
    }

    override fun getItemCount(): Int {
        return modelList.size
    }


    fun update(modelList: List<FeaturedCategory>) {
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun updateFavourite(id: Int?, favouriteStatus: Int?) {
        for (i in modelList.indices) {
//            if(modelList[i].id == id) {
//                if (favouriteStatus != null) {
//
//                }
//            }

            for (j in modelList[i].products.indices) {
                favouriteStatus?.let {
                    modelList[i].products[j].isFavourite = favouriteStatus
                    notifyItemChanged(i)
                }
            }
        }
    }


    inner class ViewHolder(private val binding: ItemHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemHomeViewModel

        //bint
        fun bind(model: FeaturedCategory) {
            if (checkedFragment) {
                viewModel = ItemHomeViewModel(model, adapterPosition, repository, fragment)
                binding.viewmodel = viewModel
                viewModel.mutableLiveData
            }
        }
    }
}