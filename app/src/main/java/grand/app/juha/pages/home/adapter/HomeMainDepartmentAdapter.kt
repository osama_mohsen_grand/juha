package grand.app.juha.pages.home.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemHomeMainDepartmentBinding
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.home.viewmodel.ItemHomeMainDepartmentViewModel
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper

class HomeMainDepartmentAdapter(private var modelList:List<Category> = ArrayList()) : RecyclerView.Adapter<HomeMainDepartmentAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemHomeMainDepartmentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_home_main_department,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]

            val bundle = Bundle()
            bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
            bundle.putInt(Constants.CATEGORY_ID,modelList[position].id)
            bundle.putString(Constants.URL, "${URLS.CATEGORY_PRODUCT}/${modelList[position].id}")
            bundle.putString(Constants.TITLE, "${modelList[position].name}")
            bundle.putBoolean(Constants.FILTER, true)
            MovementHelper.startActivityBase(
                holder.itemView.context,
                bundle,
                ""
            )

            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: List<Category>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemHomeMainDepartmentBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemHomeMainDepartmentViewModel

        //bint
        fun bind(model: Category) {
            viewModel = ItemHomeMainDepartmentViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}