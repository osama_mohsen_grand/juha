package grand.app.juha.pages.home.model


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("icon")
    var icon: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = ""
)