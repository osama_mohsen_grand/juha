package grand.app.juha.pages.home.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.product.model.list.Product

data class Data(
    @SerializedName("best_offers")
    var bestOffer: ArrayList<Product> = arrayListOf(),
    @SerializedName("categories")
    var categories: List<Category> = listOf(),
    @SerializedName("featured_categories")
    var featuredCategories: List<FeaturedCategory> = listOf(),
    @SerializedName("most_rated")
    var mostRated: ArrayList<Product> = arrayListOf(),
    @SerializedName("not-rated-products")
    var notRatedProducts: ArrayList<Product> = arrayListOf(),
    @SerializedName("slider")
    @Expose
    var slider: ArrayList<Slider> = ArrayList()
)