package grand.app.juha.pages.home.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.splash.models.Banner

data class FeaturedCategory(
    @SerializedName("banner")
    var banner: Banner? ,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("products")
    var products: ArrayList<Product> = arrayListOf()
)