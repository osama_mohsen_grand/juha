package grand.app.juha.pages.home.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class HomeResponse(
    @SerializedName("data")
    @Expose
    var data: Data = Data()

): StatusMessage()