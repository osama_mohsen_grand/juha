package grand.app.juha.pages.home.model


import com.google.gson.annotations.SerializedName

data class Slider(
    @SerializedName("attribute_id")
    var attributeId: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("img")
    var img: String = "",
    @SerializedName("link_type")
    var linkType: Int = 0
)