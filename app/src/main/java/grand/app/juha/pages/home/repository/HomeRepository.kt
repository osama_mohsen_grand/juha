package grand.app.juha.pages.home.repository

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.pages.home.model.HomeResponse
import grand.app.juha.pages.repository.BaseRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }


    fun home(): Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.HOME, Any(), HomeResponse::class.java,
            URLS.HOME, true)
    }
}