package grand.app.juha.pages.home.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialog
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentHomeBinding
import grand.app.juha.databinding.LayoutAddRateProductBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.home.model.HomeResponse
import grand.app.juha.pages.home.viewmodel.HomeViewModel
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.pages.rates.helper.DialogBottomRateProductHelper
import grand.app.juha.pages.rates.viewmodel.AddRateProductViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject


class HomeFragment : BaseFragment() {
    val TAG: String = this::class.java.name


//    lateinit var dialog: DialogRateProductHelper
    lateinit var sheetDialog: BottomSheetDialog
    private lateinit var binding: FragmentHomeBinding

    @Inject
    lateinit var viewModel: HomeViewModel
    lateinit var dailog : DialogBottomRateProductHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        viewModel.setFragment(this)
        binding.viewmodel = viewModel
        dailog = DialogBottomRateProductHelper(inflater,viewModel.productRepository)
//        dialog  = DialogRateProductHelper(layoutInflater,viewModel.productRepository)
//        initDialog()



//        sortBinding.setViewModel(viewModel)

//        initializeProgress()
//        showProgress()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        binding.rvHomeRates.itemAnimator = null
        binding.rvHomeBestOffers.itemAnimator = null
        binding.rvHomeDepartment.itemAnimator = null
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
//            Log.d(TAG, mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            when (mutable.message) {
                URLS.HOME -> {
                    viewModel.setData(mutable.obj as HomeResponse)
                    if(viewModel.response.data.notRatedProducts.size > 0){
                        val product = viewModel.response.data.notRatedProducts[0]
                        viewModel.response.data.notRatedProducts.removeAt(0)
                        dailog.rates.addAll(viewModel.response.data.notRatedProducts)
                        dailog.setProduct(product.id,product.name)
                        dailog.show()
                    }
                }
                Constants.MAIN -> {
                    (requireActivity() as MainActivity).goToDepartments()
                }
                Constants.OFFERS -> {
                    (requireActivity() as MainActivity).goToOffers()
                }
                Constants.RATES -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
                    bundle.putString(Constants.URL, URLS.MOST_RATED)
                    MovementHelper.startActivityBase(
                        context,
                        bundle,
                        getString(R.string.most_rates)
                    )
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "request ${requestCode} , result ${resultCode} ")
        if (requestCode == Constants.FAVOURITE_RESULT && resultCode == RESULT_OK) {
            val id = data?.getIntExtra(Constants.ID, -1)
            val favouriteStatus = data?.getIntExtra(Constants.FAVOURITE, -1)
            if (id != -1 && favouriteStatus != -1)
                viewModel.updateFavourite(id, favouriteStatus)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}