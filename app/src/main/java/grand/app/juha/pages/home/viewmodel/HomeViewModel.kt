package grand.app.juha.pages.home.viewmodel

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.home.adapter.HomeAdapter
import grand.app.juha.pages.home.adapter.HomeMainDepartmentAdapter
import grand.app.juha.pages.product.adapter.ProductAdapter
import grand.app.juha.pages.home.model.HomeResponse
import grand.app.juha.pages.home.repository.HomeRepository
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HomeViewModel : BaseViewModel {
    @Inject
    lateinit var repository: HomeRepository;

    @Inject
    lateinit var productRepository: ProductRepository;

    var compositeDisposable: CompositeDisposable

    private var liveData: LiveData<Mutable> = MutableLiveData()
    var type: Int = 0
    private val TAG = "HomeViewModel"
    var response: HomeResponse = HomeResponse()
    var homeMainAdapter: HomeMainDepartmentAdapter
    var bestOfferAdapter: ProductAdapter

    var homeAdapter: HomeAdapter
    var mostRates: ProductAdapter

    val bestOfferAppear = ObservableBoolean(true)
    val homeListVisiblity = ObservableBoolean(false)

    var title: ObservableField<String> = ObservableField("")

    lateinit var fragmentPage: Fragment

    @Inject
    constructor(repository: HomeRepository, productRepository: ProductRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
        homeMainAdapter = HomeMainDepartmentAdapter()

        bestOfferAdapter = ProductAdapter(
            limit_size = 3,
            repository = productRepository
        )
        homeAdapter = HomeAdapter(repository = productRepository)
        mostRates = ProductAdapter(
            limit_size = 3,
            repository = productRepository
        )
        repository.home()
    }

    fun setFragment(fragmentPage: Fragment) {
        this.fragmentPage = fragmentPage
        bestOfferAdapter.setFragmentPage(fragmentPage)
        mostRates.setFragmentPage(fragmentPage)
        homeAdapter.setFragmentPage(fragmentPage)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }


    fun setData(response: HomeResponse) {
        this.response = response
        homeListVisiblity.set(true)
        homeMainAdapter.update(response.data.categories)
        bestOfferAdapter.update(response.data.bestOffer)
        homeAdapter.update(response.data.featuredCategories)
        mostRates.update(response.data.mostRated)
        show.set(true)
        notifyChange()
    }

    fun mainMoreSubmit(){
        getLiveData().value = Mutable(Constants.MAIN)
    }

    fun offersMoreSubmit(){
        getLiveData().value = Mutable(Constants.OFFERS)
    }

    fun ratesMoreSubmit(){
        getLiveData().value = Mutable(Constants.RATES)
    }

    fun updateFavourite(id: Int?, favouriteStatus: Int?) {
        Log.d(TAG,"updateFavourite")
        bestOfferAdapter.updateFavourite(id,favouriteStatus)
        mostRates.updateFavourite(id,favouriteStatus)
        homeAdapter.updateFavourite(id,favouriteStatus)
    }

}