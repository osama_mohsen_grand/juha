package grand.app.juha.pages.home.viewmodel

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.product.adapter.ProductAdapter
import grand.app.juha.pages.home.model.FeaturedCategory
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants

data class ItemHomeViewModel(
    val model: FeaturedCategory,
    var position: Int,
    val repository: ProductRepository,
    val fragmentPage: Fragment
) {
    val products =
        ProductAdapter(
            model.products,
            limit_size = 2,
            repository = repository
        )

    init {

        products.setFragmentPage(fragmentPage)
        products.mutableLiveData.observeForever {
            val mutable = it as Mutable
            mutableLiveData.value = Mutable(Constants.SUBMIT,position,mutable.position)
        }
    }

    var mutableLiveData = MutableLiveData<Any>()


    fun homeMore(){
        mutableLiveData.value = Mutable(Constants.MORE,position)
    }
}