package grand.app.juha.pages.intro

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentIntroBinding
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.LanguagesHelper
import kotlinx.android.synthetic.main.fragment_intro.view.*

class IntroFragment : BaseFragment() {
    private lateinit var binding: FragmentIntroBinding

    private  val TAG = "IntroFragment"
    lateinit var viewModel: IntroViewModel
    private var intro = ArrayList<String>()
    private var desc = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_intro, container, false)




        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        intro.add(getString(grand.app.juha.R.string.intro1))
        intro.add(getString(grand.app.juha.R.string.intro2))
        intro.add(getString(grand.app.juha.R.string.intro3))

        desc.add(getString(grand.app.juha.R.string.desc1))
        desc.add(getString(grand.app.juha.R.string.desc2))
        desc.add(getString(grand.app.juha.R.string.desc3))

        viewModel = IntroViewModel(childFragmentManager)
        binding.viewModel = viewModel


        binding.tvTitle.text = getString(grand.app.juha.R.string.intro1)
        binding.tvMessage.text = getString(grand.app.juha.R.string.desc1)

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
                Log.d(TAG, "onPageScrollStateChanged: $state")
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Log.d(TAG, "onPageScrolled: $position")
            }
            override fun onPageSelected(position: Int) {
                Log.d(TAG, "onPageSelected: $position")
            }

        })


        setEvent()
        LanguagesHelper.setLanguage(Constants.DEFAULT_LANGUAGE)
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            when(mutable.message){
                Constants.FINISH -> {
                    finishAllActivities()
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle, "")
                }
                Constants.NEXT -> {
                    binding.viewPager.currentItem = viewModel.currentPosition

                    binding.tvTitle.text = intro[viewModel.currentPosition]
                    binding.tvMessage.text = desc[viewModel.currentPosition]
                }
            }
        })
    }
}