package grand.app.juha.pages.intro

import android.view.Gravity
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import grand.app.juha.base.BaseViewModel
import grand.app.juha.base.model.TabModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.address.list.model.AddressListResponse
import grand.app.juha.pages.intro.ui.Slide1Fragment
import grand.app.juha.pages.intro.ui.Slide2Fragment
import grand.app.juha.pages.intro.ui.Slide3Fragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.tabLayout.SwapAdapter
import java.util.*

class IntroViewModel(var fragmentManager: FragmentManager) : BaseViewModel() {

    var type: Int = 0
    private val TAG = this::class.java.name
    var response: AddressListResponse = AddressListResponse()
    var currentPosition = 0

    val previous = ObservableBoolean (false)
    val next = ObservableBoolean (true)
    val nextText = ObservableBoolean (false)


    var title: ObservableField<String> =
        ObservableField("")

    fun prev(){
        currentPosition--
        buttonStatus()
    }

    fun next(){
        if(currentPosition == 3) {
            getLiveData().value = Mutable(Constants.FINISH)
        }else
            getLiveData().value = Mutable(Constants.NEXT)

        currentPosition++
        buttonStatus()

    }

    companion object {
        @BindingAdapter("adapter")
        @JvmStatic
        fun adapter(
            wPager: ViewPager,
            fragmentManager: FragmentManager
        ) {
            val tabModels: ArrayList<TabModel> = ArrayList()
            tabModels.add(TabModel("", Slide1Fragment()))
            tabModels.add(TabModel("", Slide2Fragment()))
            tabModels.add(TabModel("", Slide3Fragment()))
            val adapter =  SwapAdapter(fragmentManager,tabModels)
            wPager.adapter = adapter
            wPager.isEnabled = false
            val layoutParams = ViewPager.LayoutParams()
            layoutParams.width = ViewPager.LayoutParams.MATCH_PARENT
            layoutParams.height = ViewPager.LayoutParams.WRAP_CONTENT
            layoutParams.gravity = Gravity.BOTTOM
        }
    }


    override fun onCleared() {
        super.onCleared()
    }

    private fun buttonStatus() {
        if(currentPosition >= 1) {
            next.set(false)
            nextText.set(true)
            previous.set(true)
        }else{
            next.set(true)
            nextText.set(false)
            previous.set(false)
        }
    }


}