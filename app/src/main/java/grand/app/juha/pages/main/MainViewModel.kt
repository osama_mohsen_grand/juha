package grand.app.juha.pages.main

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.home.adapter.HomeAdapter
import grand.app.juha.pages.home.adapter.HomeMainDepartmentAdapter
import grand.app.juha.pages.home.model.HomeResponse
import grand.app.juha.pages.home.repository.HomeRepository
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainViewModel : BaseViewModel {
    @Inject
    lateinit var repository: HomeRepository;

    val searchBarVisiblity = ObservableBoolean(false)
    val mainHeaderVisiblity = ObservableBoolean(false)

    var title: ObservableField<String> = ObservableField("")

    @Inject
    constructor(repository: HomeRepository) {
        this.repository = repository
    }

    fun search(){
        getLiveData().value = Mutable(Constants.SEARCH)
    }

    fun mainHeaderText(title: String){
        this.title.set(title)
        mainHeaderVisiblity.set(true)
    }

    fun hideMainHeader(){
        mainHeaderVisiblity.set(false)
    }
}