package grand.app.juha.pages.notfound

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.StatusMessage
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentCartBinding
import grand.app.juha.databinding.FragmentNotfoundBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.viewmodel.CartViewModel
import grand.app.juha.pages.product.model.FavouriteResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.SharedPreferenceHelper
import javax.inject.Inject

class NotFoundFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentNotfoundBinding
    @Inject
    lateinit var viewModel: NotFoundViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notfound, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        // Inflate the layout for this fragment
        return binding.root
    }
    override fun onResume() {
        super.onResume()
    }
}