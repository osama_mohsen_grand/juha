package grand.app.juha.pages.notfound

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.settings.notification.NotificationAdapter
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class NotFoundViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        repository.setLiveData(getLiveData())
        noData()
    }

    override fun onCleared() {
        super.onCleared()
    }


}