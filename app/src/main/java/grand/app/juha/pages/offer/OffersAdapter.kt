package grand.app.juha.pages.offer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemOfferBinding
import grand.app.juha.pages.offer.model.Sub
import grand.app.juha.pages.offer.viewmodel.ItemOfferViewModel

class OffersAdapter(private var modelList:List<Sub> = ArrayList()) : RecyclerView.Adapter<OffersAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    var mutableLiveData: MutableLiveData<Mutable> = MutableLiveData()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemOfferBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_offer,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    private val TAG = "OffersAdapter"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
//            selectedItem = it.position
//            mutableLiveData.value = it as Mutable
////            Log.d(TAG,selectedItem.toString())
//            val model = modelList[mutable.position]
//            notifyItemChanged(selectedItem)
            mutableLiveData.value = it
        }
    }
    

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: List<Sub>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemOfferBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOfferViewModel

        //bint
        fun bind(model: Sub) {
            viewModel = ItemOfferViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}