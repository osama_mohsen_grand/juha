package grand.app.juha.pages.offer.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.home.model.Slider

class OfferDetails(
    @SerializedName("products")
    @Expose
    var products: OfferProductDetails = OfferProductDetails(),
    @SerializedName("banners")
    @Expose
    var banners: ArrayList<Slider> = ArrayList()
) {
}