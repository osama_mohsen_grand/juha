package grand.app.juha.pages.offer.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import grand.app.juha.pages.product.model.list.Product

class OfferDetailsResponse(
    @SerializedName("data")
    @Expose
    var data: OfferDetails = OfferDetails()
) : StatusMessage() {

}