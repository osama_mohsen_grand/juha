package grand.app.juha.pages.offer.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.model.paginate.PaginationModel

class OfferProductDetails(
    @SerializedName("data")
    @Expose
    var data: ArrayList<Product> = ArrayList()
) : PaginationModel() {
}