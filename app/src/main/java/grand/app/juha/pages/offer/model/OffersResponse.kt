package grand.app.juha.pages.offer.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import java.io.Serializable

data class OffersResponse(
    @SerializedName("data")
    @Expose
    var data: List<Sub> = ArrayList()
) : StatusMessage() , Serializable