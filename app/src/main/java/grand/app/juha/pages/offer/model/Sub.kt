package grand.app.juha.pages.offer.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.splash.models.Banner
import java.io.Serializable

data class Sub(
    @SerializedName("icon")
    @Expose
    var icon: String = "",
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("name")
    @Expose
    var name: String = "",
    @SerializedName("categories_id")
    @Expose
    var categoriesId: Int = 0,
    @SerializedName("status")
    @Expose
    var status: Int = 0,
    @SerializedName("banner")
    @Expose
    var banner: Banner? = null
) : Serializable