package grand.app.juha.pages.offer.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentNotificationListBinding
import grand.app.juha.databinding.FragmentOfferDetailsBinding
import grand.app.juha.databinding.FragmentOffersBinding
import grand.app.juha.pages.departments.ui.DepartmentListFragment
import grand.app.juha.pages.offer.model.OfferDetailsResponse
import grand.app.juha.pages.offer.model.OffersResponse
import grand.app.juha.pages.offer.viewmodel.OfferDetailsViewModel
import grand.app.juha.pages.offer.viewmodel.OfferListViewModel
import grand.app.juha.pages.search.SearchFragment
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.notification.viewmodel.NotificationListViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject

class OfferDetailsFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentOfferDetailsBinding
    @Inject
    lateinit var viewModel: OfferDetailsViewModel

    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var loading = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offer_details, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        binding.viewmodel = viewModel
        viewModel.productsAdapter.setFragmentPage(this)
        init()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun init() {
        val layoutManagerItems = GridLayoutManager(context,2)
        binding.rvProducts.apply {
            adapter = viewModel.productsAdapter
            layoutManager = layoutManagerItems
        }

        binding.rvProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = layoutManagerItems.childCount
                    totalItemCount = layoutManagerItems.itemCount
                    pastVisiblesItems = layoutManagerItems.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            if(!viewModel.progress.get() && viewModel.more) {
                                loading = false
                                viewModel.callService()
                                loading = true


                            }
                        }
                    }
                }else{
                    visibleItemCount = layoutManagerItems.childCount
                    totalItemCount = layoutManagerItems.itemCount
                }
            }
        })
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
            when(mutable.message){
                URLS.INNER_OFFER -> {
                    viewModel.response = (mutable.obj as OfferDetailsResponse)
                    viewModel.setData()
//                    (binding.rvProducts.layoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
//                        override fun getSpanSize(position: Int): Int {
//                            return 1
//                        }
//                    }
//                    viewModel.productsAdapter.notifyDataSetChanged()
                }
                Constants.BACK -> {
                    requireActivity().finish()
                }
                Constants.SEARCH -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, SearchFragment::class.java.name)
                    MovementHelper.startActivityBase(requireContext(),bundle);
                }
                Constants.MORE -> {
                    requireActivity().finishAffinity()
                    val intent = Intent(requireActivity(),MainActivity::class.java)
                    intent.putExtra(Constants.PAGE,DepartmentListFragment::class.java.name)
                    startActivity(intent)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.FAVOURITE_RESULT && resultCode == Activity.RESULT_OK) {
            val id = data?.getIntExtra(Constants.ID, -1)
            val favouriteStatus = data?.getIntExtra(Constants.FAVOURITE, -1)
            if (id != -1 && favouriteStatus != -1)
                viewModel.productsAdapter.updateFavourite(id, favouriteStatus)
        }
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}