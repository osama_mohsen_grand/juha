package grand.app.juha.pages.offer.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentNotificationListBinding
import grand.app.juha.databinding.FragmentOffersBinding
import grand.app.juha.pages.offer.model.OffersResponse
import grand.app.juha.pages.offer.viewmodel.OfferListViewModel
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.notification.viewmodel.NotificationListViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject

class OfferListFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentOffersBinding
    @Inject
    lateinit var viewModel: OfferListViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == URLS.OFFERS) {
                viewModel.response = (mutable.obj as OffersResponse)
                viewModel.setData()
            }
        })
        viewModel.adapter.mutableLiveData.observe(viewLifecycleOwner, Observer {
            if(it.message == Constants.SUBMIT){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,OfferDetailsFragment::class.java.name)
                bundle.putSerializable(Constants.OFFERS,viewModel.response)
                bundle.putInt(Constants.ID,viewModel.response.data[it.position].id)
                bundle.putString(Constants.TITLE,getString(R.string.offers))
                MovementHelper.startActivityBase(requireContext(),bundle,"")
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}