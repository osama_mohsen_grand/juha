package grand.app.juha.pages.offer.viewmodel

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.offer.model.Sub
import grand.app.juha.utils.Constants

data class ItemOfferViewModel(val model: Sub, var position: Int) {

    var mutableLiveData = MutableLiveData<Any>()

    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}