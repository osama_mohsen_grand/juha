package grand.app.juha.pages.offer.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.home.adapter.HomeAdapter
import grand.app.juha.pages.home.adapter.HomeMainDepartmentAdapter
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.product.adapter.ProductAdapter
import grand.app.juha.pages.home.model.HomeResponse
import grand.app.juha.pages.home.repository.HomeRepository
import grand.app.juha.pages.offer.OffersAdapter
import grand.app.juha.pages.offer.model.OfferDetailsResponse
import grand.app.juha.pages.offer.model.OffersResponse
import grand.app.juha.pages.product.model.details.ProductRequest
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class OfferDetailsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    var more: Boolean = true
    var currentPosition = 0

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var id: Int = 0
    lateinit var offers: OffersResponse
    var response: OfferDetailsResponse = OfferDetailsResponse()
    var adapter: HomeMainDepartmentAdapter = HomeMainDepartmentAdapter()
    var productsAdapter : ProductAdapter
    lateinit var fragmentPage:Fragment
    val progress : ObservableBoolean = ObservableBoolean(false)

    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
        productsAdapter = ProductAdapter(repository = repository)
        productsAdapter.isCenter = true
    }

    fun setArgument(bundle: Bundle) {
        id = bundle.getInt(Constants.ID)
        title.set(bundle.getString(Constants.TITLE))
        if (bundle.containsKey(Constants.OFFERS)) {
            offers = bundle.getSerializable(Constants.OFFERS) as OffersResponse
            val categories = ArrayList<Category>()
            offers.data.forEach {
                val category = Category()
                category.id = it.id
                category.name = it.name
                category.icon = it.icon
                categories.add(category)
            }
            adapter.update(categories)
        }
        callService()
    }

    fun search(){
        getLiveData().value = Mutable(Constants.SEARCH)
    }

    fun back(){
        getLiveData().value = Mutable(Constants.BACK)
    }

    fun more(){
        getLiveData().value = Mutable(Constants.MORE)
    }

    fun callService(){
        if(more) {
            currentPosition++
            repository.getOfferDetails(id, currentPosition)
        }
    }



    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        more = (response.data.products.nextPageUrl != null)
        when(currentPosition){
            1 -> productsAdapter.update(response.data.products.data)
            else -> productsAdapter.updateInserted(response.data.products.data)
        }
        currentPosition++
        showPage(true)
    }

}