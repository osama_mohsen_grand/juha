package grand.app.juha.pages.offer.viewmodel

import androidx.databinding.ObservableField
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.offer.OffersAdapter
import grand.app.juha.pages.offer.model.OffersResponse
import grand.app.juha.pages.product.repository.ProductRepository
import javax.inject.Inject

class OfferListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;

    var type: Int = 0
    private val TAG = "OfferListViewModel"
    lateinit var response: OffersResponse
    var adapter: OffersAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        repository.setLiveData(getLiveData())
        adapter = OffersAdapter()
        repository.getOffers()
    }

    override fun onCleared() {
        super.onCleared()
    }

    fun setData() {
        adapter.update(response.data)
        show.set(true)
    }

}