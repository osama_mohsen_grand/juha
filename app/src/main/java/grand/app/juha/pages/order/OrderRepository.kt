package grand.app.juha.pages.order

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.StatusMessage
import grand.app.juha.pages.social.model.SocialResponse
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.pages.address.list.model.AreaListResponse
import grand.app.juha.pages.order.model.OrderListResponse
import grand.app.juha.pages.order.model.RefundItemRequest
import grand.app.juha.pages.order.model.details.OrderDetailsResponse
import grand.app.juha.pages.order.model.reason.ReasonResponse
import grand.app.juha.pages.repository.BaseRepository
import grand.app.juha.pages.settings.complain.ComplainRequest
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.term.SettingsResponse
import grand.app.juha.pages.settings.term.model.TermsResponse
import grand.app.juha.pages.splash.models.MainSettingsResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }

    fun getOrders() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.ORDERS, Any(), OrderListResponse::class.java,
            URLS.ORDERS, true)
    }

    fun sortOrder(id: Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.SORT_ORDER+id, Any(), OrderListResponse::class.java,
            URLS.ORDERS, true)
    }

    fun getOrderDetails(id: Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.ORDER_DETAILS+id, Any(), OrderDetailsResponse::class.java,
            URLS.ORDER_DETAILS, true)
    }

    fun cancelOrder(id: Int) : Disposable{
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.ORDER_CANCEL+id, Any(), StatusMessage::class.java,
            URLS.ORDER_CANCEL, true)
    }

    fun getReasons(): Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.REASONS, Any(), ReasonResponse::class.java,
            URLS.REASONS, true)
    }

    fun refund(request: RefundItemRequest): Disposable {
        return connectionHelper.requestApi(Constants.POST_REQUEST,
            URLS.REFUND, request, StatusMessage::class.java,
            URLS.REFUND, true)
    }
}