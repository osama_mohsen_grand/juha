package grand.app.juha.pages.order.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.databinding.ItemOrderBinding
import grand.app.juha.databinding.ItemOrderDetailsBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.model.UpdateCartRequest
import grand.app.juha.pages.order.model.Order
import grand.app.juha.pages.order.ui.OrderDetailsFragment
import grand.app.juha.pages.order.ui.ProductsRefundFragment
import grand.app.juha.pages.order.viewmodel.ItemOrderViewModel
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager

class  ReasonAdapter(var modelList:ArrayList<Order> = arrayListOf()) : RecyclerView.Adapter<ReasonAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    private lateinit var fragment : BaseFragment
    var isOrderDetails = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemOrderDetailsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_order_details,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    fun setFragment(fragment: BaseFragment){
        this.fragment = fragment
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[selectedItem]

            if(mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                when(model.orderStatus){
                    -1,3 -> {
                        bundle.putString(Constants.PAGE, ProductsRefundFragment::class.java.name)
                        bundle.putInt(Constants.ID, model.id)
                        MovementHelper.startActivityBaseForResult(
                            fragment,
                            bundle,
                            Constants.REFUND_RESULT,
                            ResourceManager.getString(R.string.request) + "\n" + "(" + model.ordersNum + ")")
                    }
                }
            }
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }



    fun update(modelList: ArrayList<Order>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(itemCount, modelList.size - 1);
    }

    class ViewHolder(private val binding: ItemOrderDetailsBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOrderViewModel

        //bint
        fun bind(model: Order) {
            viewModel = ItemOrderViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}