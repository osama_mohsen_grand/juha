package grand.app.juha.pages.order.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Order(
    @SerializedName("additional")
    var additional: String ="",
    @SerializedName("address_id")
    var addressId: Int = 0,
    @SerializedName("color_name")
    var colorName: String = "",
    @SerializedName("discounted_price")
    var discountedPrice: Int = 0,
    @SerializedName("has_discount")
    var hasDiscount: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("img")
    var img: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("orders_num")
    var ordersNum: String = "",
    @SerializedName("payment_method")
    var paymentMethod: Int = 0,
    @SerializedName("payment_status")
    var paymentStatus: Int = 0,
    @SerializedName("price")
    var price: Int = 0,
    @SerializedName("products_price")
    var productsPrice: Int = 0,
    @SerializedName("quantity")
    var quantity: Int = 0,
    @SerializedName("rate")
    var rate: Int = 0,
    @SerializedName("shipping")
    var shipping: Int = 0,
    @SerializedName("size_name")
    var sizeName: String = "",
    @SerializedName("order_status")
    var orderStatus: Int = 0,
    @SerializedName("status")
    var status: Int = 0,
    @SerializedName("total")
    var total: Int = 0,
    @SerializedName("user_id")
    var userId: Int = 0,
    @SerializedName("product_id")
    @Expose
    var productId: Int = 0
) : Serializable