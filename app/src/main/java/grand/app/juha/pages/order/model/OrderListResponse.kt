package grand.app.juha.pages.order.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class OrderListResponse(
    @SerializedName("data")
    var orders: ArrayList<Order> = arrayListOf()
) : StatusMessage()