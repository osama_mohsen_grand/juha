package grand.app.juha.pages.order.model


import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.validation.Validate

class RefundItemRequest() {


    @SerializedName("reason_id")
    @Expose
    var reasonId: Int = -1
    @SerializedName("order_id")
    @Expose
    var orderId: Int = -1
    @SerializedName("product_id")
    @Expose
    var productId: Int = -1

    @Transient
    private val TAG = "RefundItemRequest"


    @SerializedName("other_reason")
    @Expose
    var otherReason: String = ""
        get() = field
        set(value) {
            field = value
            otherReasonError.set(null)
        }

    @SerializedName("refunded_quantity")
    @Expose
    var refundQuantity: String = ""

    var reason = ""
        get() = field
        set(value) {
            field = value
            reasonError.set(null)
//            if (Validate.isValid(field,Constants.PHONE)) phoneError.set("")
//            else phoneError.set(Validate.error)
        }


    @Transient
    var quantityError: ObservableField<String> = ObservableField("")

    @Transient
    var reasonError: ObservableField<String> = ObservableField("")

    @Transient
    var otherReasonError: ObservableField<String> = ObservableField("")

    fun isValid(): Boolean {
        var valid = true
        if (!Validate.isValid(refundQuantity)) {
            Log.d("valid", "phone not valid")
            quantityError.set(Validate.error)
            valid = false

        } else quantityError.set("")
        if (!Validate.isValid(reason)) {
            Log.d("valid", "phone not valid")
            reasonError.set(Validate.error)
            valid = false

        } else reasonError.set("")

        if (!Validate.isValid(otherReason)) {
            Log.d("password", "password not valid")
            otherReasonError.set(Validate.error)
            valid = false
        } else otherReasonError.set("")

        return valid
    }


}