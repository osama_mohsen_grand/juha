package grand.app.juha.pages.order.model.details


import com.google.gson.annotations.SerializedName

data class OrderDetails(
    @SerializedName("additional")
    var additional: Any = Any(),
    @SerializedName("address_id")
    var addressId: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("order_address")
    var orderAddress: OrderAddress = OrderAddress(),
    @SerializedName("orders_num")
    var ordersNum: String = "",
    @SerializedName("payment_method")
    var paymentMethod: Int = 0,
    @SerializedName("payment_status")
    var paymentStatus: Int = 0,
    @SerializedName("order_status")
    var orderStatus: Int = 0,
    @SerializedName("products_price")
    var productsPrice: Int = 0,
    @SerializedName("shipping")
    var shipping: Int = 0,
    @SerializedName("total")
    var total: Int = 0,
    @SerializedName("user_id")
    var userId: Int = 0
)