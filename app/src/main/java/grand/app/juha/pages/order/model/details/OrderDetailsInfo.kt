package grand.app.juha.pages.order.model.details

import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.cart.model.Cart
import grand.app.juha.pages.order.model.Order

class OrderDetailsInfo {
    @SerializedName("order")
    var order: OrderDetails = OrderDetails()
    @SerializedName("products")
    var products: ArrayList<Order> = arrayListOf<Order>()
}