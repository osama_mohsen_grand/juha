package grand.app.juha.pages.order.model.details


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import grand.app.juha.pages.cart.model.Cart
import grand.app.juha.pages.cart.model.CartData

data class OrderDetailsResponse(
    @SerializedName("data")
    var data: OrderDetailsInfo = OrderDetailsInfo()
) : StatusMessage()