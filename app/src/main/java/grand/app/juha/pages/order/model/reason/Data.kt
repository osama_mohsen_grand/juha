package grand.app.juha.pages.order.model.reason


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("text")
    var text: String = ""
)