package grand.app.juha.pages.order.model.reason


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class ReasonResponse(
    @SerializedName("data")
    var data: List<Data> = listOf()
) : StatusMessage()