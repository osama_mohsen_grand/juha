package grand.app.juha.pages.order.ui

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialog
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.*
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.order.model.OrderListResponse
import grand.app.juha.pages.order.model.details.OrderDetailsResponse
import grand.app.juha.pages.order.viewmodel.OrderDetailsViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import javax.inject.Inject


class OrderDetailsFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentOrderDetailsBinding

    lateinit var dialog: AlertDialog

    @Inject
    lateinit var viewModel: OrderDetailsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        binding.viewmodel = viewModel
        viewModel.setFragment(this)
        setEvent()
        setEventDialog()
        // Inflate the layout for this fragment
        return binding.root
    }



    private fun setEventDialog() {
        val dialogCancelOrderBinding: DialogCancelOrderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_cancel_order, null, true)
        val grand = dialogCancelOrderBinding.root
        val builder = context?.let { AlertDialog.Builder(it, R.style.customDialog) }
        dialog = builder?.create()!!
        dialog.setView(grand)
        dialogCancelOrderBinding.rlYes.setOnClickListener {
            viewModel.cancelService()
        }
        dialogCancelOrderBinding.rlNo.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
            when (mutable.message) {
                URLS.ORDER_DETAILS -> {
                    viewModel.response = (mutable.obj as OrderDetailsResponse)
                    viewModel.setData()
                }
                URLS.ORDER_CANCEL -> {
                    val intent = Intent()
                    intent.putExtra(Constants.RELOAD,true)
                    requireActivity().setResult(Activity.RESULT_OK, intent);
                    requireActivity().finish()
                }
                Constants.CANCELED -> {
                    dialog.show()
                }
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }

}