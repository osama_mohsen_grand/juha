package grand.app.juha.pages.order.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialog
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.DialogFilterSortBinding
import grand.app.juha.databinding.FragmentOrderListBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.address.list.model.Address
import grand.app.juha.pages.order.model.OrderListResponse
import grand.app.juha.pages.order.viewmodel.OrderListViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject


class OrderListFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentOrderListBinding

    @Inject
    lateinit var viewModel: OrderListViewModel
    lateinit var dialog: BottomSheetDialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_list, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        viewModel.setFragment(this)
        setEvent()
        setDialogSort()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setDialogSort() {
        val btnsheet = layoutInflater.inflate(R.layout.dialog_filter_sort, null)
        val binding = DialogFilterSortBinding.inflate(layoutInflater, btnsheet as ViewGroup, false)
        binding.viewmodel = viewModel
        dialog = BottomSheetDialog(this.requireContext())
        dialog.setContentView(binding.root)
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
            when (mutable.message) {
                URLS.ORDERS -> {
                    Log.d(TAG,"orders")
                    viewModel.response = (mutable.obj as OrderListResponse)
                    viewModel.setData()
                }
                Constants.SORT_BY -> {
                    dialog.show()
                }
                Constants.DISMISS -> {
                    if(dialog.isShowing) dialog.dismiss()
                }
            }

        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        UserHelper.saveKey(Constants.RELOAD,"")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "requestCode:$requestCode")
        Log.d(TAG, "requestCode:$resultCode")
        if ( requestCode == Constants.CANCELLED_RESULT && resultCode == Activity.RESULT_OK) {
            viewModel.repositoryOrders.setLiveData(viewModel.getLiveData())
            data?.let {
                if(data.hasExtra(Constants.ADDRESS)){
                    viewModel.repositoryOrders.getOrders()
                }
            }
//            viewModel.getAddressList()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repositoryOrders.setLiveData(viewModel.getLiveData())
        if(UserHelper.retrieveKey(Constants.RELOAD).equals("true")){
            viewModel.repositoryOrders.getOrders()
            UserHelper.saveKey(Constants.RELOAD,"")
        }
    }

}