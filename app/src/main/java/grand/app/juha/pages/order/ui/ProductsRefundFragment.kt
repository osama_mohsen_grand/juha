package grand.app.juha.pages.order.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentProductsRefundBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.order.model.Order
import grand.app.juha.pages.order.model.details.OrderDetailsResponse
import grand.app.juha.pages.order.viewmodel.OrderDetailsViewModel
import grand.app.juha.pages.rates.helper.DialogBottomRateProductHelper
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject

import grand.app.juha.pages.rates.helper.DialogCustomHelper

class ProductsRefundFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentProductsRefundBinding


    @Inject
    lateinit var viewModel: OrderDetailsViewModel

    lateinit var dialog:DialogBottomRateProductHelper
    //
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_products_refund, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        binding.viewmodel = viewModel
        viewModel.setFragment(this)
        setEvent()
        dialog = DialogBottomRateProductHelper(inflater,viewModel.repositoryProduct)
        // Inflate the layout for this fragment
        return binding.root
    }

    fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
            when (mutable.message) {
                URLS.ORDER_DETAILS -> {
                    Log.d(TAG, "orders")
                    viewModel.response = (mutable.obj as OrderDetailsResponse)
                    viewModel.setDataProductsRefund()
                }
            }
        })

        viewModel.adapterProductsRefund.mutableLiveData.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "rate")
            val mutable = it as Mutable
            val model = viewModel.adapterProductsRefund.modelList[mutable.position]
            if (it.message == Constants.RATE) {
                dialog.setProduct(model.productId,model.name)
                dialog.show()
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "requestCode:$requestCode")
        Log.d(TAG, "requestCode:$resultCode")
        if (requestCode == Constants.REFUND_RESULT && resultCode == Activity.RESULT_OK) {
            viewModel.repository.setLiveData(viewModel.getLiveData())

            data?.let {
                if (data.hasExtra(Constants.PRODUCT)) {
                    viewModel.adapterProductsRefund.update(data.getSerializableExtra(Constants.PRODUCT) as Order)
                }
            }

            AppHelper.showDialog(
                requireActivity(),
                getString(R.string.your_order_had_been_executed_successfully),
                getString(R.string.we_will_be_contact_you_from_management)
            )
            UserHelper.saveKey(Constants.RELOAD, "true")
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }

}