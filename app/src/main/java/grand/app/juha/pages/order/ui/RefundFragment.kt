package grand.app.juha.pages.order.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.protobuf.LazyStringArrayList
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentRefundBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.order.model.details.OrderDetailsResponse
import grand.app.juha.pages.order.model.reason.ReasonResponse
import grand.app.juha.pages.order.viewmodel.RefundViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.popup.PopUpInterface
import grand.app.juha.utils.popup.PopUpMenuHelper
import javax.inject.Inject


class RefundFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentRefundBinding

    @Inject
    lateinit var viewModel: RefundViewModel
    val popUpMenuHelper = PopUpMenuHelper()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_refund, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
            when (mutable.message) {
                URLS.REASONS -> {
                    viewModel.reasonResponse = mutable.obj as ReasonResponse
                    viewModel.setData()
                    clickEvents()

                }
                URLS.REFUND -> {
                    val intent = Intent()
                    viewModel.product.quantity -= viewModel.request.refundQuantity.toInt()
                    intent.putExtra(Constants.PRODUCT,viewModel.product)
                    requireActivity().setResult(Activity.RESULT_OK, intent);
                    requireActivity().finish()
                }
            }

        })
    }

    private fun clickEvents() {
        val arr = LazyStringArrayList()
        for (item in viewModel.reasonResponse.data) arr.add(item.text)
        binding.edtReason.setOnClickListener {
            popUpMenuHelper.openPopUp(requireActivity(), binding.edtReason, arr,
                object : PopUpInterface {
                    override fun submitPopUp(position: Int) {
                        viewModel.request.reason = viewModel.reasonResponse.data[position].text
                        viewModel.request.reasonId = viewModel.reasonResponse.data[position].id
                        viewModel.notifyChange()
                    }
                })
        }

        binding.edtQuantity.setOnClickListener {
            val quantityAr = LazyStringArrayList()
            for (number in 1..viewModel.product.quantity) quantityAr.add(number.toString())
            popUpMenuHelper.openPopUp(requireActivity(), binding.edtQuantity, quantityAr,
                object : PopUpInterface {
                    override fun submitPopUp(position: Int) {
                        viewModel.request.refundQuantity = (position + 1).toString()
                        viewModel.notifyChange()
                    }
                })
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "requestCode:$requestCode")
        Log.d(TAG, "requestCode:$resultCode")
        if (requestCode == Constants.CANCELLED_RESULT && resultCode == Activity.RESULT_OK) {
//            viewModel.repositoryOrders.setLiveData(viewModel.getLiveData())
            data?.let {
                if (data.hasExtra(Constants.ADDRESS)) {
//                    viewModel.repositoryOrders.getOrders()
                }
            }
//            viewModel.getAddressList()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }

}