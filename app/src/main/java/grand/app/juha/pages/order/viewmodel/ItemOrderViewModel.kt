package grand.app.juha.pages.order.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.order.model.Order
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager

data class ItemOrderViewModel(val model : Order, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()

    private  val TAG = "ItemCartViewModel"

    var price: ObservableField<String> =
        ObservableField("${if(model.discountedPrice == 0) model.price * model.quantity else model.discountedPrice * model.quantity} ${ResourceManager.getString(R.string.currency)}")

    fun rateProduct(){
        if(model.status == 3) mutableLiveData.value = Mutable(Constants.RATE , position)
    }

    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }

    fun refundOrder(){
        mutableLiveData.value = Mutable(Constants.REFUND , position)
    }
}