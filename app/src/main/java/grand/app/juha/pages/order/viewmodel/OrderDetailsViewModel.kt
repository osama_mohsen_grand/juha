package grand.app.juha.pages.order.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.adapter.CartTransactionAdapter
import grand.app.juha.pages.cart.model.CartTransaction
import grand.app.juha.pages.cart.model.CheckoutRequest
import grand.app.juha.pages.order.OrderRepository
import grand.app.juha.pages.order.adapter.OrderDetailsAdapter
import grand.app.juha.pages.order.adapter.ProductsRefundAdapter
import grand.app.juha.pages.order.model.details.OrderDetailsResponse
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class OrderDetailsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: OrderRepository;

    @Inject
    lateinit var repositoryProduct: ProductRepository;
    var compositeDisposable: CompositeDisposable

    var response: OrderDetailsResponse = OrderDetailsResponse()
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var type: Int = 0
    private val TAG = "OrderDetailsViewModel"
    var adapterTransaction = CartTransactionAdapter()
    var adapter: OrderDetailsAdapter
    var adapterProductsRefund: ProductsRefundAdapter
    val request = CheckoutRequest()
    var id = -1
    var address: ObservableField<String> = ObservableField<String>("")
    var phone: ObservableField<String> = ObservableField<String>("")

    @Inject
    constructor(repository: OrderRepository,repositoryProduct: ProductRepository) {
        this.repository = repository
        this.repositoryProduct = repositoryProduct
        repository.setLiveData(getLiveData())
        compositeDisposable = CompositeDisposable()
        adapter = OrderDetailsAdapter()
        adapterTransaction = CartTransactionAdapter()
        adapterProductsRefund = ProductsRefundAdapter()
    }

    fun setArgument(
        bundle: Bundle
    ) {
        if(bundle.containsKey(Constants.ID))
            id = bundle.getInt(Constants.ID)
        callService()
    }

    fun callService() {
        repository.getOrderDetails(id)

    }


    fun cancelOrder() {
        getLiveData().value = Mutable(Constants.CANCELED)
    }

    fun cancelService(){
        repository.cancelOrder(id)
    }


    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        address.set("${ResourceManager.getString(R.string.address)} : ${response.data.order.orderAddress.governorate.name} - ${response.data.order.orderAddress.city.name} - ${response.data.order.orderAddress.area.name} - ${response.data.order.orderAddress.st}")
        phone.set("${ResourceManager.getString(R.string.phone)} : ${response.data.order.orderAddress.phone}")

        val cartTransaction = arrayListOf<CartTransaction>()
        cartTransaction.add(CartTransaction(getString(R.string.cost),response.data.order.productsPrice.toString()+ " "+ResourceManager.getString(R.string.currency)))
        cartTransaction.add(CartTransaction(getString(R.string.shipping),if(response.data.order.shipping == 0) getString(R.string.free) else response.data.order.shipping.toString()+" "+ResourceManager.getString(R.string.currency)))
        cartTransaction.add(CartTransaction(getString(R.string.total_summation),response.data.order.total.toString()+ " "+ResourceManager.getString(R.string.currency)))

        adapterTransaction.update(cartTransaction)
        adapter.update(response.data.products)

        show.set(true)
        notifyChange()
    }


    fun setDataProductsRefund(){
        adapterProductsRefund.update(response.data.products)
        adapterProductsRefund.orderId = id
        show.set(true)
        notifyChange()
    }

    fun setFragment(fragment: BaseFragment) {
        adapter.setFragment(fragment)
        adapterProductsRefund.setFragment(fragment)
    }
}