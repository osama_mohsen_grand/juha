package grand.app.juha.pages.order.viewmodel

import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.order.OrderRepository
import grand.app.juha.pages.order.adapter.OrderListAdapter
import grand.app.juha.pages.order.model.OrderListResponse
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

public class OrderListViewModel : BaseViewModel {


    @Inject
    lateinit var repositoryOrders: OrderRepository;

    var compositeDisposable: CompositeDisposable
    var type: Int = 0
    private val TAG = "OrderListViewModel"
    lateinit var response: OrderListResponse
    var adapter: OrderListAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: OrderRepository) {
        this.repositoryOrders = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
        adapter = OrderListAdapter( )
        repository.getOrders()
    }

    fun setFragment(fragment :BaseFragment){
        adapter.setFragment(fragment)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun sortBy(){
        getLiveData().value = Mutable(Constants.SORT_BY)
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        if (response.orders.isNotEmpty()) adapter.update(response.orders) else noData()
        Log.d(TAG,"orders:"+response.orders.size)
//        notifyChange()
    }

    var isDismiss : ObservableBoolean = ObservableBoolean(false)

    fun delete(){
        isDismiss.set(false)
        getLiveData().value = Mutable(Constants.DISMISS)
        repositoryOrders.getOrders()
    }

    public fun onSplitTypeChanged(radioGroup : RadioGroup, id : Int){
        Log.d(TAG,"id${id}")
        getLiveData().value = Mutable(Constants.DISMISS)
        when(id){
            R.id.rb_three_month -> repositoryOrders.sortOrder(1)
            R.id.rb_six_month -> repositoryOrders.sortOrder(2)
            else -> repositoryOrders.sortOrder(3)
        }
    }

}