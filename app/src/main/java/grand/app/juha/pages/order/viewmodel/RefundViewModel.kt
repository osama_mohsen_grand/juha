package grand.app.juha.pages.order.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.cart.adapter.CartTransactionAdapter
import grand.app.juha.pages.cart.model.CartTransaction
import grand.app.juha.pages.cart.model.CheckoutRequest
import grand.app.juha.pages.order.OrderRepository
import grand.app.juha.pages.order.adapter.OrderDetailsAdapter
import grand.app.juha.pages.order.adapter.ProductsRefundAdapter
import grand.app.juha.pages.order.adapter.ReasonAdapter
import grand.app.juha.pages.order.model.Order
import grand.app.juha.pages.order.model.RefundItemRequest
import grand.app.juha.pages.order.model.details.OrderDetailsResponse
import grand.app.juha.pages.order.model.reason.ReasonResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RefundViewModel : BaseViewModel {
    @Inject
    lateinit var repository: OrderRepository;
    var compositeDisposable: CompositeDisposable


    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    private val TAG = "OrderDetailsViewModel"
    var product: Order = Order()
    val request: RefundItemRequest = RefundItemRequest()
    var reasonResponse: ReasonResponse = ReasonResponse()
    val reasonAddress: ReasonAdapter = ReasonAdapter()

    var price: ObservableField<String> =
        ObservableField("")


    @Inject
    constructor(repository: OrderRepository) {
        this.repository = repository
        repository.setLiveData(getLiveData())
        compositeDisposable = CompositeDisposable()
        repository.getReasons()
    }

    fun setArgument(
        bundle: Bundle
     ) {
        if(bundle.containsKey(Constants.PRODUCT)) {
            this.product = bundle.getSerializable(Constants.PRODUCT) as Order
            price.set("${if(product.discountedPrice == 0) product.price * product.quantity else product.discountedPrice * product.quantity} ${ResourceManager.getString(R.string.currency)}")
            request.refundQuantity = product.quantity.toString()
            request.productId = product.id
        }

        if(bundle.containsKey(Constants.ID))
            request.orderId = bundle.getInt(Constants.ID)
    }

    fun refundItem(){
        if(request.isValid())
            repository.refund(request)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        show.set(true)
        notifyChange()
    }
}