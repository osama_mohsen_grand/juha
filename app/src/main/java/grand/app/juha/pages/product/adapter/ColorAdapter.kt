package grand.app.juha.pages.product.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemColorBinding
import grand.app.juha.pages.product.model.details.Color
import grand.app.juha.pages.product.viewmodel.ItemColorViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper

class ColorAdapter(
    private var modelList: List<Color> = ArrayList()
) : RecyclerView.Adapter<ColorAdapter.ViewHolder>() {
    // fetch list data

    var selected : Int = 0
    var mutableLiveData = MutableLiveData<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemColorBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_color,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    private  val TAG = "FeatureAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])

        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selected = mutable.position
            mutableLiveData.value = Mutable(Constants.POSITION)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun getColorId() : Int{
        return modelList[selected].id;
    }

    fun update(modelList: List<Color>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        selected = when(modelList.size){
            0 -> -1
            else -> 0
        }
        notifyDataSetChanged()
    }

    fun updateColorPosition(colorId: Int) {
        modelList.forEachIndexed { index, color ->
            run {
                if (color.id == colorId) {
                    selected = index
                    notifyDataSetChanged()
                    return
                }
            }
        }
    }

    inner class ViewHolder(private val binding: ItemColorBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemColorViewModel

        //bint
        fun bind(model: Color) {
            viewModel = ItemColorViewModel(model, adapterPosition,adapterPosition == selected)
            binding.viewmodel = viewModel
        }
    }
}