package grand.app.juha.pages.product.adapter

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import grand.app.juha.R
import grand.app.juha.base.MyApplication
import grand.app.juha.customviews.dialog.DialogHelper
import grand.app.juha.customviews.dialog.DialogHelperInterface
import grand.app.juha.databinding.ItemProductBinding
import grand.app.juha.databinding.ItemProductListBinding
import grand.app.juha.databinding.ItemProductListVerticalBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.pages.cart.model.UpdateCartRequest
import grand.app.juha.pages.intro.IntroFragment
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.pages.product.viewmodel.ItemProductViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_product.view.*

class ProductAdapter(
    var modelList: ArrayList<Product> = arrayListOf(),
    val limit_size: Int = 2,
    private var repository: ProductRepository
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    // fetch list data
    var favPosition: Int = 0
    var selectedItem: Int = -1
    var widthItem: Float = 0f
    private lateinit var fragment: Fragment
    var size = 0
    var mutableLiveData = MutableLiveData<Any>()
    var isCenter = true
    var isSpanTwo = true


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(isCenter && isSpanTwo){
            val viewHolder = holder as ViewHolder
            viewHolder.bind(modelList[position])
            viewHolder.viewModel.mutableLiveData.observeForever {
//                setEvent(it as Mutable,holder.itemView.rootView.context,holder.itemView.imageView_favourite,holder.itemView.imageViewCartAdd)
                setEvent(it as Mutable,holder.binding.root.context,holder.binding.imageViewFavourite,holder.binding.imageViewCartAdd)
            }
        }else if(isCenter && !isSpanTwo){
            val viewHolder = holder as ViewHolderListSpanOne

            viewHolder.bind(modelList[position])
            viewHolder.viewModel.mutableLiveData.observeForever {
                setEvent(it as Mutable,holder.binding.root.context,holder.binding.imgItemCartFav,holder.binding.imgItemProductRemove)
            }
        }else{
            val viewHolder = holder as ViewHolderSpanHorizontal
            viewHolder.bind(modelList[position])
            viewHolder.viewModel.mutableLiveData.observeForever {
//                setEvent(it as Mutable,holder.itemView.rootView.context,holder.itemView.imageView_favourite,holder.itemView.imageViewCartAdd)
                setEvent(it as Mutable,holder.binding.root.context,holder.binding.imageViewFavourite,holder.binding.imageViewCartAdd)
            }
        }

    }

    private fun setEvent(it: Mutable,context: Context , favView: View ,cartView : View ){
        val mutable: Mutable = it as Mutable
        selectedItem = it.position
        Log.d(TAG,"message mutableLiveData $selectedItem")
        val model = modelList[mutable.position]
        when (mutable.message) {
            Constants.FAVOURITE -> {
                if (UserHelper.isLogin()) {
                    favView.startAnimation(
                        AnimationUtils.loadAnimation(
                            context,
                            R.anim.clockwise
                        )
                    )
                    repository.addFavourite(modelList[selectedItem].id)
                    modelList[selectedItem].isFavourite = if(modelList[selectedItem].isFavourite == 0) 1 else 0
                    notifyItemChanged(mutable.position)
                }else {
//                    Toasty.normal(
//                        context,
//                        ResourceManager.getString(R.string.please_login_first),
//                        Toast.LENGTH_LONG
//                    ).show()
                    val ids =
                        intArrayOf(R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no)

                    DialogHelper.showDialogHelper(
                        context,
                        R.layout.dialog_register_first,
                        ids,
                        object: DialogHelperInterface {
                            override fun OnClickListenerContinue(dialog: Dialog, view: View) {
                                when (view.id) {
                                    R.id.tv_dialog_close_app_yes -> {
                                        dialog.dismiss()
                                        val bundle = Bundle()
                                            bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                                            MovementHelper.startActivityBase(context, bundle);
                                    }
                                    R.id.tv_dialog_close_app_no -> dialog.dismiss()
                                }
                            }
                        })


                }
            }
            Constants.ADD_TO_CART -> {
                if (UserHelper.isLogin()) {
                    cartView.startAnimation(
                        AnimationUtils.loadAnimation(
                            context,
                            R.anim.shake
                        )
                    )
                    repository.addToCart(UpdateCartRequest(model.id,1))

                }else
                    {val ids =
                        intArrayOf(R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no)
                        DialogHelper.showDialogHelper(
                            context,
                            R.layout.dialog_register_first,
                            ids,
                            object: DialogHelperInterface {
                                override fun OnClickListenerContinue(dialog: Dialog, view: View) {
                                    when (view.id) {
                                        R.id.tv_dialog_close_app_yes -> {
                                            dialog.dismiss()
                                            val bundle = Bundle()
                                            bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                                            MovementHelper.startActivityBase(context, bundle);
                                        }
                                        R.id.tv_dialog_close_app_no -> dialog.dismiss()
                                    }
                                }
                            })}


            }
            Constants.SUBMIT -> {
                mutableLiveData.value = Mutable(Constants.SUBMIT,selectedItem)
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,ProductDetailsFragment::class.java.name)
                bundle.putInt(Constants.ID,model.id)
                bundle.putString(Constants.TITLE,model.name)
                MovementHelper.startActivityBaseForResult(fragment,bundle,Constants.FAVOURITE_RESULT,"")
            }
        }
        notifyItemChanged(selectedItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.d(TAG,"center:"+isCenter)
        Log.d(TAG,"span:"+isSpanTwo)
        // binding item_model layout

        if(isCenter && isSpanTwo){
            val binding: ItemProductBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_product,
                parent,
                false
            )
            return ViewHolder(
                binding
            )
        }else if(isCenter && !isSpanTwo){
            val binding: ItemProductListVerticalBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_product_list_vertical,
                parent,
                false
            )

            return ViewHolderListSpanOne(
                binding
            )
        }else{

            val binding: ItemProductListBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_product_list,
                parent,
                false
            )

            return ViewHolderSpanHorizontal(
                binding
            )

        }
    }

    private  val TAG = "ProductAdapter"

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(modelList[position])
//
//        holder.viewModel.mutableLiveData.observeForever {
//

//        }
//    }

    fun setFragmentPage(fragment: Fragment){
        Log.d(TAG,"set fragment : $fragment")
        this.fragment = fragment
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: ArrayList<Product>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun updateInserted(modelList: ArrayList<Product>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList.addAll(modelList)
        notifyItemRangeInserted(
            size+1,
            modelList.size
        );
        size = modelList.size
    }

    fun updateFavourite(id: Int?, favouriteStatus: Int?) {

        for(i in modelList.indices){
            if(modelList[i].id == id) {
                if (favouriteStatus != null) {
                    modelList[i].isFavourite = favouriteStatus
                    notifyItemChanged(i)
                }
            }
        }
    }

    inner class ViewHolder(val binding: ItemProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemProductViewModel

        //bint
        fun bind(model: Product) {
            viewModel =
                ItemProductViewModel(
                    model,
                    adapterPosition,
                    widthItem
                )
            binding.viewmodel = viewModel
        }
    }

    inner class ViewHolderSpanHorizontal(val binding: ItemProductListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemProductViewModel

        //bint
        fun bind(model: Product) {
            viewModel =
                ItemProductViewModel(
                    model,
                    adapterPosition,
                    widthItem
                )
            binding.viewmodel = viewModel
        }
    }


    inner class ViewHolderListSpanOne(val binding: ItemProductListVerticalBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemProductViewModel

        //bint
        fun bind(model: Product) {
            viewModel =
                ItemProductViewModel(
                    model,
                    adapterPosition,
                    widthItem
                )
            binding.viewmodel = viewModel
        }
    }

}