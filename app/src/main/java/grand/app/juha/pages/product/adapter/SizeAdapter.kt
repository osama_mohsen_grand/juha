package grand.app.juha.pages.product.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemSizeBinding
import grand.app.juha.pages.product.model.details.Size
import grand.app.juha.pages.product.viewmodel.ItemSizeViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper

class SizeAdapter(
    private var modelList: List<Size> = ArrayList()
) : RecyclerView.Adapter<SizeAdapter.ViewHolder>() {
    // fetch list data
    var selected : Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSizeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_size,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    private  val TAG = "FeatureAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selected = mutable.position
            notifyDataSetChanged()
        }
    }

    fun getSizeId() : Int{
        if(selected != -1)
            return modelList[selected].id;
        return selected
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: List<Size>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        selected = when(modelList.size){
            0 -> -1
            else -> 0
        }
        notifyDataSetChanged()
    }

    fun updateSizePosition(sizeId: Int) {
        modelList.forEachIndexed { index, size ->
            run {
                if (size.id == sizeId) {
                    selected = index
                    notifyDataSetChanged()
                    return
                }
            }
        }
    }

    inner class ViewHolder(private val binding: ItemSizeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemSizeViewModel

        //bint
        fun bind(model: Size) {
            viewModel = ItemSizeViewModel(model, adapterPosition,adapterPosition == selected)
            binding.viewmodel = viewModel
        }
    }
}