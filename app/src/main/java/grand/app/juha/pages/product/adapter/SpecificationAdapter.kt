package grand.app.juha.pages.product.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemSpecificationBinding
import grand.app.juha.pages.product.model.details.Speicification
import grand.app.juha.pages.product.viewmodel.ItemSpecificationViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper

class SpecificationAdapter(
    var modelList: List<Speicification> = ArrayList()
) : RecyclerView.Adapter<SpecificationAdapter.ViewHolder>() {
    // fetch list data

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSpecificationBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_specification,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    private  val TAG = "FeatureAdapter"

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: List<Speicification>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemSpecificationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemSpecificationViewModel

        //bint
        fun bind(model: Speicification) {
            viewModel = ItemSpecificationViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}