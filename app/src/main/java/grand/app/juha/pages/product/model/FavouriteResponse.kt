package grand.app.juha.pages.product.model

import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

class FavouriteResponse(@SerializedName("isFavorite") var isFavorite:Int): StatusMessage() {
}