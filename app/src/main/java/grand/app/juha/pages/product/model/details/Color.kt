package grand.app.juha.pages.product.model.details


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Color(
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("color")
    var color: String = "",
    @SerializedName("count")
    @Expose
    var count: Int = 0

)