package grand.app.juha.pages.product.model.details


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.model.IdNameImage
import grand.app.juha.pages.home.model.Slider
import grand.app.juha.pages.product.model.list.Product
import java.util.ArrayList

data class Data(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("price")
    var price: String = "",
    @SerializedName("desc")
    var desc: String = "",
    @SerializedName("quantity")
    var quantity: Int = 0,
    @SerializedName("product_quantity")
    var productQuantity: Int = 0,
    @SerializedName("featured")
    var featured: Int = 0,
    @SerializedName("has_discount")
    var hasDiscount: Int = 0,
    @SerializedName("discounted_price")
    var discountedPrice: String = "",
    @SerializedName("priority")
    var priority: String = "",
    @SerializedName("shop_id")
    var shopId: Int = 0,
    @SerializedName("category_id")
    var categoryId: Int = 0,
    @SerializedName("sub_category_id")
    var subCategoryId: Int = 0,
    @SerializedName("rate")
    var rate: String = "",
    @SerializedName("is_favourite")
    var isFavourite: Int = -1,
    @SerializedName("sizes")
    var sizes: List<Size> = listOf(),
    @SerializedName("related_products")
    var relatedProducts: ArrayList<Product> = arrayListOf(),
    @SerializedName("default_img")
    var defaultImg: Slider = Slider(),
    @SerializedName("features")
    var speicifications: List<Speicification> = listOf(),
    @SerializedName("colors")
    var colors: List<Color> = listOf(),
    @SerializedName("images")
    var images: ArrayList<Slider> = arrayListOf<Slider>(),
    @SerializedName("shop")
    var shop: Shop = Shop()
)