package grand.app.juha.pages.product.model.details


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class ProductDetailsResponse(
    @SerializedName("data")
    var data: Data = Data()
): StatusMessage()