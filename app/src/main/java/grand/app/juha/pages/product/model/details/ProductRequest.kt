package grand.app.juha.pages.product.model.details


import androidx.databinding.ObservableBoolean
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.model.IdNameImage
import java.io.Serializable

class ProductRequest{
    @SerializedName("product_id")
    @Expose
    var productId = -1

    @SerializedName("color_id")
    lateinit var colorId:String

    constructor(productId: Int) {
        this.productId = productId
    }

    constructor(productId: Int,colorId: Int) {
        this.productId = productId
        this.colorId = colorId.toString()
    }
}