package grand.app.juha.pages.product.model.details


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Size(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = ""
): Serializable