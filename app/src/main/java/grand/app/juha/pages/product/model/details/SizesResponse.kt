package grand.app.juha.pages.product.model.details


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class SizesResponse(
    @SerializedName("data")
    var data: List<Size> = arrayListOf()
): StatusMessage()