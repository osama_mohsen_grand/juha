package grand.app.juha.pages.product.model.details


import com.google.gson.annotations.SerializedName

data class Speicification(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("text")
    var text: String = ""
)