package grand.app.juha.pages.product.model.list


import androidx.databinding.ObservableBoolean
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.model.IdNameImage
import java.io.Serializable

data class Product(
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("name")
    @Expose
    var name: String = "",
    @SerializedName("price")
    @Expose
    var price: String = "",
    @SerializedName("rate")
    @Expose
    var rate: Float = 0f,
    @SerializedName("discounted_price")
    @Expose
    var discountedPrice: String = "",
    @SerializedName("category_id")
    @Expose
    var categoryId: String = "",
    @SerializedName("about")
    @Expose
    var about: String = "",
    @SerializedName("default_img")
    @Expose
    var defaultImg: IdNameImage = IdNameImage(-1, ""),
    @SerializedName("type")
    @Expose
    var type: Int = 0,
    @SerializedName("quantity")
    @Expose
    var quantity: Int = 0,

    @SerializedName("is_favourite")
    @Expose
    var isFavourite: Int = 0
) : Serializable