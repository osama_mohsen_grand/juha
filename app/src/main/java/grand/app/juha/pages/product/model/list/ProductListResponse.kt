package grand.app.juha.pages.product.model.list


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage
import java.io.Serializable

data class ProductListResponse(
    @SerializedName("data")
    @Expose
    var data: ArrayList<Product> = ArrayList()
) : StatusMessage() , Serializable