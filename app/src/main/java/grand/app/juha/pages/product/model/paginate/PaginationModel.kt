package grand.app.juha.pages.product.model.paginate

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class PaginationModel {
    @SerializedName("current_page")
    @Expose
    var currentPage: Int = 0

    @SerializedName("first_page_url")
    @Expose
    var firstPageUrl: String = ""

    @SerializedName("from")
    @Expose
    var from: Int = 0

    @SerializedName("next_page_url")
    @Expose
    var nextPageUrl: Any? = null

    @SerializedName("path")
    @Expose
    var path: String = ""

    @SerializedName("per_page")
    @Expose
    var perPage: Int = 0

    @SerializedName("prev_page_url")
    @Expose
    var prevPageUrl: Any = ""

    @SerializedName("to")
    @Expose
    var to: Int = 0
}
