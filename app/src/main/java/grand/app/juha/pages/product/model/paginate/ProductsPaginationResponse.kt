package grand.app.juha.pages.product.model.paginate

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

class ProductsPaginationResponse ( @SerializedName("current_page")
                                   @Expose
                                   var data: Int = 0): StatusMessage() {
}