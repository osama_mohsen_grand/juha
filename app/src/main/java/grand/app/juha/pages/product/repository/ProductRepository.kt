package grand.app.juha.pages.product.repository

import androidx.lifecycle.MutableLiveData
import grand.app.juha.base.StatusMessage
import grand.app.juha.model.base.Mutable
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.pages.cart.CartRepository
import grand.app.juha.pages.favourite.ProductListPaginateResponse
import grand.app.juha.pages.filter.model.FilterRequest
import grand.app.juha.pages.filter.model.FilterResponse
import grand.app.juha.pages.filter.model.GetFilterRequest
import grand.app.juha.pages.offer.model.OfferDetailsResponse
import grand.app.juha.pages.offer.model.OffersResponse
import grand.app.juha.pages.product.model.FavouriteResponse
import grand.app.juha.pages.product.model.details.ProductDetailsResponse
import grand.app.juha.pages.product.model.details.ProductRequest
import grand.app.juha.pages.product.model.details.SizesResponse
import grand.app.juha.pages.rates.model.RateProductRequest
import grand.app.juha.pages.rates.model.RatesResponse
import grand.app.juha.pages.search.SearchRequest
import grand.app.juha.pages.wallet.model.WalletResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepository : CartRepository {

    @Inject
    constructor(connectionHelper: ConnectionHelper) : super(connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }

    fun getProductDetails(request: ProductRequest): Disposable {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            URLS.PRODUCT_DETAILS,
            request,
            ProductDetailsResponse::class.java,
            URLS.PRODUCT_DETAILS,
            true
        )
    }

    fun getRates(id: Int): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST,
            URLS.RATES + "/$id",
            Any(),
            RatesResponse::class.java,
            URLS.RATES,
            true
        )
    }

    fun getOffers(): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.OFFERS, Any(), OffersResponse::class.java,
            URLS.OFFERS, true
        )
    }

    fun getOfferDetails(offerId: Int, page: Int): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST,
            "${URLS.INNER_OFFER}$offerId?page=$page",
            Any(),
            OfferDetailsResponse::class.java,
            URLS.INNER_OFFER,
            page == 1
        )
    }

    fun addFavourite(
        product_id: Int
    ): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST,
            URLS.ADD_FAVOURITE + product_id,
            null,
            FavouriteResponse::class.java,
            URLS.ADD_FAVOURITE,
            false
        )
    }

    fun getWallet(): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST,
            URLS.WALLET, Any(),
            WalletResponse::class.java,
            URLS.WALLET,
            true
        )
    }

    fun rateProduct(request: RateProductRequest): Disposable {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            URLS.RATE_PRODUCT, request, StatusMessage::class.java,
            URLS.RATE_PRODUCT, false
        )
    }

    fun getProductList(url: String, page: Int): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST,
            "$url?page=$page", Any(), ProductListPaginateResponse::class.java,
            URLS.PRODUCT_LIST, true
        )
    }

    fun submitFilter(filterRequest: FilterRequest): Disposable {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            URLS.FILTER_SUBMIT, filterRequest, ProductListPaginateResponse::class.java,
            URLS.PRODUCT_LIST, true
        )
    }

    fun getSizes(colorId: Int, productId: Int): Disposable {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST,
            "${URLS.GET_SIZES}/${colorId}/${productId}", Any(), SizesResponse::class.java,
            URLS.GET_SIZES, true
        )
    }

    fun search(request: SearchRequest): Disposable {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            URLS.SEARCH, request, SizesResponse::class.java,
            URLS.SEARCH, true
        )
    }

    fun getFilter(request: GetFilterRequest): Disposable {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            "${URLS.FILTER}", request, FilterResponse::class.java,
            URLS.FILTER, true
        )
    }


//    fun search(text: String): Disposable {
//        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SEARCH+text, Any(), ProductListResponse::class.java,
//            URLS.SEARCH, true)
//    }
}