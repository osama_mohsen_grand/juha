package grand.app.juha.pages.product.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.StatusMessage
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.customviews.dialog.DialogHelper
import grand.app.juha.customviews.dialog.DialogHelperInterface
import grand.app.juha.databinding.DialogCancelOrderBinding
import grand.app.juha.databinding.FragmentProductDetailsBinding
import grand.app.juha.databinding.LayoutContinueShoppingBinding
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.pages.cart.ui.CartFragment
import grand.app.juha.pages.product.model.details.ProductDetailsResponse
import grand.app.juha.pages.product.model.details.SizesResponse
import grand.app.juha.pages.product.viewmodel.ProductDetailsViewModel
import grand.app.juha.pages.rates.RatesFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import javax.inject.Inject

class ProductDetailsFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentProductDetailsBinding
    @Inject
    lateinit var viewModel: ProductDetailsViewModel

    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var loading = true
    lateinit var dialog: AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        binding.viewmodel = viewModel
        viewModel.setFragment(this)
        setEvent()
        setEventDialog()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEventDialog() {
        val bindingShopping: LayoutContinueShoppingBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_continue_shopping, null, true)
        val grand = bindingShopping.root
        val builder = context?.let { AlertDialog.Builder(it, R.style.customDialog) }
        dialog = builder?.create()!!
        dialog.setView(grand)
        bindingShopping.tvCompleteOrder.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(requireContext(),MainActivity::class.java)
            intent.putExtra(Constants.PAGE,CartFragment::class.java.name)
            startActivity(intent)
        }
        bindingShopping.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")

            if (mutable.message == URLS.PRODUCT_DETAILS) {
                viewModel.setData(mutable.obj as ProductDetailsResponse)
            }else if (mutable.message == URLS.ADD_FAVOURITE) {
                Log.d(TAG,"success Favourite")
                toastMessage((mutable.obj as StatusMessage).mMessage)
            }else if (mutable.message == Constants.BACK) {
                val intent = Intent()
                intent.putExtra(Constants.ID,viewModel.id)
                intent.putExtra(Constants.FAVOURITE,viewModel.response.data.isFavourite)
                requireActivity().setResult(Activity.RESULT_OK, intent);
                requireActivity().finish()
            }else if (mutable.message == Constants.SHARE) {
                AppHelper.shareCustom(requireActivity(),viewModel.response.data.name,viewModel.response.data.desc)
                binding.imgProductDeailsShare.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.clockwise
                    )
                )
            }else if (mutable.message == Constants.FAVOURITE) {
                binding.imgProductDetailsFav.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.clockwise
                    )
                )
            }else if (mutable.message == URLS.SHOP_PRODUCTS) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,ProductListFragment::class.java.name)
                bundle.putInt(Constants.CATEGORY_ID,0)
                bundle.putString(Constants.URL, "${URLS.SHOP_PRODUCTS}/${viewModel.response.data.shopId}")
                bundle.putBoolean(Constants.FILTER, true)
                MovementHelper.startActivityBase(
                    context,
                    bundle,
                    ""
                )

            }else if (mutable.message == URLS.ADD_TO_CART) {
                toastMessage((mutable.obj as StatusMessage).mMessage)
                dialog.show()
            }else if (mutable.message == URLS.GET_SIZES) {
                viewModel.sizeAdapter.update((mutable.obj as SizesResponse).data)
            }else if (mutable.message == Constants.RATE) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, RatesFragment::class.java.name)
                bundle.putInt(Constants.ID,viewModel.response.data.id)
                MovementHelper.startActivityBase(context,bundle,"")
            }else if(mutable.message == Constants.LOGIN_FIRST){
                val ids =
                    intArrayOf(R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no)
                DialogHelper.showDialogHelper(
                    context,
                    R.layout.dialog_register_first,
                    ids,
                    object: DialogHelperInterface {
                        override fun OnClickListenerContinue(dialog: Dialog, view: View) {
                            when (view.id) {
                                R.id.tv_dialog_close_app_yes -> {
                                    dialog.dismiss()
                                    val bundle = Bundle()
                                    bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                                    MovementHelper.startActivityBase(context, bundle);
                                }
                                R.id.tv_dialog_close_app_no -> dialog.dismiss()
                            }
                        }
                    })
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}