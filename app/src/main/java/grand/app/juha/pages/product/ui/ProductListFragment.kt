package grand.app.juha.pages.product.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.DialogFilterProductListBinding
import grand.app.juha.databinding.DialogFilterSortBinding
import grand.app.juha.databinding.FragmentProductListBinding
import grand.app.juha.pages.favourite.ProductListPaginateResponse
import grand.app.juha.pages.filter.FilterFragment
import grand.app.juha.pages.filter.model.FilterRequest
import grand.app.juha.pages.product.viewmodel.ProductListViewModel
import grand.app.juha.pages.search.SearchFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.popup.PopUpMenuHelper
import grand.app.juha.utils.resources.ResourceManager
import javax.inject.Inject

class ProductListFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentProductListBinding

    @Inject
    lateinit var viewModel: ProductListViewModel

    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0

    var loading = true
    lateinit var dialog: BottomSheetDialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        arguments?.let { viewModel.setArgument(it) }
        viewModel.setFragment(this)
        binding.viewmodel = viewModel

        init()
        setEvent()
        setDialogSort()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setDialogSort() {
        val btnsheet = layoutInflater.inflate(R.layout.dialog_filter_product_list, null)
        val binding =
            DialogFilterProductListBinding.inflate(layoutInflater, btnsheet as ViewGroup, false)
        binding.viewmodel = viewModel
        dialog = BottomSheetDialog(this.requireContext())
        dialog.setContentView(binding.root)
    }

    lateinit var layoutManagerItems: GridLayoutManager

    private fun init() {
        layoutManagerItems = GridLayoutManager(context, viewModel.span)

        binding.rvProducts.apply {
            adapter = viewModel.adapter
            layoutManager = layoutManagerItems
        }

        binding.rvProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = layoutManagerItems.childCount
                    totalItemCount = layoutManagerItems.itemCount
                    pastVisiblesItems = layoutManagerItems.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            if (!viewModel.progress.get() && viewModel.more) {
                                loading = false
                                viewModel.callService()
                                loading = true
                            }
                        }
                    }
                } else {
                    visibleItemCount = layoutManagerItems.childCount
                    totalItemCount = layoutManagerItems.itemCount
                }
            }
        })
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            if (viewModel.showParentProgress) handleActions(mutable)
            when (mutable.message) {
                Constants.SHOW_PROGRESS -> viewModel.showProgress()
                Constants.HIDE_PROGRESS -> viewModel.hideProgress()
                Constants.SERVER_ERROR -> {
                    viewModel.hideProgress()
                    showError(ResourceManager.getString(R.string.please_check_your_connection))
                };
                URLS.PRODUCT_LIST -> {
                    viewModel.showParentProgress = true
                    viewModel.hideProgress()
                    viewModel.paginateResponse = (mutable.obj as ProductListPaginateResponse)
                    viewModel.setData()

//                    (binding.rvProducts.layoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
//                        override fun getSpanSize(position: Int): Int {
//                            return 1
//                        }
//                    }
//                    viewModel.adapter.notifyDataSetChanged()

                }
                Constants.SEARCH -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, SearchFragment::class.java.name)
                    MovementHelper.startActivityBase(requireContext(), bundle);
                }
                Constants.FILTER -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, FilterFragment::class.java.name)
                    bundle.putSerializable(Constants.FILTER, viewModel.filterRequest)
                    if (viewModel.subCategoryId != 0)
                        bundle.putInt(Constants.SUB_CATEGORY_ID, viewModel.subCategoryId)
                    else
                        bundle.putInt(Constants.CATEGORY_ID, viewModel.categoryId)
                    MovementHelper.startActivityBaseForResult(
                        this,
                        bundle,
                        Constants.FILTER_RESULT
                    );
                }
                Constants.DELETE_SORT -> {
                    if (dialog.isShowing) dialog.dismiss()
                    setDialogSort()
                }
                Constants.SORT_BY -> {
                    dialog.show()
                }
                Constants.BACK -> {
                    requireActivity().finish()
                }
                Constants.DISMISS -> {
                    dialog.dismiss()
                }
                Constants.GRID -> {
                    layoutManagerItems.spanCount = viewModel.span
                    viewModel.adapter.notifyDataSetChanged()
                    binding.rvProducts.adapter = viewModel.adapter
                    viewModel.notifyChange()
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "requestCode:$requestCode")
        Log.d(TAG, "requestCode:$resultCode")
        if (requestCode == Constants.FILTER_RESULT && resultCode == Activity.RESULT_OK) {
            viewModel.filterRequest =
                data?.getSerializableExtra(Constants.FILTER_MODEL) as FilterRequest
            viewModel.currentPosition = 0
            viewModel.more = true
            viewModel.repository.setLiveData(viewModel.liveDataViewModel)
            viewModel.callService()
//            viewModel.getAddressList()
        } else if (requestCode == Constants.FAVOURITE_RESULT && resultCode == Activity.RESULT_OK) {
            val id = data?.getIntExtra(Constants.ID, -1)
            val favouriteStatus = data?.getIntExtra(Constants.FAVOURITE, -1)
            if (id != -1 && favouriteStatus != -1)
                viewModel.adapter.updateFavourite(id, favouriteStatus)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}