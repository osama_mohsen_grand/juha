package grand.app.juha.pages.product.viewmodel

import android.util.Log
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.product.model.details.Color
import grand.app.juha.pages.product.model.details.Speicification
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.utils.Constants

data class ItemColorViewModel(val model: Color, var position: Int,var selected : Boolean) {

    var mutableLiveData = MutableLiveData<Any>()
    private val TAG = "ItemColorViewModel"
    fun submit(){
        Log.d(TAG,"submit color")
        mutableLiveData.value = Mutable(Constants.SUBMIT,position)
    }
}