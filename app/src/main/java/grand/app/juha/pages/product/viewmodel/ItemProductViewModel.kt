package grand.app.juha.pages.product.viewmodel

import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.utils.Constants

data class ItemProductViewModel(val model: Product, var position: Int, var width: Float = 0f) {

    var mutableLiveData = MutableLiveData<Any>()

    fun submit() {
        mutableLiveData.value = Mutable(Constants.SUBMIT, position)
    }

    fun addToCart() {
        mutableLiveData.value = Mutable(Constants.ADD_TO_CART, position)
    }

    fun fav() {
        mutableLiveData.value = Mutable(Constants.FAVOURITE, position)
    }
}