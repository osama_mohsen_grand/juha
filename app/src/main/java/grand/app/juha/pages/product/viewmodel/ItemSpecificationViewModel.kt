package grand.app.juha.pages.product.viewmodel

import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.product.model.details.Speicification
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.utils.Constants

data class ItemSpecificationViewModel(val model: Speicification, var position: Int) {

    var mutableLiveData = MutableLiveData<Any>()
}