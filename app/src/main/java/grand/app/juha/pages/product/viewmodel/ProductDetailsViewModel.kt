package grand.app.juha.pages.product.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.cart.model.UpdateCartRequest
import grand.app.juha.pages.product.adapter.ColorAdapter
import grand.app.juha.pages.product.adapter.ProductAdapter
import grand.app.juha.pages.product.adapter.SizeAdapter
import grand.app.juha.pages.product.adapter.SpecificationAdapter
import grand.app.juha.pages.product.model.details.ProductDetailsResponse
import grand.app.juha.pages.product.model.details.ProductRequest
import grand.app.juha.pages.product.repository.ProductRepository
//import grand.app.juha.pages.product.model.details.ProductDetailsResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.resources.ResourceManager
import grand.app.juha.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProductDetailsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;

    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var type: Int = 0
    var id: Int = 0
    var colorId = 0
    var sizeId = 0
    private val TAG = "ProductListSliderViewModel"

    //    var response: ObservableField<ProductDetailsResponse> =
//        ObservableField(ProductDetailsResponse())
    var title: ObservableField<String> =
        ObservableField("")
    var qty: ObservableField<String> =
        ObservableField("1")
    var response: ProductDetailsResponse = ProductDetailsResponse()

    val specificationAdapter = SpecificationAdapter()
    val colorAdapter = ColorAdapter()
    val sizeAdapter = SizeAdapter()
    var productAdapter: ProductAdapter
    lateinit var fragmentPage: Fragment

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        productAdapter = ProductAdapter(arrayListOf(), limit_size = 2, repository = repository)
        setEventAdapter()
        repository.setLiveData(getLiveData())
    }

    private fun setEventAdapter() {
        colorAdapter.mutableLiveData.observeForever {
            val colorId = colorAdapter.getColorId()
            if (colorId != -1) {
                repository.getSizes(colorId, id)
            }
        }
    }

    fun setArgument(bundle: Bundle) {
        id = bundle.getInt(Constants.ID)
        title.set(bundle.getString(Constants.TITLE))
        if (bundle.containsKey(Constants.COLOR_ID)) {
            colorId = bundle.getInt(Constants.COLOR_ID)
        }
        if (bundle.containsKey(Constants.SIZE_ID)) {
            sizeId = bundle.getInt(Constants.SIZE_ID)
        }
        repository.getProductDetails(
            if (colorId != 0)
                ProductRequest(id, bundle.getInt(Constants.COLOR_ID)) else ProductRequest(id)
        )

    }

    fun setFragment(fragmentPage: Fragment) {
        this.fragmentPage = fragmentPage
        productAdapter.setFragmentPage(fragmentPage)
    }

    fun back() {
        getLiveData().value = Mutable(Constants.BACK)
    }

    fun favourite() {
        if (UserHelper.isLogin()) {
            repository.addFavourite(id)
            response.data.isFavourite = when (response.data.isFavourite) {
                0 -> 1
                else -> 0
            }
            Mutable(Constants.FAVOURITE)
            notifyChange()
        } else {
            getLiveData().value =
                Mutable(Constants.LOGIN_FIRST, ResourceManager.getString(R.string.please_login_first))
        }
    }
//    fun addToCart() {
//        repository.updateCart(
//            UpdateCartRequest(
//                productId = id,
//                qty = qty.get()!!.toInt(),
//                type = type
//            )
//        )
//    }

    fun shopProducts() {
        getLiveData().value = Mutable(URLS.SHOP_PRODUCTS)
    }

    fun rates() {
        getLiveData().value = Mutable(Constants.RATE)
    }

    fun share() {
        getLiveData().value = Mutable(Constants.SHARE)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: ProductDetailsResponse) {
        response.data.images.add(response.data.defaultImg)
        this.response = response
        specificationAdapter.update(response.data.speicifications)
        colorAdapter.update(response.data.colors)
        colorAdapter.updateColorPosition(colorId)
        sizeAdapter.update(response.data.sizes)
        sizeAdapter.updateSizePosition(sizeId)
        productAdapter.update(response.data.relatedProducts)
        show.set(true)
        notifyChange()
    }

    fun addedToCart() {
        if(response.data.quantity > 0) {
            if (UserHelper.isLogin()) {
                repository.addToCart(
                    UpdateCartRequest(
                        response.data.id,
                        1,
                        sizeAdapter.getSizeId().toString(),
                        colorAdapter.getColorId().toString()
                    )
                )
            } else {
                getLiveData().value =
                    Mutable(Constants.LOGIN_FIRST, getString(R.string.please_login_first))
            }
        }
    }
}