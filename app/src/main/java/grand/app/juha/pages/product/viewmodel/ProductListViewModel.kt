package grand.app.juha.pages.product.viewmodel

import android.os.Bundle
import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.favourite.ProductListPaginateResponse
import grand.app.juha.pages.filter.model.FilterRequest
import grand.app.juha.pages.product.adapter.ProductAdapter
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.Constants.URL
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProductListViewModel : BaseViewModel {
    var categoryId: Int = 0
    var subCategoryId: Int = 0

    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable
    var liveDataViewModel: MutableLiveData<Mutable>
    var url: String = ""
    val TAG = "ProductListViewModel"
    lateinit var paginateResponse: ProductListPaginateResponse
    var more: Boolean = true
    var currentPosition = 0
    var size = 0
    var adapter: ProductAdapter
    var title: ObservableField<String> =
        ObservableField("")
    var qty: ObservableField<String> =
        ObservableField("")
    var defaultGrid: ObservableBoolean =
        ObservableBoolean(true)
    var filter: ObservableBoolean =
        ObservableBoolean(false)
    lateinit var fragmentPage: Fragment
    var filterRequest = FilterRequest()
    var span = 2
    var showParentProgress = false
    var haveData: ObservableBoolean = ObservableBoolean(true)


    val progress : ObservableBoolean = ObservableBoolean(false)
    val result: ObservableField<String> = ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        adapter = ProductAdapter(repository = repository)
        adapter.isCenter = true
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        if(bundle.containsKey(Constants.TITLE)){
            title.set(bundle.getString(Constants.TITLE).toString())
        }
        if(bundle.containsKey(Constants.FILTER)){
            filter.set(bundle.getBoolean(Constants.FILTER))
        }
        if(bundle.containsKey(Constants.CATEGORY_ID)){
            categoryId  =bundle.getInt(Constants.CATEGORY_ID)
            if(categoryId != 0) filterRequest.categoryIds.add(categoryId)
        }
        if(bundle.containsKey(Constants.SUB_CATEGORY_ID)){
            subCategoryId  =bundle.getInt(Constants.SUB_CATEGORY_ID)
        }
        if(bundle.containsKey(Constants.FILTER_MODEL)){
            filterRequest  = bundle.getSerializable(Constants.FILTER_MODEL) as FilterRequest
        }
        if(bundle.containsKey(URL)){
            url = bundle.getString(URL).toString()
            Log.d(TAG,"url: $url")
            callService()
        }
    }

    fun callService(){
        showParentProgress = false
        if(more) {
            if(filterRequest.isClear()) {
                currentPosition++
                checkSubCategory()
                repository.getProductList(url, currentPosition)
            }else{
                currentPosition++
                checkSubCategory()
                repository.submitFilter(filterRequest)
            }
        }
    }

    fun checkSubCategory(){
        if(subCategoryId != 0){
            if(filterRequest.subCategoriesId == null) {
                filterRequest.subCategoriesId = arrayListOf()
                filterRequest.subCategoriesId!!.add(subCategoryId)
            }else{
                if(!filterRequest.subCategoriesId!!.contains(subCategoryId)){
                    filterRequest.subCategoriesId!!.add(subCategoryId)
                }
            }
        }
    }

    fun changeGrid(){
        defaultGrid.set(!defaultGrid.get())
        if(span == 1){
            span = 2
            adapter.isSpanTwo = true
        }else {
            span = 1
            adapter.isSpanTwo = false
        }
        liveDataViewModel.value = Mutable(Constants.GRID)
        notifyChange()
    }


    fun setFragment(fragmentPage: Fragment){
        this.fragmentPage = fragmentPage
        adapter.setFragmentPage(fragmentPage)
    }

    fun showProgress(){
        progress.set(true)
    }

    fun hideProgress(){
        progress.set(false)
    }

    fun sortBy(){
        liveDataViewModel.value = Mutable(Constants.SORT_BY)
    }

    fun filter(){
        liveDataViewModel.value = Mutable(Constants.FILTER)
    }

    fun search(){
        liveDataViewModel.value = Mutable(Constants.SEARCH)
    }

    fun back(){
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun delete(){
        filterRequest.sortBy = null
        liveDataViewModel.value = Mutable(Constants.DELETE_SORT)
        resetService()
    }

    fun onSplitTypeChanged(radioGroup : RadioGroup, id : Int){
        liveDataViewModel.value = Mutable(Constants.DISMISS)
        when(id){
            R.id.rb_high_price -> filterRequest.sortBy = 3
            R.id.rb_low_price -> filterRequest.sortBy = 2
            else -> filterRequest.sortBy = 1
        }
//        Log.d(TAG,filterRequest.sortBy.toString())
        resetService()
    }

    private fun resetService() {
        more = true
        currentPosition = 0
        callService()
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        more = (paginateResponse.data.nextPageUrl != null)
        when(currentPosition){
            1 -> {
                haveData.set(paginateResponse.data.products.isNotEmpty())
                adapter.update(paginateResponse.data.products)
            }
            else -> adapter.updateInserted(paginateResponse.data.products)
        }
        result.set("${adapter.modelList.size} ${getString(R.string.commodity_name)}")
        currentPosition++
    }

}