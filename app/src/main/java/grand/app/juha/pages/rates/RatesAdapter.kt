package grand.app.juha.pages.rates

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemRateBinding
import grand.app.juha.pages.rates.model.Rate
import grand.app.juha.pages.rates.viewmodel.ItemRateViewModel

class RatesAdapter(private var modelList:List<Rate> = ArrayList()) : RecyclerView.Adapter<RatesAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemRateBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_rate,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: ArrayList<Rate>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemRateBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemRateViewModel

        //bint
        fun bind(model: Rate) {
            viewModel = ItemRateViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}