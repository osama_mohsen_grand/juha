package grand.app.juha.pages.rates.helper

import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import grand.app.juha.R
import grand.app.juha.base.MyApplication
import grand.app.juha.databinding.LayoutAddRateProductBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.rates.viewmodel.AddRateProductViewModel
import grand.app.juha.utils.Constants

class DialogBottomRateProductHelper(
    val layoutInflater: LayoutInflater,
    val repository: ProductRepository
) {
    var dialog: BottomSheetDialog = BottomSheetDialog(layoutInflater.context)
    val binding: LayoutAddRateProductBinding =
        DataBindingUtil.inflate(LayoutInflater.from(layoutInflater.context), R.layout.layout_add_rate_product, null, false)
//    val addRateProductViewModel = AddRateProductViewModel(repository)


    val viewModel = AddRateProductViewModel(repository)

    init {

        binding.addRateClose.setOnClickListener {
            if (dialog.isShowing) dialog.dismiss()
        }

//        binding.edtAddRateProduct.imeOptions = EditorInfo.IME_ACTION_DONE;
        binding.edtAddRateProduct.imeOptions = EditorInfo.IME_ACTION_DONE;
        binding.edtAddRateProduct.setRawInputType(InputType.TYPE_CLASS_TEXT);

        binding.viewmodel = viewModel
        binding.materialRating.setOnRatingChangeListener { ratingBar, rating ->
            viewModel.request.rate = rating
        }
        dialog.setContentView(binding.root)
        setEvent()
    }

    val rates = arrayListOf<Product>()

    private fun setEvent() {
        viewModel.liveDataViewModel.observeForever {
            when((it as Mutable).message){
                Constants.RATE_PRODUCT -> {
                    if (rates.isEmpty()) dialog.dismiss()
                    else {
                        val product = rates[0]
                        rates.removeAt(0)
                        setProduct(product.id,product.name)
                        dialog.show()
                    }
                }
                Constants.DISMISS -> {
                    dialog.dismiss()
                }
            }
        }
    }

    private  val TAG = "DialogBottomRateProduct"

    fun setProduct(productId: Int, name: String) {
        Log.d(TAG,"setProdcut:"+productId + ",name:$name")
        viewModel.request.productId = productId
        viewModel.request.comment = ""
        viewModel.request.rate = 0f
        binding.materialRating.rating = 0f
        viewModel.request.name.set(name)
        viewModel.notifyChange()

    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        if (dialog.isShowing)
            dialog.dismiss()
    }
}