package grand.app.juha.pages.rates.helper

import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import grand.app.juha.R
import grand.app.juha.base.MyApplication
import grand.app.juha.databinding.LayoutAddRateProductBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.rates.viewmodel.AddRateProductViewModel
import grand.app.juha.utils.Constants

class DialogCustomHelper(
    val layoutInflater: LayoutInflater,
    val repository: ProductRepository
) {
    private lateinit var dialog: AlertDialog
    var addRateBinding: LayoutAddRateProductBinding =
        DataBindingUtil.inflate(layoutInflater, R.layout.layout_add_rate_product, null, true)

    val viewModel = AddRateProductViewModel(repository)

    init {
        addRateBinding.addRateClose.setOnClickListener {
            if (dialog.isShowing) dialog.dismiss()
        }
        addRateBinding.viewmodel = viewModel
        addRateBinding.materialRating.setOnRatingChangeListener { ratingBar, rating ->
            viewModel.request.rate = rating
        }
        val grand = addRateBinding.root
        val builder = layoutInflater.context.let { AlertDialog.Builder(it, R.style.customDialog) }
        dialog = builder.create()
        dialog.window?.let {
//            it.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            it.setWindowAnimations(
                R.style.dialog_animation_fade
            )
        }
        dialog.setView(grand)
        setEvent()
    }

    val rates = arrayListOf<Product>()

    private fun setEvent() {
        viewModel.liveDataViewModel.observeForever {
            when((it as Mutable).message){
                Constants.RATE_PRODUCT -> {
                    if (rates.isEmpty()) dialog.dismiss()
                    else {
                        val product = rates[0]
                        rates.removeAt(0)
                        setProduct(product.id,product.name)
                        dialog.show()
                    }
                }
                Constants.DISMISS -> {
                    dialog.dismiss()
                }
            }
        }
    }

    fun setProduct(productId: Int, name: String) {
        viewModel.request.productId = productId
        viewModel.request.comment = ""
        viewModel.request.rate = 0f
        addRateBinding.materialRating.rating = 0f
        viewModel.request.name.set(name)
        viewModel.notifyChange()

    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        if (dialog.isShowing)
            dialog.dismiss()
    }
}