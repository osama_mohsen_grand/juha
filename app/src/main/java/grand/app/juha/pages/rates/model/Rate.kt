package grand.app.juha.pages.rates.model


import com.google.gson.annotations.SerializedName

data class Rate(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("rate")
    var rate: Float = 0f,
    @SerializedName("comment")
    var comment: String = "",
    @SerializedName("created_at")
    var createdAt: String = "",
    @SerializedName("user")
    var rater: Rater = Rater()
)