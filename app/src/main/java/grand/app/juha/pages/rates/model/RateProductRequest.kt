package grand.app.juha.pages.rates.model


import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.utils.Constants
import grand.app.juha.utils.validation.Validate

class RateProductRequest() {

    @Transient
    private val TAG = "RateProductRequest"


    @SerializedName("comment")
    @Expose
    var comment: String = ""

    @SerializedName("product_id")
    @Expose
    var productId:Int = 0


    @SerializedName("rate")
    @Expose
    var rate:Float = 0.0F


    @Transient
    var name:ObservableField<String> = ObservableField()



}