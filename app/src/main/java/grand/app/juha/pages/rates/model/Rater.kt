package grand.app.juha.pages.rates.model


import com.google.gson.annotations.SerializedName

data class Rater(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("img")
    var img: String = ""
)