package grand.app.juha.pages.rates.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class RatesResponse(
    @SerializedName("data")
    var data: ArrayList<Rate> = arrayListOf<Rate>()
) : StatusMessage()