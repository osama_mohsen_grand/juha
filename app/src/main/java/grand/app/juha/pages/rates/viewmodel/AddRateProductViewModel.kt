package grand.app.juha.pages.rates.viewmodel

import android.util.Log
import android.widget.RatingBar
import android.widget.RatingBar.OnRatingBarChangeListener
import android.widget.Toast
import androidx.annotation.RestrictTo
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.databinding.*
import androidx.lifecycle.MutableLiveData
import es.dmoral.toasty.Toasty
import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.base.MyApplication
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.rates.model.RateProductRequest
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddRateProductViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;

    lateinit var compositeDisposable: CompositeDisposable
    var request: RateProductRequest

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var lastName = ""


    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        request = RateProductRequest()
        repository.setLiveData(liveDataViewModel)
    }

    fun dismiss() {
        liveDataViewModel.value = Mutable(Constants.DISMISS)
    }

    private val TAG = "AddRateProductViewModel"
    fun submit() {
        if (request.comment.trim().isNotEmpty()) {
            Log.d(TAG,""+request.comment)
            Log.d(TAG,""+request.rate)
            val requestNew = RateProductRequest()
            requestNew.rate = request.rate
            requestNew.comment = request.comment
            requestNew.productId = request.productId
            requestNew.name.set(request.name.get())
            repository.rateProduct(requestNew)
            liveDataViewModel.value = Mutable(Constants.RATE_PRODUCT)
            Toast.makeText(
                MyApplication.instance,
                "${getString(R.string.done_rate)} ${requestNew.name.get()}",
                Toast.LENGTH_SHORT
            ).show()
        }else{
            Toasty.warning(MyApplication.instance,getString(R.string.please_leave_comment),Toast.LENGTH_SHORT).show()
        }
    }

}