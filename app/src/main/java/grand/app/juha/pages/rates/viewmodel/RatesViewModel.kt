package grand.app.juha.pages.rates.viewmodel

import android.os.Bundle
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.rates.RatesAdapter
import grand.app.juha.pages.rates.model.RatesResponse
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RatesViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var id: Int = 0
    private val TAG = "RatesViewModel"
    lateinit var response: RatesResponse
    var adapter: RatesAdapter

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
        adapter = RatesAdapter( )
    }

    fun setArgument(bundle: Bundle) {
        id = bundle.getInt(Constants.ID)
        repository.getRates(id)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        if (response.data.size > 0) adapter.update(response.data) else noData()
    }

}