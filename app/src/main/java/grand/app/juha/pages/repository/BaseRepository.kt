package grand.app.juha.pages.repository

import androidx.lifecycle.MutableLiveData
import grand.app.juha.utils.Constants

open class BaseRepository {
    private var status = 0
    private var message: String? = ""
    private val mMutableLiveData: MutableLiveData<Any?>? = null

    fun catchErrorResponse(response: Any?): Boolean {
        return false
    }

    fun getMessage(): String? {
        return message
    }

    fun getStatus(): Int {
        return status
    }

    fun setMessage(status: Int, message: String?) {
        this.status = status
        this.message = message
        if (status == Constants.RESPONSE_JWT_EXPIRE) {
            if (mMutableLiveData != null) mMutableLiveData.value = Constants.LOGOUT
        }
    }

    fun getmMutableLiveData(): MutableLiveData<Any?>? {
        return mMutableLiveData
    }

    companion object {
        private val TAG: String? = "BaseRepository"
    }
}