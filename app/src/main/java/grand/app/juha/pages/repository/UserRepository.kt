package grand.app.juha.pages.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.pages.address.add.AddAddressRequest
import grand.app.juha.pages.address.list.model.AddAddressResponse
import grand.app.juha.pages.address.list.model.AddressListResponse
import grand.app.juha.pages.cart.model.CartResponse
import grand.app.juha.pages.cart.model.PromoRequest
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository : BaseRepository {

    private val TAG = "UserRepository"

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }

    fun getAddresses() : Disposable {
        Log.d(TAG,"getAddresses Disposable")
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.ADDRESS_LIST, Any(), AddressListResponse::class.java,
            URLS.ADDRESS_LIST, true)
    }

    fun addAddress(request : AddAddressRequest) : Disposable {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.ADD_ADDRESS, request, AddAddressResponse::class.java,
            URLS.ADD_ADDRESS, true)
    }

    fun addPromo(promo: PromoRequest): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.PROMO, promo,
            CartResponse::class.java, URLS.PROMO, true
        )
    }
}