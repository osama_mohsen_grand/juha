package grand.app.juha.pages.search

import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.home.model.Category
import grand.app.juha.pages.product.model.details.Size
import grand.app.juha.pages.product.model.details.Speicification
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.utils.Constants

data class ItemSearchViewModel(val model: Product, var position: Int) {

    var mutableLiveData = MutableLiveData<Any>()

    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT,position)
    }
}