package grand.app.juha.pages.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.R
import grand.app.juha.databinding.ItemSearchBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.filter.model.FilterRequest
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.ui.ProductDetailsFragment
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.UserHelper

class SearchAdapter(private var modelList:ArrayList<Product> = ArrayList()) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    var name: String = ""
    val request = FilterRequest()
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    var search: String = ""
    var size = 0
    var mutableLiveData = MutableLiveData<Any>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSearchBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_search,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            val bundle = Bundle()
//            bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
//            bundle.putInt(Constants.ID,model.id)
//            bundle.putString(Constants.TITLE,model.name)
//            bundle.putString(Constants.NAME,search)
//            MovementHelper.startActivityBase(holder.itemView.context, bundle);


            request.name = model.name
            addInStorage(model)
            mutableLiveData.value = Mutable(Constants.SEARCH,request.name)
        }
    }

    fun addInStorage(model: Product){
        val response = UserHelper.retrieveSearchResponse()
        var exist = false
        response.data.forEach {
            if(it.id == model.id) {
                exist = true
                return
            }
        }
        if(!exist) {
            response.data.add(model)
            UserHelper.saveSearchResponse(response)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun submitSearch(modelList:ArrayList<Product>){
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun update(modelList: ArrayList<Product>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun updateInserted(modelList: ArrayList<Product>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList.addAll(modelList)
        notifyItemRangeInserted(
            size+1,
            modelList.size
        );
        size = modelList.size
    }

    private  val TAG = "SearchAdapter"
    fun removeSession() {
        val response = UserHelper.retrieveSearchResponse()
        Log.d(TAG,"response_session:"+response.data.size)
//        for(i in 0 until response.data.size){
//            Log.d(TAG,"i:"+i)
//            Log.d(TAG,"i_size:"+response.data.size)
//            if(i < modelList.size) modelList.removeAt(i)
//        }
        modelList.subList(0,response.data.size).clear()
        UserHelper.clearSearch()
        notifyDataSetChanged()
    }


    class ViewHolder(private val binding: ItemSearchBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemSearchViewModel

        //bint
        fun bind(model: Product) {
            viewModel = ItemSearchViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}