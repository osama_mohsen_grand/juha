package grand.app.juha.pages.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentSearchBinding
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.favourite.ProductListPaginateResponse
import grand.app.juha.pages.product.ui.ProductListFragment
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.MovementHelper
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SearchFragment : BaseFragment() {
    private lateinit var binding: FragmentSearchBinding

    private val TAG = "SearchFragment"

    @Inject
    lateinit var viewModel: SearchViewModel

    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var loading = true


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        Observable.create<Any> {
            binding.search.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {}
                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
                ) {
                    viewModel.currentPosition = 1
                    viewModel.search(s.toString())
                }
            })
        }.debounce(2, TimeUnit.SECONDS)
            .subscribe()


        binding.search.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                goToProductList()
                return@OnEditorActionListener true
            }
            false
        })
        init()
        setEvent()

        return binding.root
    }

    private fun init() {
        val layoutManagerItems = GridLayoutManager(context, 1)
        binding.rvProducts.apply {
            adapter = viewModel.adapter
            layoutManager = layoutManagerItems
        }

        binding.rvProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = layoutManagerItems.childCount
                    totalItemCount = layoutManagerItems.itemCount
                    pastVisiblesItems = layoutManagerItems.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            if (viewModel.more) {
                                loading = false
                                viewModel.adapter.request.name?.let { viewModel.search(it) }
                                loading = true
                            }
                        }
                    }
                } else {
                    visibleItemCount = layoutManagerItems.childCount
                    totalItemCount = layoutManagerItems.itemCount
                }
            }
        })
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG, "here:" + mutable.message)
            when (mutable.message) {
                URLS.PRODUCT_LIST -> {
                    viewModel.response = mutable.obj as ProductListPaginateResponse
                    viewModel.setData()
                }
                Constants.SEARCH -> {
                    goToProductList()
                }
                Constants.BACK -> {
                    requireActivity().finish()
                }
            }
        })
    }

    fun goToProductList(){
        viewModel.adapter.request.name?.let {
            if(viewModel.adapter.request.name.toString().trim().isNotEmpty()){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ProductListFragment::class.java.name)
                bundle.putString(Constants.URL, URLS.FILTER_SUBMIT)
                bundle.putSerializable(Constants.FILTER_MODEL, viewModel.adapter.request)
                bundle.putBoolean(Constants.FILTER, true)
                MovementHelper.startActivityBase(
                    requireContext(),
                    bundle,
                    ""
                )
            }
        }
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }

}