package grand.app.juha.pages.search

import com.google.gson.annotations.SerializedName

data class SearchRequest(@SerializedName("name") var name : String = "") {
}