package grand.app.juha.pages.search

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.favourite.ProductListPaginateResponse
import grand.app.juha.pages.product.model.list.Product
import grand.app.juha.pages.product.model.list.ProductListResponse
import grand.app.juha.pages.product.repository.ProductRepository

import grand.app.juha.utils.Constants
import grand.app.juha.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SearchViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var type: Int = 0
    private val TAG = "SearchViewModel"
    lateinit var response: ProductListPaginateResponse
    var adapter: SearchAdapter
    var hasSearchExist : ObservableBoolean = ObservableBoolean(true)
    var title: ObservableField<String> =
        ObservableField("")
    var more: Boolean = true
    var currentPosition = 0
    lateinit var preResponse: ProductListResponse

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
        preResponse = UserHelper.retrieveSearchResponse()
//        Log.d(TAG,preResponse.data.size.toString())
        hasSearchExist.set(preResponse.data.size > 0)
        adapter = SearchAdapter(preResponse.data)
        adapter.mutableLiveData.observeForever {
            getLiveData().value = it as Mutable
        }
    }

    fun back(){
        getLiveData().value = Mutable(Constants.BACK)
    }

    fun search(s: String) {
        if (s.trim() != "") {
            adapter.request.name = s.trim()
            repository.submitFilter(adapter.request)
        }
    }

    fun remove(){
        adapter.removeSession()
        hasSearchExist.set(false)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
//        adapter.update(response.data)
        more = (response.data.nextPageUrl != null)
        when(currentPosition){
            1 -> adapter.update(response.data.products)
            else -> adapter.updateInserted(response.data.products)
        }
        currentPosition++
    }
}