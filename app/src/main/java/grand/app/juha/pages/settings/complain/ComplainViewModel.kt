package grand.app.juha.pages.settings.complain

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.settings.repository.SettingsRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ComplainViewModel: BaseViewModel {
    private val TAG = "ComplainViewModel"
    @Inject
    lateinit var repository: SettingsRepository;
    var compositeDisposable: CompositeDisposable
    var liveDataViewModel: MutableLiveData<Mutable>
    var request: ComplainRequest


    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        request = ComplainRequest()
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    fun submit(){
        if(request.isValid()){
            repository.suggestion(request)
        }
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}