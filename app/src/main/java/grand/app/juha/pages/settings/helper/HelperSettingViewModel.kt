package grand.app.juha.pages.settings.helper

import grand.app.juha.R
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.account.AccountSettingAdapter
import grand.app.juha.pages.account.model.AccountModel
import grand.app.juha.pages.auth.login.LoginFragment
import grand.app.juha.pages.settings.complain.ComplainFragment
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.pages.settings.term.SettingsViewModel
import grand.app.juha.pages.settings.term.ui.SettingsFragment
import grand.app.juha.pages.settings.term.ui.TermsFragment
import grand.app.juha.pages.social.SocialFragment
import grand.app.juha.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HelperSettingViewModel : BaseViewModel {
    //    @Inject
    lateinit var repository: SettingsRepository;
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = AccountSettingAdapter()
    var bottomList = arrayListOf<AccountModel>()

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        repository.setLiveData(getLiveData())
    }

    init {

        bottomList.add(
            AccountModel(
                getString(R.string.rate_app),
                R.drawable.ic_rate
            )
        )
        bottomList.add(AccountModel(getString(R.string.share_app), R.drawable.ic_share))
        bottomList.add(AccountModel(getString(R.string.complains_and_suggestions), R.drawable.ic_complains,ComplainFragment::class.java.name,isAuthorize = UserHelper.isLogin()))
        bottomList.add(AccountModel(getString(R.string.terms_and_conditions),
            R.drawable.ic_terms, TermsFragment::class.java.name,isAuthorize = UserHelper.isLogin()))
        bottomList.add(
            AccountModel(
                getString(R.string.policy), R.drawable.ic_policy,
                SettingsFragment::class.java.name,
                isAuthorize = true
            )
        )
        bottomList.add(
            AccountModel(
                getString(R.string.call_us),
                R.drawable.ic_call_us,
                SocialFragment::class.java.name,
                3,
                isAuthorize = true
            )
        )
        adapter.update(bottomList)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}

