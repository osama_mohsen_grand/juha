package grand.app.juha.pages.settings.helper

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentHelperSettingsBinding
import grand.app.juha.databinding.FragmentNotificationListBinding
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.notification.viewmodel.NotificationListViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import javax.inject.Inject

class HelperSettingsFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentHelperSettingsBinding
    @Inject
    lateinit var viewModel: HelperSettingViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_helper_settings, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}