package grand.app.juha.pages.settings.notification.model


import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("id")
    var id: Int,
    @SerializedName("img")
    var image: String,
    @SerializedName("title")
    var title: String,
    @SerializedName("desc")
    var message: String,
    @SerializedName("created_at")
    var date: String,
    @SerializedName("type")
    var type: Int,
    @SerializedName("target_id")
    var targetId: String
)