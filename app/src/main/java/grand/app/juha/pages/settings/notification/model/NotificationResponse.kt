package grand.app.juha.pages.settings.notification.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class NotificationResponse(
    @SerializedName("data")
    var data : ArrayList<Notification>
) : StatusMessage()