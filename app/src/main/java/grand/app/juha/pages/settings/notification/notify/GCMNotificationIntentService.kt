package grand.app.juha.pages.settings.notification.notify

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import grand.app.juha.R
import grand.app.juha.activity.BaseActivity
import grand.app.juha.base.MyApplication
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.session.SharedPreferenceHelper
import grand.app.juha.utils.session.UserHelper
import java.io.IOException
import java.net.URL
import java.util.*


class GCMNotificationIntentService : FirebaseMessagingService() {
    override fun onNewToken(s: String) {
        super.onNewToken(s)
        UserHelper.saveKey(Constants.TOKEN, s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        //ar_text , en_text
        if (UserHelper.getUserId() != -1) {
            val details = remoteMessage.data[Constants.MESSAGE]
            details?.let {
                val notificationGCMModel: NotificationGCMModel =
                    NotificationGCMModel.getNotification(details)
                val title: String = notificationGCMModel.title
                val message: String = notificationGCMModel.body
                val intent = Intent(this, BaseActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(Constants.BUNDLE_NOTIFICATION, notificationGCMModel)
                intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle)
//                sendBroadCast(notificationGCMModel)
                val pendingIntent = PendingIntent.getActivity(
                    applicationContext,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT
                )
                val notification =
                    NotificationCompat.Builder(this)
                notification.priority = NotificationManager.IMPORTANCE_HIGH
                notification.setContentTitle(title)
                notification.color =
                    ContextCompat.getColor(MyApplication.instance, R.color.colorPrimary);
                notification.setContentText(message)
//                notification.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                //        notification.setAutoCancel(false);
                notification.setAutoCancel(true)
                notification.setShowWhen(true)
                notification.setLargeIcon(
                    BitmapFactory.decodeResource(
                        resources,
                        R.mipmap.ic_launcher
                    )
                )



                if (notificationGCMModel.image != "") {
                    try {
                        val bitmap = AppHelper.getBitmapFromUrl(notificationGCMModel.image)
                        notification.setStyle(
                            NotificationCompat.BigPictureStyle()
                                .bigPicture(bitmap)
                                .bigLargeIcon(bitmap)
                        )
                    } catch (e: IOException) {
                        println(e)
                    }
                } else notification.setStyle(
                    NotificationCompat.BigTextStyle().bigText(notificationGCMModel.message)
                )

                notification.setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
                notification.setContentIntent(pendingIntent)
                val currentApiVersion = Build.VERSION.SDK_INT
                if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
                    notification
                        .setSmallIcon(R.drawable.ic_logo_notificaion)//set from notification drawable
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                } else {
                    notification.setSmallIcon(R.drawable.ic_logo_notificaion)
                }
                val random = Random().nextInt(10000000) + 1
                //Android O
                val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val helper = NotificationHelper(applicationContext)
//                    helper.createChannels(title, message, intent);
//                    helper.createChannels(title, message, intent);
                    val builder: Notification.Builder =
                        helper.getChannel(title, message, pendingIntent, random,notificationGCMModel)
                    NotificationHelper
                        .getManager(helper)?.notify(random, builder.build())
                }else
                    notificationManager.notify(random, notification.build())
            }
            //here
            val uri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            val r: Ringtone = RingtoneManager.getRingtone(applicationContext, uri);
            r.play();
        }

    }

//    private fun sendBroadCast(notificationGCMModel: NotificationGCMModel) {
//        Log.e(TAG, "sendBroadCast: ")
//        var intent: Intent? = null
//        val bundle = Bundle()
//        bundle.putSerializable(Constants.BUNDLE_NOTIFICATION, notificationGCMModel)
//        when (notificationGCMModel.notification_type) {
//            1 -> {
//                intent = Intent(Constants.NOTIFICATIONS)
//                val cart = SharedPreferenceHelper.getKey(Constants.CART)
//                if (!cart.equals("")) {
//                    var counter: Int? = cart?.toInt()
//                    if (counter != null) {
//                        counter++
//                        SharedPreferenceHelper.saveKey(Constants.CART, counter.toString())
//                        applicationContext.sendBroadcast(intent)
//                    }
//                }
//            }
//            2 -> {
//
//            }
//        }
//    }

    companion object {
        private const val TAG = "GCMNotificationIntentSe"
    }
}
