package grand.app.juha.pages.settings.notification.notify

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import grand.app.juha.R
import grand.app.juha.utils.Constants

class MyNotificationManager private constructor(private val mCtx: Context) {
    fun displayNotification(
        title: String?,
        body: String?,
        intent: Intent?
    ) {
        val mBuilder =
            NotificationCompat.Builder(mCtx, Constants.CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)


        /*
         *  Clicking on the notification will take us to this intent
         *  Right now we are using the MainActivity as this is the only activity we have in our application
         *  But for your project you can customize it as you want
         * */


        /*
         *  Now we will create a pending intent
         *  The method getActivity is taking 4 parameters
         *  All paramters are describing themselves
         *  0 is the request code (the second parameter)
         *  We can detect this code in the activity that will open by this we can get
         *  Which notification opened the activity
         * */
        val pendingIntent =
            PendingIntent.getActivity(mCtx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        /*
         *  Setting the pending intent to notification builder
         * */mBuilder.setContentIntent(pendingIntent)
        val mNotifyMgr =
            mCtx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        /*
         * The first parameter is the notification id
         * better don't give a literal here (light now we are giving a int literal)
         * because using this id we can modify it later
         * */mNotifyMgr?.notify(1, mBuilder.build())
    }

    companion object {
        private var mInstance: MyNotificationManager? = null

        @Synchronized
        fun getInstance(context: Context): MyNotificationManager? {
            if (mInstance == null) {
                mInstance = MyNotificationManager(context)
            }
            return mInstance
        }
    }

}