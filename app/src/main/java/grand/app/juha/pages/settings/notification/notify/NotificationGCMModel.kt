package grand.app.juha.pages.settings.notification.notify

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class NotificationGCMModel : Serializable {
    @SerializedName("id")
    @Expose
    var id = 0

    @SerializedName("order_id")
    @Expose
    var orderId = 0

    @SerializedName("order_number")
    @Expose
    var orderNumber = ""

    /*
        notification list   notification_type => 1
        notification order  notification_type => 2
        notification chat   notification_type => 3
     */
    @SerializedName("notification_type")
    @Expose
    var notification_type = 0

    @SerializedName("title")
    @Expose
    var title: String = ""

    @SerializedName("time")
    @Expose
    var time: String = ""

    @SerializedName("message")
    @Expose
    var message: String = ""

    //message=>1, image=>2
    @SerializedName("product_id")
    @Expose
    var productId = 0

    @SerializedName("body")
    @Expose
    var body: String = ""

    @SerializedName("image")
    @Expose
    var image: String = ""


    companion object {
        fun getNotification(mJsonString: String): NotificationGCMModel {
            val parser = JsonParser()
            val mJson = parser.parse(mJsonString)
            val gson = Gson()
            return gson.fromJson(mJson, NotificationGCMModel::class.java)
        }    
    }
    
}