package grand.app.juha.pages.settings.notification.notify

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import grand.app.juha.R
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper


class NotificationHelper(base: Context) : ContextWrapper(base) {
    var manager: NotificationManager? = null
    fun createChannels(
        title: String?,
        message: String?,
        intent: Intent?
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel =
                NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, importance)
            mChannel.description = Constants.CHANNEL_DESCRIPTION
            mChannel.enableLights(true)
            mChannel.lightColor = Color.RED
            mChannel.enableVibration(true)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            Companion.getManager(this)!!.createNotificationChannel(mChannel)
            MyNotificationManager.getInstance(this)!!.displayNotification(title, message, intent)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun getChannel(
        title: String?,
        body: String?,
        pendingIntent: PendingIntent?,
        number: Int,
        notificationGCMModel: NotificationGCMModel = NotificationGCMModel()
    ): Notification.Builder {
        val builder = Notification.Builder(applicationContext, Constants.CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(body)
            .setContentIntent(pendingIntent)
            .setFullScreenIntent(pendingIntent, true)
            .setNumber(number) //                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)

        if (notificationGCMModel.image == "") {
            builder.setLargeIcon(
                BitmapFactory.decodeResource(
                    applicationContext.resources,
                    R.mipmap.ic_launcher
                )
            )
        } else {
            val bitmap = AppHelper.getBitmapFromUrl(notificationGCMModel.image)
            builder.setLargeIcon(
                bitmap
            )
        }
        return builder
    }

    companion object {
        var CHANNEL_NAME = ""
        var CHANNEL_DESCRIPTION = ""
        fun getManager(notificationHelper: NotificationHelper): NotificationManager? {
            if (notificationHelper.manager == null) notificationHelper.manager =
                notificationHelper.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            return notificationHelper.manager
        }
    }

    init {
        CHANNEL_NAME = base.getString(R.string.app_name)
        CHANNEL_DESCRIPTION = base.getString(R.string.app_name)
    }
}
