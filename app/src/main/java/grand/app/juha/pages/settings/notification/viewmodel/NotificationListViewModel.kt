package grand.app.juha.pages.settings.notification.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.settings.notification.NotificationAdapter
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class NotificationListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "NotificationListViewModel"
    lateinit var response: NotificationResponse
    var adapter: NotificationAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = NotificationAdapter( )
        repository.getNotifications()
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        if (response.data.size > 0) adapter.update(response.data) else noData()
    }

}