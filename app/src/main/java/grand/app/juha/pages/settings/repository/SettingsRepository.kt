package grand.app.juha.pages.settings.repository

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.StatusMessage
import grand.app.juha.pages.social.model.SocialResponse
import grand.app.juha.connection.ConnectionHelper
import grand.app.juha.pages.address.list.model.AreaListResponse
import grand.app.juha.pages.repository.BaseRepository
import grand.app.juha.pages.settings.complain.ComplainRequest
import grand.app.juha.pages.settings.notification.model.NotificationResponse
import grand.app.juha.pages.settings.term.SettingsResponse
import grand.app.juha.pages.settings.term.model.TermsResponse
import grand.app.juha.pages.splash.models.MainSettingsResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingsRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }


    fun getMainSettings() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MAIN_SETTINGS, Any(), MainSettingsResponse::class.java,
            URLS.MAIN_SETTINGS, false)
    }

    fun getAreaData() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ALL_AREA_DATA, Any(), AreaListResponse::class.java,
            URLS.ALL_AREA_DATA, true)
    }

    fun suggestion(request: ComplainRequest): Disposable {
        return connectionHelper.requestApi(Constants.POST_REQUEST,
            URLS.SUGGESTION, request, StatusMessage::class.java,
            URLS.SUGGESTION, true)
    }

    fun getSettings() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.SETTINGS, Any(),SettingsResponse::class.java,
            URLS.SETTINGS, true)
    }

    fun getTerms(): Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.TERMS, Any(), TermsResponse::class.java,
            URLS.TERMS, true)
    }

    fun getNotifications(): Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.NOTIFICATIONS, Any(), NotificationResponse::class.java,
            URLS.NOTIFICATIONS, true)
    }

    fun getSocials() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.SOCIALS, Any(), SocialResponse::class.java,
            URLS.SOCIALS, true)
    }
}