package grand.app.juha.pages.settings.term


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class SettingsResponse(
    @SerializedName("data")
    @Expose
    var data: String = ""
) : StatusMessage()