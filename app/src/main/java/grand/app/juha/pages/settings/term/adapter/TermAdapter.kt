package grand.app.juha.pages.settings.term.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.databinding.ItemTermBinding
import grand.app.juha.pages.settings.term.model.Term
import grand.app.juha.pages.settings.term.viewmodel.ItemTermViewModel

class TermAdapter(private var modelList:List<Term> = ArrayList()) : RecyclerView.Adapter<TermAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemTermBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_term,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: ArrayList<Term>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemTermBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemTermViewModel

        //bint
        fun bind(model: Term) {
            viewModel = ItemTermViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}