package grand.app.juha.pages.settings.term.model


import com.google.gson.annotations.SerializedName

data class Term(
    @SerializedName("desc")
    var desc: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("title")
    var title: String = ""
)