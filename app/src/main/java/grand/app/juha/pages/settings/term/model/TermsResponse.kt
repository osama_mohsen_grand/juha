package grand.app.juha.pages.settings.term.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class TermsResponse(
    @SerializedName("data")
    var data: ArrayList<Term> = ArrayList()
): StatusMessage()