package grand.app.juha.pages.settings.term.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentSettingsBinding
import grand.app.juha.pages.settings.term.SettingsResponse
import grand.app.juha.pages.settings.term.SettingsViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import javax.inject.Inject

class SettingsFragment : BaseFragment() {
    private lateinit var binding: FragmentSettingsBinding
    @Inject
    lateinit var viewModel: SettingsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.SETTINGS) {
                viewModel.setData(mutable.obj as SettingsResponse)
            }
            else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }
        })
    }
    private var hasNextPage: Boolean = true
    val TAG: String = this::class.java.name
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}