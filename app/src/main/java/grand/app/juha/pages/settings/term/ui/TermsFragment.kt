package grand.app.juha.pages.settings.term.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentSettingsBinding
import grand.app.juha.databinding.FragmentTermsBinding
import grand.app.juha.pages.settings.term.SettingsResponse
import grand.app.juha.pages.settings.term.SettingsViewModel
import grand.app.juha.pages.settings.term.model.TermsResponse
import grand.app.juha.pages.settings.term.viewmodel.TermsViewModel
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import javax.inject.Inject

class TermsFragment : BaseFragment() {
    private lateinit var binding: FragmentTermsBinding
    @Inject
    lateinit var viewModel: TermsViewModel
    val TAG: String = this::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
            when(mutable.message){
                URLS.TERMS -> viewModel.setData(mutable.obj as TermsResponse)
                Constants.BACK -> requireActivity().finish()
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}