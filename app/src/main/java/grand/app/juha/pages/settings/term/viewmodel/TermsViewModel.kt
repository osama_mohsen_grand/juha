package grand.app.juha.pages.settings.term.viewmodel

import androidx.databinding.ObservableField
import grand.app.juha.base.BaseViewModel
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.pages.settings.term.adapter.TermAdapter
import grand.app.juha.pages.settings.term.model.TermsResponse
import grand.app.juha.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class TermsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
    var compositeDisposable: CompositeDisposable

    var type: Int = 0
    private val TAG = "NotificationListViewModel"
    lateinit var response: TermsResponse
    var adapter: TermAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(getLiveData())
        adapter = TermAdapter( )
        repository.getTerms()
    }

    fun back(){
        getLiveData().value = Mutable(Constants.BACK)
    }

    protected fun unSubscribeFromObservable() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: TermsResponse) {
        this.response = response
        adapter.update(response.data)
        show.set(true)
    }

}