package grand.app.juha.pages.social

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.social.model.Social
import grand.app.juha.pages.social.viewmodel.ItemSocialViewModel
import grand.app.juha.R
import grand.app.juha.databinding.ItemSocialBinding
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper


class SocialAdapter : RecyclerView.Adapter<SocialAdapter.ViewHolder>() {
    // fetch list data
    private  val TAG = "SocialAdapter"
    private lateinit var modelList: ArrayList<Social>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSocialBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_social,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    lateinit var customActivity: Activity
    fun setActivity(activity: Activity){
        this.customActivity = activity
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
//        Log.d(TAG,modelList[position].value)
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            val model: Social = modelList[mutable.position]
            if (mutable.message == Constants.SUBMIT) {
                Log.d(TAG,model.name)
                if (model.type == "phone")
                    AppHelper.openDialNumber(holder.itemView.context, model.text)
                else if (model.type == "email")
                    AppHelper.openEmail(holder.itemView.context, model.text)
                else
                    AppHelper.openBrowser(holder.itemView.context, model.text)
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return if (::modelList.isInitialized) modelList.size else 0
    }

    fun update(modelList: ArrayList<Social>) {
        //update data after call service again in scroll , and notify list which end with
        Log.d(TAG,"update")
        this.modelList = modelList
        Log.d(TAG,"update : "+modelList.size)
        notifyDataSetChanged()
//        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    class ViewHolder(private val binding: ItemSocialBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemSocialViewModel
        fun bind(model: Social) {
            viewModel =
                ItemSocialViewModel(
                    model,
                    adapterPosition
                )
            binding.viewmodel = viewModel
        }
    }
}