package grand.app.juha.pages.social

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.social.model.SocialResponse
import grand.app.juha.pages.social.viewmodel.SocialViewModel
import grand.app.juha.R
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentSocialBinding
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS

import javax.inject.Inject

class SocialFragment : BaseFragment() {
    private lateinit var binding: FragmentSocialBinding
    @Inject
    lateinit var viewModel: SocialViewModel

    private  val TAG = "AccountSettingFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_social, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        // Inflate the layout for this fragment
        viewModel.adapter.setActivity(requireActivity())
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            when(mutable.message){
                URLS.SOCIALS -> {
                    viewModel.setData(mutable.obj as SocialResponse)
                }
                Constants.BACK -> {
                    requireActivity().finish()
                }
            }
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)

    }
}