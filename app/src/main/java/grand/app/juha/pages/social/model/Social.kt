package grand.app.juha.pages.social.model


import com.google.gson.annotations.SerializedName

data class Social(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("text")
    var text: String = "",
    @SerializedName("type")
    var type: String = "",
    @SerializedName("image")
    var image: Int = 0
)