package grand.app.juha.pages.social.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class SocialResponse(
    @SerializedName("data")
    var social: ArrayList<Social> = ArrayList()
) : StatusMessage()