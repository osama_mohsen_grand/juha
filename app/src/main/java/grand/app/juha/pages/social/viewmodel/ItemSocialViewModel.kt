package grand.app.juha.pages.social.viewmodel

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.social.model.Social
import grand.app.juha.utils.Constants

data class ItemSocialViewModel(val model : Social, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}