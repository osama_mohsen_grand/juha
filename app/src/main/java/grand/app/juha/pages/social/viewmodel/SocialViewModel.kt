package grand.app.juha.pages.social.viewmodel

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import grand.app.juha.R
import grand.app.juha.model.base.Mutable
import grand.app.juha.pages.social.SocialAdapter
import grand.app.juha.pages.social.model.SocialResponse
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.pages.social.model.Social
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SocialViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
    private val TAG = "SocialViewModel"
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = SocialAdapter()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    lateinit var response: SocialResponse

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        repository.setLiveData(liveDataViewModel)
        repository.getSocials()
    }


    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun back(){
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun setData(response: SocialResponse) {
        this.response = response
/*        if(response.social.size > 0) response.social[0].image = R.mipmap.ic_facebook_white
        if(response.social.size > 1) response.social[1].image = R.mipmap.ic_twitter_white
        if(response.social.size > 2) response.social[2].image = R.mipmap.ic_instgram
        if(response.social.size > 3) response.social[3].image = R.mipmap.ic_whatsapp_white
        if(response.social.size > 4) response.social[4].image = R.mipmap.ic_email
        if(response.social.size > 5) response.social[5].image = R.mipmap.ic_web*/

        for(social : Social in response.social){
            when(social.id){
                1 -> social.image = R.mipmap.ic_facebook_white
                2 -> social.image = R.mipmap.ic_twitter_white
                3 -> social.image = R.mipmap.ic_instgram
                4 -> social.image = R.mipmap.ic_whatsapp_white
                5 -> social.image = R.mipmap.ic_email
                6 -> social.image = R.mipmap.ic_web
            }
        }

        adapter.update(response.social)
        showPage(true)
    }
}

