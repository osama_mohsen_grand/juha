package grand.app.juha.pages.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.juha.model.base.Mutable
import grand.app.juha.R
import grand.app.juha.activity.MainActivity
import grand.app.juha.base.BaseFragment
import grand.app.juha.base.MyApplication
import grand.app.juha.base.di.IApplicationComponent
import grand.app.juha.databinding.FragmentSplashBinding
import grand.app.juha.pages.intro.IntroFragment
import grand.app.juha.pages.splash.models.MainSettingsResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.URLS
import grand.app.juha.utils.helper.AppHelper
import grand.app.juha.utils.helper.MovementHelper
import grand.app.juha.utils.session.LanguagesHelper
import grand.app.juha.utils.session.UserHelper
import javax.inject.Inject

class SplashFragment : BaseFragment() {
    private lateinit var binding: FragmentSplashBinding
    @Inject
    lateinit var viewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        viewModel.call()
        setEvent()
        LanguagesHelper.setLanguage(Constants.DEFAULT_LANGUAGE)
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.getLiveData().observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if(mutable.message == URLS.MAIN_SETTINGS){
                finishActivity()
                UserHelper.saveJsonResponse(Constants.SETTINGS,mutable.obj as MainSettingsResponse)
                val bundle = Bundle()
                if(UserHelper.getUserId() == -1) {
                    bundle.putString(Constants.PAGE, IntroFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle);
                }else{
                    MovementHelper.startActivity(context, MainActivity::class.java);
                }
            }else if (mutable.message == Constants.URL){
                AppHelper.openBrowser(requireContext(),"https://2grand.net/")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.getLiveData())
    }
}