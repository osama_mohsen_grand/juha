package grand.app.juha.pages.splash

import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.utils.Utils
import com.bumptech.glide.util.Util
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.AppHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
//    private var liveData: LiveData<Mutable> = MutableLiveData()
    var x: Int = 6

    @Inject
    constructor(repository: SettingsRepository){
        this.repository = repository
        repository.setLiveData(getLiveData())
    }

    private val compositeDisposable: CompositeDisposable? = CompositeDisposable()

    fun call() {
        compositeDisposable?.add(repository.getMainSettings())
        notifyChange()
    }

    fun byGrand(){
        getLiveData().value = Mutable(Constants.URL)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}