package grand.app.juha.pages.splash.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Banner(
    @SerializedName("attribute_id")
    @Expose
    var attributeId: Int = 0,
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("img")
    @Expose
    var img: String = "",
    @SerializedName("link_type")
    @Expose
    var linkType: Int = 0
)  : Serializable