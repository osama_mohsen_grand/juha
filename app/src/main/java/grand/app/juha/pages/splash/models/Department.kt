package grand.app.juha.pages.splash.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.offer.model.Sub

data class Department(
    @SerializedName("banners")
    @Expose
    var banners: List<Banner> = listOf(),
    @SerializedName("featured")
    @Expose
    var featured: Int = 0,
    @SerializedName("icon")
    @Expose
    var icon: String = "",
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("img")
    @Expose
    var img: String = "",
    @SerializedName("name")
    @Expose
    var name: String = "",
    @SerializedName("status")
    @Expose
    var status: Int = 0,
    @SerializedName("subs")
    @Expose
    var subs: List<Sub> = listOf()
)