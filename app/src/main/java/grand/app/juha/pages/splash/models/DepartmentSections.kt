package grand.app.juha.pages.splash.models


data class DepartmentSections(
    var departmentSection: ArrayList<Section> = arrayListOf()
)