package grand.app.juha.pages.splash.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class MainSettingsResponse(
    @SerializedName("data")
    @Expose
    var data: ArrayList<Department> = arrayListOf()
):StatusMessage()