package grand.app.juha.pages.splash.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.offer.model.Sub

data class Section(
    @SerializedName("subs")
    @Expose
    var subs: ArrayList<Sub> = arrayListOf()
)