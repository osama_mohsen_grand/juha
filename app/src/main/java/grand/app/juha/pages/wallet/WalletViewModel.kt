package grand.app.juha.pages.wallet

import androidx.lifecycle.MutableLiveData
import grand.app.juha.model.base.Mutable
import grand.app.juha.base.BaseViewModel
import grand.app.juha.pages.cart.adapter.CartDetailsAdapter
import grand.app.juha.pages.product.repository.ProductRepository
import grand.app.juha.pages.settings.repository.SettingsRepository
import grand.app.juha.pages.wallet.model.WalletResponse
import io.reactivex.disposables.CompositeDisposable
import okhttp3.internal.wait
import javax.inject.Inject

class WalletViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
//    private var liveData: LiveData<Mutable> = MutableLiveData()
    var x: Int = 6
    var response: WalletResponse = WalletResponse()

    var adapterProducts: CartDetailsAdapter = CartDetailsAdapter()
    var adapterProductsRefund: CartDetailsAdapter = CartDetailsAdapter()

    @Inject
    constructor(repository: ProductRepository){
        this.repository = repository
        repository.setLiveData(getLiveData())
        call()
    }

    private val compositeDisposable: CompositeDisposable? = CompositeDisposable()

    fun call() {
        compositeDisposable?.add(repository.getWallet())
        notifyChange()
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun setData(){
        response.data.ratedProducts.forEach {
            it.img = it.defaultImg.img
        }
        adapterProducts.update(response.data.ratedProducts)
        adapterProductsRefund.update(response.data.refundedOrders)
        showPage(true)
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}