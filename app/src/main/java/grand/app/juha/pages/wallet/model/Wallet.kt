package grand.app.juha.pages.wallet.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.pages.cart.model.Cart
import grand.app.juha.pages.product.model.list.Product

data class Wallet(
    @SerializedName("wallet")
    var wallet: String = "",
    @SerializedName("rated-products")
    var ratedProducts: ArrayList<Cart> = arrayListOf(),
    @SerializedName("refunded-orders")
    var refundedOrders: ArrayList<Cart> = arrayListOf()
)