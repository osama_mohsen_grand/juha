package grand.app.juha.pages.wallet.model


import com.google.gson.annotations.SerializedName
import grand.app.juha.base.StatusMessage

data class WalletResponse(
    @SerializedName("data")
    var data: Wallet = Wallet()
) : StatusMessage()