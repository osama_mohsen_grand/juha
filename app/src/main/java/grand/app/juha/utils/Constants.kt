package grand.app.juha.utils

object Constants {
    const val PRODUCT_LIST: String = "product_list"
    const val HAS_ESCAPE: String = "has_escape"
    const val DIALOG: String = "dialog"
    const val SELECT: String = "select"
    const val TITLE: String =""
    val CHANNEL_ID: String = "ELQASAR"
    val CHANNEL_NAME: String = "ELQASAR Application"
    val CHANNEL_DESCRIPTION: String = "ELQASAR Application Desc"
    const val HOME: String = "home"
    const val BUNDLE_NOTIFICATION: String = "BUNDLE_NOTIFICATION"
    val FAVOURITE: String = "favourite"
    const val TYPE: String = "type"
    val ADD_TO_CART: String = "add_to_cart"
    val EMAIL: String = "email"
    val PHONE: String = "phone"
    val VERIFY_ID: String = "verify_id"
    val UPDATE_PROFILE: String = "update_profile"
    val IMAGE: String = "img"
    const val SETTINGS = "settings"
    const val SELECT_IMAGE: String = "select_image"
    val LOGOUT: String = "logout"
    val ADD_ADDRESS: String = "add_address"
    const val URL: String = "url"
    const val MESSAGE: String = "message"
    val NEXT: String = "next"
    val FINISH: String = "finish"
    val DEPARTMENTS: String = "departments"
    val SORT_BY :String = "sort_by"
    val RATE_PRODUCT :String = "rate_product"


    const val ERROR_TOAST: String = "error_toast"
    const val ERROR: String = "error"
    const val SHOW_PROGRESS: String = "showProgress"
    const val HIDE_PROGRESS: String = "hideProgress"
    const val SERVER_ERROR: String = "serverError"
    const val ERROR_RESPONSE: String = "error_response"
    const val ERROR_LOGIN_RESPONSE: String = "error_login_response"
    const val FAILURE_CONNECTION: String = "failure_connection"
    const val LOGIN_FIRST: String = "login_first"
    const val LANGUAGE: String = "language"
    const val DEFAULT_LANGUAGE: String = "ar"
    val TOKEN: String = "token"
    val PAGE: String = "page"
    const val NAME_BAR: String = "name_bar"
    const val BUNDLE: String = "bundle"
    val FILTER: String = "filter"
    val FILTER_MODEL: String = "filter_model"
    const val ADDRESS: String = "address"
    val IS_BACK: String = "is_backl"
    val CITY: String = "city"
    const val AREA: String = "area"
    const val GOVERNMENT: String = "government"
    val VERIFY: String = "verify"
    val WRITE_CODE: String = "write_code"
    val CATEGORY_ID: String = "category_id"
    val SUB_CATEGORY_ID: String = "sub_category_id"


    val SUCCESS: String = "success"

    //REQUESTS
    const val POST_REQUEST = 200
    const val GET_REQUEST = 201
    const val DELETE_REQUEST = 202

    //RESPONSES
    const val RESPONSE_SUCCESS = 200
    const val RESPONSE_ERROR = 203
    const val RESPONSE_TRIP_FAILED = 401
    const val RESPONSE_FAILED = 402
    const val RESPONSE_JWT_EXPIRE = 403
    const val RESPONSE_NOT_ACTIVE = 405
    const val RESPONSE_401 = 401
    const val GPS_REQUEST = 406
    const val LOCATION_REQUEST = 407
    const val RATE_REQUEST = 408
    const val FILTER_REQUEST = 409
    const val SEARCH_REQUEST = 410
    const val LOCATION_PERMISSION = 6000
    const val FILE_TYPE_VIDEO = 377
    const val FILE_TYPE_IMAGE = 378
    const val FILE_TYPE_DOCUMENT = 379
    const val REQUEST_MODELS_RESULT = 380
    const val FAVOURITE_RESULT = 381
    val RELOAD_REQUEST: Int = 382

    val CONTACT_US: String = "contact_us"
    val TERMS_AND_CONDITIONS: String = "terms_and_conditions"
    val PRODUCT: String = "product"
    val RATE_APP: String = "rate_app"
    val HIDE_KEYBOARD: String = "hide_keyboard"
    val LOGIN: String = "login"
    val BE_INTRODUCER: String = "be_introducer"
    val FORGET_PASSWORD: String = "forget_password"
    val BACK: String = "back"
    val SEARCH: String = "search"
    val MAIN: String = "main"
    val SEARCH_BAR_TEXT: String = "search_bar_text"
    val REGISTER: String = "register"
    val SKIP: String = "skip"
    const val ADDRESS_RESULT = 1890
    const val CANCELLED_RESULT = 1891
    const val REFUND_RESULT = 1892
    const val FILTER_RESULT = 1893
    const val STATUS_ID: String = "staus_id"
    val RATE: String = "rate"
    val ORDER: String = "order"
    val ORDER_ID: String = "order_id"
    val TOGGLE: String = "toggle"
    val OFFERS: String = "offers"
    val SUBMIT: String = "submit"
    val MORE: String = "more"
    val REFUND: String = "refund"
    val DELETE: String = "delete"
    val RATES: String = "rates"
    val DISMISS: String = "dismiss"

    val SHARE: String = "share"
    val NAME_HEADER: String = "name_header"
    val STATUS: String = "status"
    val SIZE_ID: String = "size_id"
    val COLOR_ID: String = "color_id"
    val CHANGE_PASSWORD: String = "forget-password"
    val CART: String = "cart"
    val PROMO: String = "promo"
    val LANGUAGE_AR: String = "ar"
    val LANGUAGE_EN: String = "en"
    val POSITION: String = "position"
    const val PRODUCTS_RESPONSE: String = "products"
    val ID: String = "id"
    val MINUS: String = "minus"
    val ADD: String = "add"
    val LANGUAGE_SELECTED: String = "language_selected"
    val TRUE: String = "true"
    val DELETE_SORT: String = "delete_sort"
    val CANCELED: String = "canceled"
    const val TYPE_COLOR = 1
    const val TYPE_SIZE = 2
    val RELOAD: String = "reload"
    val ACCOUNT: String = "account"
    val NOT_FOUND: String = "not_found"
    val RC_SIGN_IN: Int = 425
    val SIGN_IN_RESULT_CODE: Int = 426


    val GRID: String = "grid"
    val FACEBOOK: String = "facebook"
    val TWITTER: String = "twitter"
    val WHATSAPP: String = "whatsapp"
    const val GOOGLE: String = "google"

}