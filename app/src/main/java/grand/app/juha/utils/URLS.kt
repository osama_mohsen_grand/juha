package grand.app.juha.utils

object URLS {
    const val FILTER: String  = "settings/filter-data"
    const val FILTER_SUBMIT: String  = "settings/filter-address"

    const val BASE_URL: String = "http://juha.my-staff.net/api/"
    const val MAIN_SETTINGS: String = "settings"
    const val ALL_AREA_DATA: String = "settings/all-area-data"
    const val LOGIN: String = "user/login"
    const val LOGIN_SOCIAL: String = "user/social-login"
    const val REGISTER: String = "user/register"
    const val FORGET_PASSWORD: String = "check-phone"
    const val UPDATE_PROFILE: String = "user/update-profile"
    const val VERIFY: String = "user/verify-code"
    const val SEND_CODE: String = "user/send-code"

    const val UPDATE_TOKEN: String = "user/update-token"

    //    public  final static String COMPANIES = "company/companies?category_id=5&lat=30.1341689&lng=31.3655877&city_id=1";
    const val ORDER_STATUS: String = "order/status"
    const val USER_CART: String = "orders/cart"
    const val CHECKOUT: String = "orders/check-out"
    const val PROMO: String = "orders/add-promo-code"
    const val ADD_TO_CART: String = "orders/add-to-cart"
    const val DELETE_ITEM_CART: String = "orders/remove-from-cart/"

    const val ADD_FAVOURITE: String = "settings/favorite/"
    const val FAVOURITE: String = "settings/favorites"

    const val PRODUCT_SEND_MESSAGE: String = "product/send-message"
    const val SETTINGS: String = "settings/privacy"
    const val NOTIFICATIONS: String = "settings/notifications"
    const val SUGGESTION: String = "settings/suggestions"
    const val PRIVACY: String = "settings/privacy"
    const val TERMS: String = "settings/terms"
    const val CONTACT_US: String = "contactUs/add"
    const val CHANGE_PASSWORD: String = "user/change-password"
    const val RATES: String = "settings/rates"
    const val MOST_RATED: String = "settings/most-rated-products"
    const val RATE_PRODUCT = "orders/rate-product"
    const val SHOP_PRODUCTS: String = "settings/products"
    const val CATEGORY_PRODUCT: String = "settings/category-products"
    const val SUB_CATEGORY_PRODUCT: String = "settings/sub-category-products"

    const val REASONS: String = "settings/reasons"
    const val ORDERS = "orders/my-orders"
    const val SORT_ORDER = "orders/sort-orders/"

    const val ORDER_DETAILS: String = "orders/order/"
    const val ORDER_CANCEL: String = "orders/cancel-order/"
    const val PRODUCT_LIST: String = "products/list"
    const val SEARCH: String = "orders/search?search="
    const val PRODUCT_DETAILS: String = "settings/product"

    const val ADDRESS_LIST: String = "settings/addresses"
    const val ADD_ADDRESS: String = "settings/add-address"
    const val OFFERS: String = "settings/offers"
    const val INNER_OFFER: String = "settings/offers-inner/"
    const val SOCIALS: String = "settings/socials"
    const val REFUND: String = "orders/refund-order"
    const val GET_SIZES: String = "settings/sizes"
    const val WALLET: String = "orders/refunded-orders"

    //
//


    /*
    {{moata}}/user/variations-calculations?product_id=5&advanced[]=2&advanced[]=3
     */
    const val HOME: String = "settings/home"
}