package grand.app.juha.utils.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.DisplayMetrics
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ScrollView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import grand.app.juha.R
import grand.app.juha.base.MyApplication
import java.net.URL
import java.util.*

/**
 * Created by osama on 03/01/2018.
 */
object AppHelper {
    fun setBackgroundNoData(context: Context?, view: ScrollView?) {
        //ToDO setBackgroundNoData Image drawable
        view?.background = context?.let { ContextCompat.getDrawable(it, R.drawable.ccp_down_arrow) }
    }

    @SuppressLint("MissingPermission")
    fun getDeviceId(context: Activity?): String? {
        val tm = context?.getBaseContext()
            ?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val tmDevice: String
        val tmSerial: String
        val androidId: String
        tmDevice = "" + tm.deviceId
        tmSerial = "" + tm.simSerialNumber
        androidId = (""
                + Settings.Secure.getString(
            context.getContentResolver(),
            Settings.Secure.ANDROID_ID
        ))
        val deviceUuid = UUID(
            androidId.hashCode().toLong(),
            tmDevice.hashCode() as Long shl 32 or tmSerial.hashCode().toLong()
        )
        return deviceUuid.toString()
    }

    fun showDialog(context: Context, title: String = "",message:String = "") {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_success)
        (dialog.findViewById(R.id.tv_dialog_title_text) as AppCompatTextView).text = title
        (dialog.findViewById(R.id.tv_dialog_message_text) as AppCompatTextView).text = message
        dialog.show()
    }


    fun shareUrl(url: String, context: Context) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share")
        sharingIntent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"))
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            MyApplication.instance?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun initCalender(
        context: Context?,
        startFromNow: Boolean,
        datePickerDialog: DatePickerDialog.OnDateSetListener?
    ): DatePickerDialog? {
        val calendar = Calendar.getInstance()
        var day = 6
        var month = 6
        var year = 1990
        if (startFromNow) {
            day = calendar[Calendar.DAY_OF_MONTH]
            month = calendar[Calendar.MONTH]
            year = calendar[Calendar.YEAR]
        }
        val dPickerDialog = context?.let {
            DatePickerDialog(
                it,
                R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Calendar,
                datePickerDialog,
                year,
                month,
                day
            )
        }
        if (startFromNow) dPickerDialog?.datePicker?.minDate = calendar.timeInMillis
        dPickerDialog?.setOnShowListener {
            //                dPickerDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setPadding(5,5,5,5);
//                dPickerDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setPadding(5,5,5,5);
        }
        return dPickerDialog
    }

    fun restartApp(activity: Activity?) {
        activity?.let {
            val i = activity.baseContext.packageManager
                .getLaunchIntentForPackage(activity.baseContext.packageName)
            i?.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.finish()
            activity.startActivity(i)
        }
    }

    fun getScreenWidth(context: Context?): Int {
//        val displaymetrics = DisplayMetrics()
//        (context as Activity?)?.windowManager?.defaultDisplay?.getMetrics(displaymetrics)
//        return displaymetrics.widthPixels
        return Resources.getSystem().displayMetrics.widthPixels;

    }

    fun getScreenHeight(context: Context?): Int {
        val displaymetrics = DisplayMetrics()
        (context as Activity?)?.windowManager?.defaultDisplay?.getMetrics(displaymetrics)
        return displaymetrics.heightPixels
    }

    fun dateConvert(year: Int, month: Int, day: Int): String? {
        val month_select = month + 1
        var month_final = month_select.toString()
        var day_final = day.toString()
        if (month_select < 10) {
            month_final = "0$month_final"
        }
        if (day < 10) {
            day_final = "0$day_final"
        }
        return "$year-$month_final-$day_final"
    }

    fun closeApp(activity: Activity?) {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity?.startActivity(intent)
    }

    fun shareApp(activity: Activity) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        val shareSub = activity?.getString(R.string.app_name)
        val shareBody = getPlayStoreLink(activity)
        intent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
        intent.putExtra(Intent.EXTRA_TEXT, shareBody)
        //        activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.share)));
        activity.startActivity(Intent.createChooser(intent, "share"))
    }

    fun getPlayStoreLink(context: Context): String {
        val appPackageName = context.packageName
        return "https://play.google.com/store/apps/details?id=$appPackageName"
    }

    fun shareCustom(activity: Activity?, title: String?, message: String?) {
        var message = message
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        val appPackageName: String? = MyApplication.instance?.packageName
        message += "\n\nhttps://play.google.com/store/apps/details?id=$appPackageName"
        intent.putExtra(Intent.EXTRA_SUBJECT, title)
        intent.putExtra(Intent.EXTRA_TEXT, message)
        //        activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.share)));
        activity?.startActivity(Intent.createChooser(intent, "share"))
    }
    private fun isAppInstalled(packageName: String): Boolean {
        val pm: PackageManager = MyApplication.instance.getPackageManager()
        val app_installed: Boolean
        app_installed = try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
        return app_installed
    }

    fun openWhats(activity: Activity, number: String) {
        val appPackageName = "com.whatsapp"
        if(isAppInstalled(appPackageName)) {
            val uri = Uri.parse("smsto:$number")
            val i = Intent(Intent.ACTION_SENDTO, uri)
            i.setPackage(appPackageName)
            activity.startActivity(Intent.createChooser(i, ""))
        }else{
            activity.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )

        }
    }

    fun openDialNumber(context: Context?, number: String?) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        context?.startActivity(intent)
    }

    fun openBrowser(context: Context?, url: String) {
        var Url: String = url
        url.let {
            if (!url.startsWith("http://") && !url.startsWith("https://")) Url = "http://$url"
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(Url))
            context?.startActivity(browserIntent)
        }
    }

    fun openEmail(context: Context?, email: String?) {
        val mailto = "mailto:" + email + "?" +  //                "?cc=" + "sales@2grand.net" +
                "&subject=" + context?.getString(R.string.app_name) +
                "&body="
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse(mailto)
        try {
            context?.startActivity(emailIntent)
        } catch (e: ActivityNotFoundException) {
            //TODO: Handle case where no email app is available
            Toast.makeText(context, "No Server Mail Application Found", Toast.LENGTH_SHORT).show()
        }
    }

//    @SuppressLint("WrongConstant")
//    fun initVerticalRV(recyclerView: RecyclerView?, spanCount: Int) {
//        recyclerView?.let {
//            recyclerView.setHasFixedSize(true)
//            recyclerView.setItemViewCacheSize(30)
//            recyclerView.isDrawingCacheEnabled = true
//            recyclerView.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
//            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, spanCount, LinearLayoutManager.VERTICAL, false)
//        }
//    }
//
//    @SuppressLint("WrongConstant")
//    fun initHorizontalRV(recyclerView: RecyclerView?, spanCount: Int) {
//
//        recyclerView?.let {
//            recyclerView.setHasFixedSize(true)
//            recyclerView.setItemViewCacheSize(30)
//            recyclerView.isDrawingCacheEnabled = true
//            recyclerView.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
//            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, spanCount, LinearLayoutManager.HORIZONTAL, false)
//        }
//
//
//    }

    fun numberToDecimal(number: Int): String? {
        var number2Decimal = number.toString()
        if (number < 10) {
            number2Decimal = "0$number2Decimal"
        }
        return number2Decimal
    }

    fun hideKeyboard(activity: Activity?) {
        activity?.let {
            val view = activity.currentFocus
            if (view != null) {
                val imm =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
            activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }
    }

    fun getBitmapFromUrl(imageUrl: String): Bitmap {
        val url = URL(imageUrl)
        var myBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        return myBitmap
    }
}