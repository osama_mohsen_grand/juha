package grand.app.juha.utils.helper

interface IOBackCommonPressed {
    fun onBackPressed(): Boolean
}