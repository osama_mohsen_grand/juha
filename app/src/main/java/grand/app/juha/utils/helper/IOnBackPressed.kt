package grand.app.juha.utils.helper

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}