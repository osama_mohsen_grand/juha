package grand.app.juha.utils.popup

interface PopUpInterface {
    fun submitPopUp(position: Int)
}