package grand.app.juha.utils.popup

import android.app.Activity
import android.view.View
import androidx.appcompat.widget.PopupMenu
import com.google.protobuf.LazyStringArrayList
import grand.app.juha.base.model.IdName
import kotlin.collections.ArrayList

class PopUpMenuHelper() {
    private lateinit var popUpMenu: PopupMenu

    fun openPopUp(
        context: Activity,
        viewAction: View,
        objects: LazyStringArrayList,
        popUpInterface: PopUpInterface
    ) {
        val list: MutableList<String> =
            ArrayList()
        list.clear()
        list.addAll(objects)
        popUpMenu = PopupMenu(context, viewAction)
        for (i in list.indices) {
            popUpMenu.menu.add(i, i, i, list[i])
        }
        popUpMenu.setOnMenuItemClickListener { item ->
            popUpInterface.submitPopUp(item.itemId)
            false
        }
        popUpMenu.show()
    }


    fun openPopUpIdName(
        context: Activity,
        viewAction: View,
        objects: ArrayList<IdName>,
        popUpInterface: PopUpInterface
    ) {
        val list: MutableList<String> =
            ArrayList()
        list.clear()
        val arr = LazyStringArrayList()
        for(item : IdName in objects)
            arr.add(item.name)
        list.addAll(arr)
        popUpMenu = PopupMenu(context, viewAction)
        for (i in list.indices) {
            popUpMenu.menu.add(i, i, i, list[i])
        }
        popUpMenu.setOnMenuItemClickListener { item ->
            popUpInterface.submitPopUp(item.itemId)
            false
        }
        popUpMenu.show()
    }


}