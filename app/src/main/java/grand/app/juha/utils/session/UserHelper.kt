package grand.app.juha.utils.session

import android.util.Log
import com.google.gson.Gson
import grand.app.juha.R
import grand.app.juha.pages.auth.login.model.User
import grand.app.juha.pages.product.model.list.ProductListResponse
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager
import java.lang.reflect.Type

/**
 * Created by mohamedatef on 1/12/19.
 */
object UserHelper {
    fun getUserId(): Int {
        return SharedPreferenceHelper.getSharedPreferenceInstance().getInt("id", -1)
    }

    fun saveUserDetails(userModel: User) {

        val prefsEditor =
            SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        val gson = Gson()
        val json = gson.toJson(userModel)
        prefsEditor.putString("userDetails", json)
        prefsEditor.putInt("id", userModel.id)
        Log.e("userDetails", json.toString())
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun saveUserId() {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance()?.edit()

        prefsEditor?.let {
            prefsEditor.putInt("id", -1)
            prefsEditor.apply()
            prefsEditor.commit()
        }

    }

    fun clearUserDetails() {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance()?.edit()
        prefsEditor?.let {
            prefsEditor.putString("userDetails", null)
            prefsEditor.putInt("id", -1)
            prefsEditor.apply()
            prefsEditor.commit()
        }
        saveKey(Constants.CART,"0")
    }

    fun saveKey(key: String?, value: String?) {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance()?.edit()
        prefsEditor?.let {
            prefsEditor.putString(key, value)
            prefsEditor.apply()
            prefsEditor.commit()
        }
    }

    fun retrieveKey(key: String?): String? {
        val preferences = SharedPreferenceHelper.getSharedPreferenceInstance()
        return if (preferences.contains(key)) preferences.getString(key, ""); else ""
    }

    fun getUserDetails(): User {

        val json: String =
            SharedPreferenceHelper.getSharedPreferenceInstance().getString("userDetails", "")
                .toString()
        return if (Gson().fromJson(json, User::class.java) != null) {
            Gson().fromJson(json, User::class.java)
        } else {
            val user = User()
            user.name = ResourceManager.getString(R.string.guest)
            user.img = "http://juha.my-staff.net/public/assets/img/no-image.png"
            return user
        }
    }

    fun isLogin() : Boolean{
        return getUserId() != -1
    }

    fun samePhone(phone : String) : Boolean{
        return getUserDetails().phone == phone
    }



    fun saveJsonResponse(key: String?, value: Any?) {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance()?.edit()
        val gson = Gson()
        val json = gson.toJson(value)
        prefsEditor?.let {
            prefsEditor.putString(key, json)
            prefsEditor.apply()
            prefsEditor.commit()
        }
    }

    fun retrieveJsonResponse(key: String?, value: Any?): Any? {
        val gson = Gson()
        val json = SharedPreferenceHelper.getSharedPreferenceInstance()?.getString(key, "")
        return gson.fromJson(json, value as Type?) ?: return Any()
    }


    fun saveSearchResponse(productListResponse: ProductListResponse) {

        val prefsEditor =
            SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        val gson = Gson()
        val json = gson.toJson(productListResponse)
        prefsEditor.putString("productList", json)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun retrieveSearchResponse(): ProductListResponse {

        val json: String =
            SharedPreferenceHelper.getSharedPreferenceInstance().getString("productList", "")
                .toString()
        return if (Gson().fromJson(json, User::class.java) != null) {
            Gson().fromJson(json, ProductListResponse::class.java)
        } else
            ProductListResponse()
    }

    fun clearSearch() {

        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        prefsEditor?.let {
            prefsEditor.putString("productList", null)
            prefsEditor.apply()
            prefsEditor.commit()
        }
    }
}