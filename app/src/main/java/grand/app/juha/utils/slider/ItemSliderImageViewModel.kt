package grand.app.juha.utils.slider

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.juha.pages.home.model.Slider

data class ItemSliderImageViewModel(val model: Slider) {

    var mutableLiveData = MutableLiveData<Any>()

}