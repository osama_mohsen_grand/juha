package grand.app.juha.utils.slider

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.SliderViewAdapter

import grand.app.juha.R
import grand.app.juha.pages.helper.ZoomFragment
import grand.app.juha.pages.home.model.Slider
import grand.app.juha.utils.Constants
import grand.app.juha.utils.helper.MovementHelper


class SliderAdapterExample(context: Context,var mImages: MutableList<Slider> = ArrayList()) :
    SliderViewAdapter<SliderAdapterExample.SliderAdapterVH?>() {
    private val context: Context
    fun renewItems(Images: MutableList<Slider>) {
        mImages = Images
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        mImages.removeAt(position)
        notifyDataSetChanged()
    }

    fun addItem(Image: Slider) {
        mImages.add(Image)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
        val inflate: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_slider_layout_item, null)
        return SliderAdapterVH(inflate)
    }

    override fun getCount(): Int {
        return mImages.size
    }

    private val TAG = "SliderAdapterExample"

    override fun onBindViewHolder(
        viewHolder: SliderAdapterVH?,
        position: Int
    ) {
        val image: Slider = mImages[position]

//        viewHolder?.textViewDescription?.setText(Image.getDescription())
//        viewHolder?.textViewDescription?.textSize = 16f
//        viewHolder?.textViewDescription?.setTextColor(Color.WHITE)
        Log.d(TAG,image.img)
        viewHolder?.itemView?.context?.let {
            Glide.with(it)
                .load(image.img)
                .fitCenter()
                .into(
                    viewHolder.imageViewBackground
                )
        }
        viewHolder?.itemView?.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(Constants.IMAGE,image.img)
            bundle.putString(Constants.PAGE,ZoomFragment::class.java.name)
            MovementHelper.startActivityBase(viewHolder.itemView?.context,bundle)
        }
    }

    //slider view count could be dynamic size

    inner class SliderAdapterVH(itemView: View) :
        SliderViewAdapter.ViewHolder(itemView) {
//        lateinit var itemView: View
        var imageViewBackground: ImageView
//        var imageGifContainer: ImageView
//        var textViewDescription: TextView

        init {
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider)
//            imageGifContainer = itemView.findViewById(R.id.iv_gif_container)
//            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider)
//            this.itemView = itemView
        }
    }

    init {
        this.context = context
    }

//    override fun onBindViewHolder(viewHolder: SliderAdapterVH?, position: Int) {
//        TODO("Not yet implemented")
//    }
}