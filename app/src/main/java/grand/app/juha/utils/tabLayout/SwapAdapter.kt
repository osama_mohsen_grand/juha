package grand.app.juha.utils.tabLayout

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import grand.app.juha.base.model.TabModel
import java.util.*
import kotlin.collections.ArrayList

class SwapAdapter(fm: FragmentManager,var tabs: ArrayList<TabModel>) : FragmentPagerAdapter(fm,tabs.size) {

    override fun getItem(position: Int): Fragment {
        return tabs[position].fragment
    }

    override fun getCount(): Int {
        return tabs.size
    }
}