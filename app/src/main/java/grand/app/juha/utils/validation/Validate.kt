package grand.app.juha.utils.validation

import android.util.Log
import android.util.Patterns
import android.webkit.URLUtil
import grand.app.juha.R
import grand.app.juha.utils.Constants
import grand.app.juha.utils.resources.ResourceManager
import java.util.regex.Pattern

/**
 * Created by osama on 12/31/2017.
 */
object Validate {
    var error = ""

    fun isValid(data: String?, type: String?): Boolean {
        if (isValid(data)) {
            if (data != null) {
                Log.e("valid", data)
            }
            if (type == Constants.EMAIL && !isMail(data)!!) {
                Log.e("valid", "EMAIL")
                error = ResourceManager.getString(R.string.email_not_valid).toString()
                return false
            } else if (type == Constants.PHONE && !isPhoneValid(data)) {
                Log.e("valid", "PHONE")
                error = ResourceManager.getString(R.string.phone_not_valid).toString()
                return false
            }
            return true
        }
        return false
    }

    fun isMatchPassword(password: String?, passwordConfirm: String?): Boolean {
        return if (isValid(password) && isValid(passwordConfirm)) {
            if (password != passwordConfirm) {
                error = ResourceManager.getString(R.string.both_password_are_not_same).toString();
                false
            } else true
        } else false
    }

    fun isPhoneValid(phoneNo: String?): Boolean {
        val expression = "^[0-9-+]{9,15}$"
        val inputStr: CharSequence? = phoneNo
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(inputStr)
        return if (matcher.matches()) true else false
    }

    fun isValid(data: String?): Boolean {
        if (data != null && data.trim { it <= ' ' } == "") {
            Log.e("valid", "dataFalse")
            error = ResourceManager.getString(R.string.this_field_is_required).toString()
            return false
        }
        return true
    }

    fun isNumeric(str: String?): Boolean {
        try {
            str?.toDouble()
        } catch (nfe: NumberFormatException) {
            return false
        }
        return true
    }

    fun isEmpty(str: String?): Boolean? {
        return str == null || str.isEmpty()
    }

    fun isAvLen(str: String?, from: Int, to: Int): Boolean? {
        return str?.length!! > from && str.length < to
    }

    fun isPassword(str: CharSequence?): Boolean? {
        val PASSWORD_PATTERN = Pattern.compile("^" +
                "(?=.*[0-9])" +  //at least 1 digit
                //"(?=.*[a-z])" +         //at least 1 lower case letter
                //"(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[a-zA-Z])" +  //any letter
                //                        "(?=.*[@#$%^&+=])" +    //at least 1 special character
                "(?=\\S+$)" +  //no white spaces
                ".{8,}" +  //at least 4 characters
                "$")
        return if (str == null) false else PASSWORD_PATTERN.matcher(str).matches()
    }

    fun isEqual(str1: String?, str2: String?): Boolean? {
        return if (str1 == str2) true else false
    }

    //Different between URL and NetworkURL http://stackoverflow.com/questions/23866700/difference-between-isnetworkurl-and-isvalidurl
    fun isUrl(url: String?): Boolean? {
        return URLUtil.isValidUrl(url)
    }

    fun isNetworkUrl(url: String?): Boolean? {
        return URLUtil.isNetworkUrl(url)
    }

    fun isMail(str: CharSequence?): Boolean? {
        return if (str == null) false else Patterns.EMAIL_ADDRESS.matcher(str).matches()
    }

    fun isIPAddress(str: CharSequence?): Boolean? {
        return if (str == null) false else Patterns.IP_ADDRESS.matcher(str).matches()
    }

    fun isPhone(str: CharSequence?): Boolean? {
//        Pattern phone_pattern;
//        if (phoneExpression == 1) {
//            phone_pattern =
//                    Pattern.compile("(201)[0-9]{9}");
//            return str == null ? false : phone_pattern.matcher(str).matches();
//        } else
        return if (str == null) false else Patterns.PHONE.matcher(str).matches()
    }
}